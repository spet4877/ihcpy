#!/usr/bin/env python

"""

This script takes the paths of example segmentation, and outputs it with its location on the slides.

"""

import os
import sys

import imageio
from pptx.dml.color import RGBColor

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

import argparse
from os.path import join, exists, basename, dirname
import numpy as np
from modules import visualization, dir_utils
from re import split, findall
import matplotlib.pyplot as plt


def main():
    p = argparse.ArgumentParser(description="Save the segmentation onto a powerpoint slide")


    p.add_argument('-paths', '--paths',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='file paths of the segmentation')
    p.add_argument('-output', '--output_dir',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='directory to save the powerpoint')

    # parse arguments
    args = p.parse_args()
    #args.paths = ' '.join(args.paths)
    args.output_dir = ' '.join(args.output_dir)

    # parse directories
    paths, output_dir = args.paths, args.output_dir

    print(paths)

    # set up slide options

    slide_opts = {"size": [12, 12],
                  "image_size": [12, 12],
                  "layout": 6}

    # set up initial powerpoint slide
    pptxfname, prs = visualization.initialize_pptx(output_dir,
                                                 title="Evaluation of segmentation",
                                                 comments=" ",
                                                 size=[slide_opts["size"][0], slide_opts["size"][1]],
                                                 fname="segmentation_summary")

    slide_opts["prs"] = prs
    slide_opts["pptxfname"] = pptxfname

    #placeholder to save thumbnail (will delete later)

    dir_utils.create_folder(output_dir)
    placeholder_thumbnail_path = join(output_dir, "thumbnail_placeholder.png")

    # loop over given segmentation paths

    for path in paths:

        subject = split("/", path)[-5] #get subject name
        alpha = split("/", path)[-2]
        # color= RGBColor(0xFF, 0xFF, 0xFF) #white
        color = RGBColor(0x00, 0x00, 0x00)  # black
        fontsize = 30

        # add it to slide options
        slide_opts["label"] = [subject, color, fontsize]

        # find percentiles based on name

        percentiles = findall(r'\d+', basename(path))
        percentiles = [int(percentiles[0]), int(percentiles[1])]

        """Thumbnail slide"""

        thumbnailpath = join(dirname(path), "thumbnail.png")

        # add thumbnail
        slide_opts["image_size"] = [12, 10]  # for thumbnail
        slide_opts["position"] = [0, 1]

        # locate patch on the thumbnail

        thumbnail_ = np.array(imageio.imread(thumbnailpath))

        width, height = thumbnail_.shape[1], thumbnail_.shape[0]

        coord_x = int(np.percentile(np.arange(0, width, 2), percentiles[0]))
        coord_y = int(np.percentile(np.arange(0, height, 2), percentiles[1]))

        fig = plt.figure(figsize=(20, 20))
        plt.imshow(thumbnail_, vmax=255, vmin=0)
        plt.yticks([])
        plt.xticks([])

        plt.axvline(coord_x, lw=4)
        plt.axhline(coord_y, lw=4)

        fig.savefig(placeholder_thumbnail_path)
        plt.close()
        slide_opts["prs"] = visualization.image_to_slide(placeholder_thumbnail_path, slide_opts)

        """RGB slide"""

        color = RGBColor(255, 0, 0)
        slide_opts["image_size"] = [12, 12]
        slide_opts["label"] = [subject, color, fontsize]
        png_of_choice_path = join(dirname(path),
                                  "patch_{}_{}.png".format(percentiles[0],
                                                           percentiles[1]))
        print(png_of_choice_path)
        if not exists(png_of_choice_path): #check to make sure it's there
            print("patch {},{} of {} not present in {}".format(percentiles[0],
                                                               percentiles[1],
                                                               subject,
                                                               path))
            continue
        slide_opts["prs"] = visualization.image_to_slide(png_of_choice_path, slide_opts)

        """Segmentation slide"""

        slide_opts["image_size"] = [12, 12]  # for plot
        color = RGBColor(255, 0, 0)

        label = subject + "\n" + alpha

        slide_opts["label"] = [label, color, fontsize]

        """Add mask"""
        slide_opts["prs"] = visualization.image_to_slide(path, slide_opts)
        slide_opts["prs"].save(slide_opts["pptxfname"])


if __name__ == '__main__':
    main()
