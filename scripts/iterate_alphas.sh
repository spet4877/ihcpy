#!/bin/bash

echo ""
echo "=== Iterating over alphas for segmentation optimization ==="
echo ""
echo "(IMPORTANT: Use this script in the scripts folder with the other .py scripts (i.e. get_dab_thresholds.py))"
echo ""

instructions()
{
   echo ""
   echo "Usage: $0 -s <svs filepath> -r <root directory> -c <config> -1 <starting alpha> -2 <increments> -3 -3 <ending alpha> -d <column width> -f <patch dimensions> -l <log directory> "
   echo ""
   echo -e "\t-s Path of IHC svs file"
   echo -e "\t-r Directory to save your output to"
   echo -e "\t-c Configuration of pipeline (default or artefact)"
   echo -e "\t-1 Starting alpha for the range of alphas to iterate"
   echo -e "\t-2 Increments for the range of alphas to iterate"
   echo -e "\t-3 End alpha for the range of alphas to iterate"
   echo -e "\t-d Segmentation dimension; column width of window (i.e. 32)"
   echo -e "\t-f Patch size to display segmentation (i.e. 1024 1024)"
   echo -e "\t-l Directory to save logfiles (if using cluster)"
   echo ""
   exit 1 # Exit script after printing help
}

while getopts "s:r:c:1:2:3:d:f:l:" opt
do
   case "$opt" in
      s ) svs_path="$OPTARG" ;;
      r ) root_dir="$OPTARG" ;;
      c ) config="$OPTARG" ;;
      1 ) start="$OPTARG" ;;
      2 ) incre="$OPTARG" ;;
      3 ) end="$OPTARG" ;;
      d ) segmentation_dims="$OPTARG" ;;
      f ) window_dims="$OPTARG" ;;
      l ) logbasedir="$OPTARG" ;;
      ? ) instructions ;; # Print instructions in case parameter is non-existent
   esac
done

# Check if parameters are empty
if [ -z "$svs_path" ]
then
   echo "-s (Path of IHC svs file) is empty";
fi

if [ -z "$root_dir" ]
then
   echo "-r (Directory to save your output to) is empty";
fi


if [ -z "$config" ]
then
   echo "-c (Configuration of pipeline) is empty";
fi

if [ -z "$start" ]
then
   echo "-1 (Starting alpha for the range of alphas to iterate) is empty";
fi

if [ -z "$incre" ]
then
   echo "-2 (Increments for the range of alphas to iterate) is empty";
fi

if [ -z "$end" ]
then
   echo "-3 (End alpha for the range of alphas to iterate) is empty";
fi

if [ -z "$segmentation_dims" ]
then
   echo "-d (Segmentation dimension; column width of window) is empty";
fi

if [ -z "$window_dims" ]
then
   echo "-f (Patch size for displaying segmentation) is empty";
fi

if [ -z "$logbasedir" ]
then
   echo "-l (Directory to save logfiles) is empty";
fi


if [ -z "$svs_path" ] || [ -z "$root_dir" ] || [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ] || [ -z "$segmentation_dims" ] || [ -z "$window_dims" ] || [ -z "$logbasedir" ] || [ -z "$config" ]
then
    exit 1
fi
##===========Input===========##

#directories
slice_basename=$(basename $svs_path) #get the slice name
slice_basename=${slice_basename%.*}

# announcements
echo "Subject: $slice_basename"
echo "Config: $config"
echo ""

##===========Cluster options=============##

#cluster parameters
logdir="$logbasedir/$slice_basename/$config/iterate_alphas"
r=1

#make output directory
if ! [ -d $root_dir ]; then
  mkdir -p $root_dir
fi

# remove logfiles if present
if [ -d $logdir ]; then
    if [ $r -eq 1 ]; then
      rm -r $logdir
    else
      echo "Folder with log and text files already exists"
      echo "See $logdir or select remove with -r"
      exit 1
    fi
fi

mkdir -p $logdir


##===========Running pipeline=============##

if [ $config = "default" ]; then #default pipeline

  colourmatrix_dir="$root_dir/$slice_basename/$config/global_colour_matrix"
  echo python get_colour_matrix.py --svs_path $svs_path --cm_dir $colourmatrix_dir>>$logdir/colour_matrix.txt

else #artefact pipeline

  colourmatrix_dir="$root_dir/$slice_basename/$config/local_colour_matrix_and_MAD" #get local colour matrix matrix first, if necessary
  echo python get_local_colour_matrix_and_MAD.py --svs_path $svs_path --local_dir $colourmatrix_dir --seg_dim $segmentation_dims>>$logdir/colour_matrix.txt
  beta="0" #minimal weighting for striping correction (we're not evaluating it here)

fi

echo ""
echo "Generating thresholds for..."
echo ""

for alpha in $(seq $start $incre $end);

  do
    echo ""
    echo "Alpha : $alpha"

    if [ $config = "default" ]; then #default pipeline

      threshold_dir="$root_dir/$slice_basename/$config/thresholds/$alpha"
      segmentation_dir="$root_dir/$slice_basename/$config/segmentation/$alpha"

      echo ""
      echo python get_dab_thresholds.py --svs_path $svs_path --cm_dir $colourmatrix_dir --thr_dir $threshold_dir --seg_dim $segmentation_dims --alpha $alpha --config $config>>$logdir/thresholds.txt

    else

      echo ""
      echo "Beta : $beta"

      threshold_dir="$root_dir/$slice_basename/$config/thresholds/$alpha/$beta"
      segmentation_dir="$root_dir/$slice_basename/$config/segmentation/$alpha"

      echo python get_dab_thresholds.py --svs_path $svs_path --cm_dir $colourmatrix_dir --thr_dir $threshold_dir --seg_dim $segmentation_dims --alpha $alpha --beta $beta --config $config>>$logdir/thresholds.txt

    fi

    echo python check_segmentation.py --svs_path $svs_path --cm_dir $colourmatrix_dir --thr_dir $threshold_dir --seg_dir $segmentation_dir --seg_dim $segmentation_dims --patch_dim $window_dims --config $config>>$logdir/segmentation.txt

done
#print text file just to be sure
echo "~~Printing text files to be submitted~~"
echo ""
echo "Colour matrix derivation"
echo ""
cat $logdir/colour_matrix.txt
echo ""
echo "DAB thresholds"
echo ""
cat $logdir/thresholds.txt
echo ""
echo "Segmentation"
echo ""
cat $logdir/segmentation.txt
echo ""
echo "~~~"

# submit the textfiles as jobs to queues
jid_cm=$(fsl_sub -q short.q -l $logdir -t $logdir/colour_matrix.txt)
jid_thr=$(fsl_sub -q long.q -j $jid_cm -l $logdir -t $logdir/thresholds.txt)
fsl_sub -q veryshort.q -j $jid_thr -l $logdir -t $logdir/segmentation.txt
