#!/bin/bash

echo ""
echo "=== Evaluating segmentation ==="
echo ""
echo "(IMPORTANT: Use this script in the scripts folder with the other .py scripts (i.e. get_SAF_map.py))"
echo ""

instructions()
{
   echo ""
   echo "Usage: $0 -p <paths> -o <output directory> -l <log directory>"
   echo ""
   echo -e "\t-p Paths of the different segmentations"
   echo -e "\t-o Directory to save your output to"
   echo -e "\t-l Directory to save logfiles (if using cluster)"
   echo ""
   exit 1 # Exit script after printing help
}

while getopts "p:o:l:" opt
do
   case "$opt" in
      p ) paths=(${OPTARG}) ;;
      o ) output_dir="$OPTARG" ;;
      l ) logbasedir="$OPTARG" ;;
      ? ) instructions ;; # Print instructions in case parameter is non-existent
   esac
done

# Check if parameters are empty
if [ -z "$paths" ]
then
   echo "-p (Paths of the different segmentations) is empty";
   #instructions
fi

if [ -z "$output_dir" ]
then
   echo "-o (Directory to save your output to) is empty";
fi

if [ -z "$logbasedir" ]
then
   echo "-l (Directory to save logfiles) is empty";
fi

if [ -z "$paths" ] || [ -z "$output_dir" ] || [ -z "$logbasedir" ]
then
    exit 1
fi
##===========Input===========##


# announcements
echo "Paths to display:"
echo ""
echo "${paths[@]}"

##===========Cluster options=============##

#cluster parameters
logdir="$logbasedir/segmentation"
r=1

#make output directory
if ! [ -d $output_dir ]; then
  mkdir -p $output_dir
fi

# remove logfiles if present
if [ -d $logdir ]; then
    if [ $r -eq 1 ]; then
      rm -r $logdir
    else
      echo "Folder with log and text files already exists"
      echo "See $logdir or select remove with -r"
      exit 1
    fi
fi

mkdir -p $logdir


##===========Running pipeline=============##

# shellcheck disable=SC2068
echo python evaluate_segmentation.py --paths ${paths[@]} --output_dir $output_dir>>$logdir/evaluate_segmentation.txt

#print text file just to be sure
echo "~~Printing text files to be submitted~~"
echo ""
echo "Evaluating segmentation"
echo ""
cat $logdir/evaluate_segmentation.txt
echo "~~~"

# submit the textfiles as jobs to queues
fsl_sub -q short.q -l $logdir -t $logdir/evaluate_segmentation.txt

