#!/usr/bin/env python

"""
This script takes the .svs slide and computes a SAF map.
Assumes that colour matrix ("get_colour_matrix.py" for
default; "get_local_colour_matrix_and_MAD.py" for artefact) and DAB thresholds ("get_dab_thresholds.py") are already
computed.

:param svs_path: path of the .svs file
:param cm_dir: directory containing the colour matrix .npz
:param thr_dir: directory containing the DAB thresholds
:param saf_dir: directory to save the SAF map
:param seg_dim: Dimension of column or patch to perform segmentation
:param patch_dim: Dimension of patch to store area; this is the patch map resolution (high resolution SAF map)
:param saf_patch_dim: dimension of patch to compute SAF; this is the SAF map resolution (lower or MRI-scale resolution SAF map)
:param gamma: weighting used to smooth DAB thresholds (kernel size); only used in the artefact configuration.
:param config: configuration to use; only 'default' or 'artefact'.

:return patch_map.tiff/png: high-resolution map containing number of pixels segmented. Each pixel = area stained for patch size of patch_dim. Use .tiff for correlations.
:return SAF_map.tiff/png: low-resolution SAF. Each pixel = area fraction (i.e. alreeady normalized) stained for patch size of saf_patch_dim. Use .tiff for correlations.
:return tissue_map.tiff/png: Tissue map used for normalization.
:return results.p: pickle with SAF map and patch map metadata
:return metadata.json: metadata of svs file
"""

import os
import sys

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

import argparse
from os.path import join, exists
import numpy as np
from modules import histoSAF, dir_utils, maps, tissue_masking, stain_utils
import matplotlib.pyplot as plt


def main():
    p = argparse.ArgumentParser(description="SAF map for WSI ")

    """directories"""

    p.add_argument('-sp', '--svs_path',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='path of the svs file')
    p.add_argument('-cm', '--cm_dir',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='directory containing GLOBAL colour matrix for the stain')
    p.add_argument('-td', '--thr_dir',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='directory to save the thresholds to')
    p.add_argument('-safd', '--saf_dir',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='directory to save the SAF map to')

    """parameters"""

    p.add_argument('-sd', '--seg_dim',
                   required=True, nargs='+', type=int,
                   help='dimension of segmentation window')
    p.add_argument('-pd', '--patch_dim',
                   required=True, nargs='+', type=int,
                   help='dimension of patch to store area; this is the patch map resolution (high resolution SAF map)')
    p.add_argument('-spd', '--saf_patch_dim',
                   required=True, nargs='+', type=int,
                   help='dimension of patch to compute SAF; this is the SAF map resolution (lower or MRI-scale '
                        'resolution SAF map)')
    p.add_argument('-gamma', '--gamma',
                   required=False, nargs='+', type=int,
                   help='weighting used to smooth DAB thresholds (kernel size); only used in config that is '
                        'artefact-corrected')

    """Configuration type"""

    p.add_argument('-c', '--config',
                   required=True, nargs='+', type=str,
                   help='default or artefact-corrected')

    # parse arguments
    args = p.parse_args()
    args.svs_path = ' '.join(args.svs_path)
    args.saf_dir = ' '.join(args.saf_dir)
    args.cm_dir = ' '.join(args.cm_dir)
    args.thr_dir = ' '.join(args.thr_dir)
    args.config = ' '.join(args.config)

    dim_window, dim_patch, dim_saf_patch = np.array(args.seg_dim), \
                                           np.array(args.patch_dim), \
                                           np.array(args.saf_patch_dim)
    config = args.config

    # some assertions; check beta is available if artefact-corrected configuration

    assert config in ["default", "artefact"], "Configuration must be either default or artefact (given: {})".format(
        config)

    if config in ["artefact"]:
        gamma = int(np.array(args.gamma)[0])
        assert isinstance(gamma, int), "Gamma parameter is not an int ({})".format(gamma)

    # parse directories
    svs_fname, thr_dir, cm_dir, saf_dir = args.svs_path, args.thr_dir, args.cm_dir, args.saf_dir

    """Check for threshold pickle"""

    output_checker = join(saf_dir, "SAF_map.tiff")

    if exists(output_checker):
        print("{} exists. Stopping get_SAF_map.py...".format(output_checker))
        return

    """Set different masks opts"""
    tissue_mask_opts = {}

    saf_tissue_mask_opts = {"hema_mask": True,  # operations on tissue mask that we normalize our SAF by
                            "dilation": (3, 3),
                            "median": (10, 10),
                            "fill_holes": True}

    if config in ["default"]:

        cell_mask_opts = {"default_threshold": True,
                          "median": [2, 2]}  # median filter for cell mask; needed for x40 slides

    elif config in ["artefact"]:

        cell_mask_opts = {"external_dab_threshold_map": True,  # local threshold map that has been adjusted w/ MAD
                          "columnwise_colourmatrix": True,  # local colour matrix map (columnwise here)
                          "median": [2, 2]}

    dir_utils.dict_to_json(fname="masks_opts",
                           output_dir=thr_dir,
                           data=[cell_mask_opts, tissue_mask_opts, saf_tissue_mask_opts])

    """Initialize histoSAF object and all window dimensions"""

    # create histosaf object
    histosaf = histoSAF.histoSAF(svs_dir=svs_fname,
                                 output_dir=saf_dir)

    histosaf.segmentation_info(dim_window)
    histosaf.patch_info(dim_patch)  # high resolution SAF map
    histosaf.SAF_patch_info(dim_saf_patch)  # lower resolution (i.e. MRI-scale) SAF map

    """Load colour matrix"""
    # check colour matrix

    cm_file = join(cm_dir, "{}.npz".format(histosaf.metadata["name"]))
    assert exists(cm_file), "Colour matrix .npz file does not exist (used {})".format(cm_file)
    colourmatrix = np.load(cm_file)["a"]
    histosaf.metadata["colour_matrix"] = colourmatrix

    if config in ["artefact"]:  # if artefact configuration, load local colour matrix, MAD maps

        maps_data = join(cm_dir,
                         "maps_data.p")

        assert exists(maps_data), "Local colour matrix and MAD maps for {} do not exist".format(
            histosaf.metadata["name"])

        cell_mask_opts["colour_matrix_map"] = dir_utils.pickle_to_data(maps_data)["colour_matrix_map"]

    """Create output directory"""

    dir_utils.create_folder(saf_dir)

    """Get hematoxylin threshold for tissue mask"""

    global_hema_thr = tissue_masking.get_global_hematoxylin_mask(histosaf.thumbnail,
                                                                 colourmatrix=colourmatrix)
    histosaf.metadata["hema_thr_for_tissue"] = global_hema_thr

    saf_tissue_mask_opts["hema_mask"] = [histosaf.metadata["colour_matrix"],
                                         histosaf.metadata["hema_thr_for_tissue"]]

    ###=========Default=========###

    """Get default DAB threshold for cell mask"""

    if config in ["default"]:
        thr_file = join(thr_dir,
                        "default_threshold.p")

        assert exists(thr_file), "No default threshold pickle (used {})".format(thr_file)

        thr_dict = dir_utils.pickle_to_data(thr_file)
        default_dab_threshold = thr_dict["default"]
        histosaf.metadata["default_thr_for_dab"] = default_dab_threshold

        """Get patch map (high resolution SAF map)"""

        histosaf.patch_map_info["map"], histosaf.patch_map_info[
            "tissue_map"] = maps.patch_map_from_seg_map_RGB_via_default_threshold(histosaf.metadata,
                                                                                  histosaf.patch_map_info,
                                                                                  histosaf.seg_map_info,
                                                                                  tissue_mask_opts,
                                                                                  saf_tissue_mask_opts,
                                                                                  cell_mask_opts)
    ###=========Artefact=========###

    elif config in ["artefact"]:

        local_thr_file = join(thr_dir,
                              "dab_thresholds.p")
        assert exists(local_thr_file), "No local DAB threshold pickle (used {})".format(local_thr_file)

        cell_mask_opts["dab_threshold_map_RGB"] = dir_utils.pickle_to_data(local_thr_file)["dab_threshold_map"]

        histosaf.patch_map_info["map"], histosaf.patch_map_info[
            "tissue_map"] = maps.patch_map_from_seg_map_RGB_via_local_thresholds(histosaf.metadata,
                                                                                 histosaf.patch_map_info,
                                                                                 histosaf.seg_map_info,
                                                                                 tissue_mask_opts,
                                                                                 cell_mask_opts,
                                                                                 saf_tissue_mask_opts,
                                                                                 gamma)

    """Convert patch map to SAF map"""

    histosaf.SAF_map_info["map"], histosaf.SAF_map_info["tissue_map_mask"] = maps.SAF_map_from_patch_map(
        histosaf.SAF_map_info,
        histosaf.patch_map_info)

    results = {"patch_map": histosaf.patch_map_info,
               "SAF_map": histosaf.SAF_map_info}

    """Save outputs"""

    dir_utils.save_arr_to_tiff(histosaf.SAF_map_info["map"],
                               dir_=saf_dir,
                               name_="SAF_map")

    dir_utils.save_arr_to_tiff(histosaf.SAF_map_info["tissue_map_mask"],
                               dir_=saf_dir,
                               name_="tissue_map")  # this is not the mask yet. You still need to morph it.

    dir_utils.save_arr_to_tiff(histosaf.patch_map_info["map"],
                               dir_=saf_dir,
                               name_="patch_map")

    dir_utils.save_arr_to_png(histosaf.SAF_map_info["map"],
                              dir_=saf_dir,
                              name_="SAF_map")

    dir_utils.save_arr_to_png(histosaf.patch_map_info["map"],
                              dir_=saf_dir,
                              name_="patch_map")

    """Save other pickles and jsons (for future correlations)"""

    dir_utils.data_to_pickle(fname="results",
                             output_dir=saf_dir,
                             data=results)

    histosaf.metadata["colour_matrix"] = histosaf.metadata[
        "colour_matrix"].ravel().tolist()  # some comestics to save it properly

    dir_utils.dict_to_json(fname="metadata",
                           output_dir=saf_dir,
                           data=histosaf.metadata)


if __name__ == '__main__':
    main()
