#!/usr/bin/env python

"""

This script takes the .svs slide and computes a slide-specific SINGLE colour matrix

:param svs_path: path of the .svs file
:param cm_dir: directory to save the colour matrix .npz

:return (slide_basename).txt/npz: Text and npz file containing the colour matrix (use np.load("(..).npz")[a] to
access colour matrix as array)

"""


import os
import sys

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

import argparse
from os.path import join, exists
import numpy as np
from modules import histoSAF, dir_utils, colour_matrix


def main():
    p = argparse.ArgumentParser(description="Global colour matrix for WSI")

    """directories"""

    p.add_argument('-sp', '--svs_path',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='Path of the svs file')
    p.add_argument('-cm', '--cm_dir',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='directory containing GLOBAL colour matrix for the stain')


    # parse arguments
    args = p.parse_args()
    args.svs_path = ' '.join(args.svs_path)
    args.cm_dir = ' '.join(args.cm_dir)

    # parse directories
    svs_fname, cm_dir = args.svs_path, args.cm_dir

    print(svs_fname)

    # create histosaf object
    histosaf = histoSAF.histoSAF(svs_dir=svs_fname,
                                 output_dir=cm_dir)

    """Using global colour matrix"""

    # check and/or compute colour matrix
    cm_file = join(cm_dir, "{}.npz".format(histosaf.metadata["name"]))

    if not exists(cm_file):  # colour matrix does not exist
        dir_utils.create_folder(cm_dir)

        images_array = colour_matrix.extract_patches(svs_fname,
                                                     size=128,
                                                     n=1000,
                                                     option=1,
                                                     threshold_values=[0.85, 0, 0.50, 0.50],
                                                     verbose=True)

        colourmatrix = colour_matrix.k_means(images_array,
                                             verbose=True,
                                             threshold_value=0.85)

        np.savez_compressed(join(cm_dir, "{}.npz".format(histosaf.metadata["name"])),
                            a=colourmatrix)

    else:

        print("Global colour matrix exists. Stopping get_colour_matrix.py...")

    colourmatrix = np.load(cm_file)["a"]
    histosaf.metadata["colour_matrix"] = colourmatrix

    np.savetxt(join(cm_dir, "{}.txt".format(histosaf.metadata["name"])), colourmatrix)


if __name__ == '__main__':
    main()
