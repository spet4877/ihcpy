#!/bin/bash

echo ""
echo "=== Single subject submission ==="
echo ""
echo "(IMPORTANT: Use this script in the scripts folder with the other .py scripts (i.e. get_SAF_map.py))"
echo ""

instructions()
{
   echo ""
   echo "Usage: $0 -s <svs filepath> -r <root directory> -a <alpha parameter> -a <alpha> -b <beta> -g <gamma> -d <column width of window> -p <patch dimensions> -f <patch dimensions> -l <log directory>"
   echo ""
   echo -e "\t-s Path of IHC svs file"
   echo -e "\t-r Directory to save your output to"
   echo -e "\t-a Weighting for segmentation in WOV cost function"
   echo -e "\t-b Weighting parameter for MAD (higher striping requires higher weighting)"
   echo -e "\t-g Smoothing parameter for DAB thresholds"
   echo -e "\t-d Segmentation dimension; column width of window (i.e. 32)"
   echo -e "\t-p Patch size for high resolution SAF (i.e. 32 32)"
   echo -e "\t-f Patch size for lower resolution SAF (i.e. 1024 1024)"
   echo -e "\t-l Directory to save logfiles (if using cluster)"
   echo ""
   exit 1 # Exit script after printing help
}

while getopts "s:r:a:b:g:d:p:f:l:" opt
do
   case "$opt" in
      s ) svs_path="$OPTARG" ;;
      r ) root_dir="$OPTARG" ;;
      a ) alpha="$OPTARG" ;;
      b ) beta="$OPTARG" ;;
      g ) gamma="$OPTARG" ;;
      d ) segmentation_dims="$OPTARG" ;;
      p ) patch_dims="$OPTARG" ;;
      f ) SAF_dims="$OPTARG" ;;
      l ) logbasedir="$OPTARG" ;;
      ? ) instructions ;; # Print instructions in case parameter is non-existent
   esac
done

# Check if parameters are empty
if [ -z "$svs_path" ]
then
   echo "-s (Path of IHC svs file) is empty";
   #instructions
fi

if [ -z "$root_dir" ]
then
   echo "-r (Directory to save your output to) is empty";
fi

if [ -z "$alpha" ]
then
   echo "-a (Weighting for segmentation in WOV cost function) is empty";
fi

if [ -z "$beta" ]
then
   echo "-b (Weighting parameter for MAD) is empty";
   #instructions
fi

if [ -z "$gamma" ]
then
   echo "-g (Smoothing parameter for DAB thresholds) is empty";
   #instructions
fi


if [ -z "$segmentation_dims" ]
then
   echo "-d (column width of segmentation window) is empty";
fi

if [ -z "$patch_dims" ]
then
   echo "-d (Patch size for high resolution SAF) is empty";
fi

if [ -z "$SAF_dims" ]
then
   echo "-f (Patch size for lower resolution SAF) is empty";
fi

if [ -z "$logbasedir" ]
then
   echo "-l (Directory to save logfiles) is empty";
fi

if [ -z "$svs_path" ] || [ -z "$root_dir" ] || [ -z "$alpha" ] || [ -z "$segmentation_dims" ] || [ -z "$patch_dims" ] || [ -z "$SAF_dims" ] || [ -z "$logbasedir" ]
then
    exit 1
fi
##===========Input===========##

#parameters
config="artefact"

#directories
slice_basename=$(basename $svs_path) #get the slice name
slice_basename=${slice_basename%.*}

# announcements
echo "Subject: $slice_basename"
echo "Config: $config"
echo "Alpha: $alpha"
echo "Beta: $beta"
echo "Gamma: $gamma"
echo ""

##===========Cluster options=============##

#cluster parameters
logdir="$logbasedir/$slice_basename/$config"
r=1

#make output directory
if ! [ -d $root_dir ]; then
  mkdir -p $root_dir
fi

# remove logfiles if present
if [ -d $logdir ]; then
    if [ $r -eq 1 ]; then
      rm -r $logdir
    else
      echo "Folder with log and text files already exists"
      echo "See $logdir or select remove with -r"
      exit 1
    fi
fi

mkdir -p $logdir


##===========Running pipeline=============##

slice_basename=$(basename $svs_path) #get the slice name
slice_basename=${slice_basename%.*}
local_dir="$root_dir/$slice_basename/$config/local_colour_matrix_and_MAD"
segmentation_dir="$root_dir/$slice_basename/$config/segmentation/$alpha"
threshold_dir="$root_dir/$slice_basename/$config/thresholds/$alpha/$beta"
SAFmap_dir="$root_dir/$slice_basename/$config/SAF/$alpha/$beta/$gamma"

echo python get_local_colour_matrix_and_MAD.py --svs_path $svs_path --local_dir $local_dir --seg_dim $segmentation_dims>>$logdir/local_colour_matrix_and_MAD.txt
echo python get_dab_thresholds.py --svs_path $svs_path --cm_dir $local_dir --thr_dir $threshold_dir --seg_dim $segmentation_dims --alpha $alpha --beta $beta --config $config>>$logdir/thresholds.txt
echo python check_segmentation.py --svs_path $svs_path --cm_dir $local_dir --thr_dir $threshold_dir --seg_dir $segmentation_dir --seg_dim $segmentation_dims --patch_dim $SAF_dims --config $config>>$logdir/segmentation.txt
echo python get_SAF_map.py --svs_path $svs_path --cm_dir $local_dir --thr_dir $threshold_dir --saf_dir $SAFmap_dir --seg_dim $segmentation_dims --patch_dim $patch_dims --saf_patch_dim $SAF_dims --gamma $gamma --config $config>>$logdir/SAF.txt


#print text file just to be sure
echo "~~Printing text files to be submitted~~"
echo ""
echo "Colour matrix derivation"
echo ""
cat $logdir/local_colour_matrix_and_MAD.txt
echo ""
echo "DAB thresholds"
echo ""
cat $logdir/thresholds.txt
echo ""
echo "Segmentation"
echo ""
cat $logdir/segmentation.txt
echo ""
echo "SAF map"
echo ""
cat $logdir/SAF.txt
echo "~~~"

# submit the textfiles as jobs to queues
jid_cm=$(fsl_sub -q short.q -l $logdir -t $logdir/local_colour_matrix_and_MAD.txt)
jid_thr=$(fsl_sub -q long.q -j $jid_cm -l $logdir -t $logdir/thresholds.txt)
jid_seg=$(fsl_sub -q veryshort.q -j $jid_thr -l $logdir -t $logdir/segmentation.txt)
fsl_sub -q long.q -j $jid_seg -l $logdir -t $logdir/SAF.txt

