#!/usr/bin/env python

"""
This script takes a directory with many SAF maps, and finds the "best" SAF map with the best metric score.
This metric score measures the impact of striping and staining gradient on the SAF map. The "best" SAF map is the most 'artefact-corrected' SAF map.
NOTE: this is only used in the artefact pipeline.

:param safs_dir: directory with the different SAF maps for a single subject.
:param remove: if this flag is set, remove existing chosen_SAF directory. Useful for running the grid search with more data.

:return chosen_SAF: directory with the "best" SAF map.
"""

import glob
import os
import shutil
import sys

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from os.path import join, dirname, exists
from modules import dir_utils, tissue_masking, evaluation, visualization
import numpy as np
import matplotlib.pyplot as plt
import argparse
from pandas import DataFrame, read_pickle
from seaborn import heatmap
from re import split


def main():
    p = argparse.ArgumentParser(description="SAF map for WSI ")

    """directories"""

    p.add_argument('-sd', '--safs_dir',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='path containing all the SAF maps used for grid search')
    """flag"""
    p.add_argument('--remove', action='store_true',
                   help='flag to remove pickles and results, and redo the grid search')

    args = p.parse_args()
    args.safs_dir = ' '.join(args.safs_dir)
    safs_dir = args.safs_dir
    remove = args.remove
    dir_with_pickles = join(safs_dir, "pickles")  # directory with different SAF pickles
    visualization_dir = join(safs_dir, "chosen_SAF")  # directory with chosen SAF

    """Remove directories if flag"""
    if remove and exists(dir_with_pickles):
        print("Remove flag activated. Removing pickles...")

        shutil.rmtree(dir_with_pickles)
        shutil.rmtree(visualization_dir)

    dir_utils.create_folder(dir_with_pickles)
    dir_utils.create_folder(visualization_dir)

    metric_ppath = join(dir_with_pickles, "minimized_highpass_std.p")
    within_slide_variation_ppath = join(dir_with_pickles, "within_slide_variations.p")
    no_striping_correction_ppath = join(dir_with_pickles, "no_striping_correction.p")

    """Options to evaluate striping and gradient staining"""
    high_pass_filters_opts = {"cutoff_lower": 0.02,
                              "cutoff_upper": 0.08,
                              "order": 6,
                              "sample_rate": 300}

    """Percentiles for width and height"""

    h1, h2, w1, w2 = 0.01, 0.99, 0.10, 0.90

    if not exists(metric_ppath) or not exists(within_slide_variation_ppath):

        print("Iterating paths")

        """Sort directories to iterate over"""
        all_gammas = glob.glob(join(safs_dir, "*", "*", "results.p"))
        # all_gammas = [glob.glob(join(x, "*")) for x in all_betas]
        # all_gammas = [y for x in all_gammas for y in x if basename(y).isnumeric()]

        beta_list = []
        gamma_list = []
        safpath_list = []

        print(all_gammas)

        metric_list = []  # list to contain evaluation metric

        for idx, result_pickle in enumerate(all_gammas):

            safdir = dirname(result_pickle)
            gamma = split("/", safdir)[-1]
            beta = split("/", safdir)[-2]

            if not exists(result_pickle):
                with open(join(safs_dir, "beta_{}_gamma_{}.txt").format(beta, gamma), "w") as text_file:
                    print("none", file=text_file)
                pass

            print("beta: {} ; gamma: {} ".format(beta, gamma))

            saf_map = dir_utils.pickle_to_data(result_pickle)["patch_map"]["map"] / (32 ** 2)

            if idx == 0:
                # dimensions
                height, width = saf_map.shape[0], saf_map.shape[-1]
                start_y, end_y = round(w1 * width), round(w2 * width)
                start_x, end_x = round(h1 * height), round(h2 * height)

                tissue_mask = tissue_masking.tissue_mask_from_SAF_map(
                    saf_map)  # get the first tissue mask for evaluation

                tissue_mask = tissue_mask[start_x:end_x, start_y: end_y]

            # get the striping metric

            saf_map = saf_map[start_x:end_x, start_y: end_y]

            saf_map_opts = {"SAF_map": saf_map,
                            "tissue_mask": tissue_mask}

            striping_char, non_striping_char, _ = evaluation.characteristics_of_striping(saf_map_opts,
                                                                                         high_pass_filters_opts,
                                                                                         plot=True)

            highpass_std = striping_char["std_r"]  # select highpass standard deviation as metric to optimize over

            beta_list.append(beta)
            gamma_list.append(gamma)
            safpath_list.append(result_pickle)
            metric_list.append(highpass_std)

        within_slide_variations = DataFrame()
        within_slide_variations["safmap_path"] = safpath_list

        # parameters
        within_slide_variations["beta"] = beta_list
        within_slide_variations["beta"] = within_slide_variations["beta"].astype(float)
        within_slide_variations["gamma"] = gamma_list
        within_slide_variations["gamma"] = within_slide_variations["gamma"].astype(float)

        within_slide_variations["highpass_std"] = metric_list
        within_slide_variations["highpass_std"] = within_slide_variations["highpass_std"].astype(float)

        # finding SAF map entry with lowest metric AND SAF map entry with beta=0, gamma=1 (i.e. no striping correction)
        minimized_metric = within_slide_variations.loc[within_slide_variations["highpass_std"].idxmin()]
        max_gamma = within_slide_variations["gamma"].max()
        #print(max_gamma)
        no_striping_correction = within_slide_variations.loc[
            (within_slide_variations["beta"] == 0.0) & (within_slide_variations["gamma"] == max_gamma)]

        # save pickles
        minimized_metric.to_pickle(path=metric_ppath)
        no_striping_correction.to_pickle(path=no_striping_correction_ppath)
        within_slide_variations.to_pickle(path=within_slide_variation_ppath)

    """Visualization"""

    from numpy.ma import masked_array
    from modules import stain_utils

    # load the the 3 dataframes originally saved
    minimized_metric = read_pickle(metric_ppath)
    within_slide_variations = read_pickle(within_slide_variation_ppath)
    no_striping_correction = read_pickle(no_striping_correction_ppath)

    # load optimized SAF map & parameters
    safmap_path = minimized_metric["safmap_path"]
    beta = minimized_metric["beta"]
    gamma = int(minimized_metric["gamma"])

    saf_map = dir_utils.pickle_to_data(safmap_path)["patch_map"]["map"] / (32 ** 2)

    tissue_mask = tissue_masking.tissue_mask_from_SAF_map(
        saf_map)  # get tissue mask

    # get dimensions to crop
    height, width = saf_map.shape[0], saf_map.shape[-1]
    start_y, end_y = round(w1 * width), round(w2 * width)
    start_x, end_x = round(h1 * height), round(h2 * height)

    # crop to match dimensions
    saf_map = saf_map[start_x:end_x, start_y: end_y]
    tissue_mask = tissue_mask[start_x:end_x, start_y: end_y]

    saf_map_opts = {"SAF_map": saf_map,
                    "tissue_mask": tissue_mask}

    striping_char, non_striping_char, \
    _ = evaluation.characteristics_of_striping(saf_map_opts,
                                               high_pass_filters_opts,
                                               plot=True)

    # load no striping correction SAF map & parameters

    no_correction_safmap_path = no_striping_correction["safmap_path"].ravel()[0]

    no_correction_saf_map = dir_utils.pickle_to_data(no_correction_safmap_path)["patch_map"]["map"] / (32 ** 2)

    # crop to match dimensions
    no_correction_saf_map = no_correction_saf_map[start_x:end_x, start_y: end_y]

    saf_map_opts = {"SAF_map": no_correction_saf_map,
                    "tissue_mask": tissue_mask}

    nc_striping_char, nc_non_striping_char, \
    _ = evaluation.characteristics_of_striping(saf_map_opts,
                                               high_pass_filters_opts,
                                               plot=True)

    # make the plot
    axd = plt.figure(constrained_layout=True, figsize=(40, 20)).subplot_mosaic(
        """
        AABBDDEEGG
        AABBDDEEGG
        CCCCCFFFFF
        CCCCCFFFFF
        """
    )
    """Automated SAF"""
    """Standardized SAF map"""
    im = axd["A"].imshow(masked_array(stain_utils.standardize(saf_map), np.logical_not(tissue_mask)), vmax=2,
                         vmin=0,
                         cmap="hot")
    axd["A"].set_yticks([])
    axd["A"].set_xticks([])
    axd["A"].set_title("Standardized", fontsize=40)

    cb = visualization.add_colorbar(im, aspect=10, pad_fraction=2, location="left")

    """Optimized SAF map"""
    im2 = axd["B"].imshow(masked_array(saf_map, np.logical_not(tissue_mask)), vmax=1, vmin=0, cmap="hot")
    axd["B"].set_yticks([])
    axd["B"].set_xticks([])
    axd["B"].set_title("Raw", fontsize=40)

    cb = visualization.add_colorbar(im2, aspect=12, pad_fraction=2, location="left")

    """horizontal plot"""
    horplot = striping_char["horplot"]
    residual = striping_char["residual"]

    std_r = striping_char["std_r"]

    n = horplot.shape[0]  # total number of samples
    N = np.arange(0, n, 1)
    axd["C"].plot(N, horplot, 'r-', linewidth=4, label='raw SAF')
    axd["C"].plot(N, residual, "b-", linewidth=4, ls="dashed",
                  label='Highpass striping; std = {:.4f}'.format(std_r))

    axd["C"].set_xlabel('Index of WSI', fontsize=30)
    axd["C"].set_ylabel('Averaged SAF', fontsize=30)
    axd["C"].legend(fontsize=40)

    # display b and c for convenience
    axd["C"].set_title("beta = {} , gamma = {}".format(beta, gamma), fontsize=40)

    """No correction SAF"""
    im = axd["D"].imshow(masked_array(stain_utils.standardize(no_correction_saf_map),
                                      np.logical_not(tissue_mask)), vmax=2,
                         vmin=0,
                         cmap="hot")
    axd["D"].set_yticks([])
    axd["D"].set_xticks([])
    axd["D"].set_title("Standardized", fontsize=40)

    cb = visualization.add_colorbar(im, aspect=10, pad_fraction=2, location="left")

    """SAF map"""
    im2 = axd["E"].imshow(masked_array(no_correction_saf_map,
                                       np.logical_not(tissue_mask)), vmax=1, vmin=0, cmap="hot")
    axd["E"].set_yticks([])
    axd["E"].set_xticks([])
    axd["E"].set_title("Raw", fontsize=40)

    cb = visualization.add_colorbar(im2, aspect=12, pad_fraction=2, location="left")

    """horizontal plot"""

    horplot = nc_striping_char["horplot"]
    residual = nc_striping_char["residual"]

    std_r = nc_striping_char["std_r"]

    n = horplot.shape[0]  # total number of samples
    N = np.arange(0, n, 1)
    axd["F"].plot(N, horplot, 'r-', linewidth=4, label='raw SAF')
    axd["F"].plot(N, residual, "b-", linewidth=4, ls="dashed",
                  label='Highpass striping; std = {:.4f}'.format(std_r))

    axd["F"].set_xlabel('Index of WSI', fontsize=30)
    axd["F"].set_ylabel('Averaged SAF', fontsize=30)
    axd["F"].legend(fontsize=40)

    # display b and c for convenience
    axd["F"].set_title("beta = 0; gamma = 30", fontsize=40)

    # create heatmap

    within_slide_variations = within_slide_variations.drop('safmap_path', 1)

    heatmap(within_slide_variations.pivot("beta", "gamma", values='highpass_std'), annot=True, cmap="hot", fmt='.4f',
            annot_kws={"size": 15}, ax=axd["G"])

    axd["G"].set_xlabel("gamma", fontsize=30)
    axd["G"].set_ylabel("beta", fontsize=30)

    """Save it"""
    plt.savefig(join(visualization_dir, "beta_{}_gamma_{}.png".format(beta, gamma)))

    """Move SAF map directory to same directory"""

    safmap_src = dirname(safmap_path)
    safmap_destination = join(visualization_dir, "beta_{}_gamma_{}".format(beta, gamma))
    shutil.copytree(safmap_src, safmap_destination)


if __name__ == '__main__':
    main()
