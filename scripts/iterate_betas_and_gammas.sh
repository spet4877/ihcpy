#!/bin/bash

echo ""
echo "=== Iterating over betas and gammas for grid search (artefact config. only) ==="
echo ""
echo "(IMPORTANT: Use this script in the scripts folder with the other .py scripts (i.e. get_dab_thresholds.py))"
echo "This also assumes you have selected an alpha term."
echo ""

instructions()
{
   echo ""
   echo "Usage: $0 -s <svs filepath> -r <root directory> -a <alpha> -1 <starting beta> -2 <increments in beta> -3 <ending beta> -4 <starting gamma> -5 <increments in gamma> -6 <ending gamma> -d <column width of window> -p <patch dimensions> -f <patch dimensions> -l <log directory> "
   echo ""
   echo -e "\t-s Path of IHC svs file"
   echo -e "\t-r Directory to save your output to"
   echo -e "\t-a Alpha term for segmentation"
   echo -e "\t-1 Starting beta for the range of betas to iterate"
   echo -e "\t-2 Increments for the range of betas to iterate"
   echo -e "\t-3 End beta for the range of betas to iterate"
   echo -e "\t-4 Starting gamma for the range of gammas to iterate"
   echo -e "\t-5 Increments for the range of gammas to iterate"
   echo -e "\t-6 End gamma for the range of gammas to iterate"
   echo -e "\t-d Segmentation dimension; column width of window (i.e. 32)"
   echo -e "\t-p Patch size for high resolution SAF (i.e. 32 32)"
   echo -e "\t-f Patch size for lower resolution SAF (i.e. 1024 1024)"
   echo -e "\t-l Directory to save logfiles (if using cluster)"
   echo ""
   exit 1 # Exit script after printing help
}

while getopts "s:r:a:1:2:3:4:5:6:d:p:f:l:" opt
do
   case "$opt" in
      s ) svs_path="$OPTARG" ;;
      r ) root_dir="$OPTARG" ;;
      a ) alpha="$OPTARG" ;;
      1 ) start_b="$OPTARG" ;;
      2 ) incre_b="$OPTARG" ;;
      3 ) end_b="$OPTARG" ;;
      4 ) start_g="$OPTARG" ;;
      5 ) incre_g="$OPTARG" ;;
      6 ) end_g="$OPTARG" ;;
      d ) segmentation_dims="$OPTARG" ;;
      p ) patch_dims="$OPTARG" ;;
      f ) SAF_dims="$OPTARG" ;;
      l ) logbasedir="$OPTARG" ;;
      ? ) instructions ;; # Print instructions in case parameter is non-existent
   esac
done

# Check if parameters are empty
if [ -z "$svs_path" ]
then
   echo "-s (Path of IHC svs file) is empty";
fi

if [ -z "$root_dir" ]
then
   echo "-r (Directory to save your output to) is empty";
fi

if [ -z "$alpha" ]
then
   echo "-a (Alpha) is empty";
fi

if [ -z "$start_b" ]
then
   echo "-1 (Starting beta for the range of betas to iterate) is empty";
fi

if [ -z "$incre_b" ]
then
   echo "-2 (Increments for the range of betas to iterate) is empty";
fi

if [ -z "$end_b" ]
then
   echo "-3 (End beta for the range of betas to iterate) is empty";
fi

if [ -z "$start_g" ]
then
   echo "-4 (Starting gamma for the range of gammas to iterate) is empty";
fi

if [ -z "$incre_g" ]
then
   echo "-5 (Increments for the range of gammas to iterate) is empty";
fi

if [ -z "$end_g" ]
then
   echo "-6 (End gamma for the range of gammas to iterate) is empty";
fi

if [ -z "$segmentation_dims" ]
then
   echo "-d (Segmentation dimension; column width of window) is empty";
fi

if [ -z "$patch_dims" ]
then
   echo "-p (Patch size for higher resolution SAF) is empty";
fi

if [ -z "$SAF_dims" ]
then
   echo "-f (Patch size for lower resolution SAF) is empty";
fi

if [ -z "$logbasedir" ]
then
   echo "-l (Directory to save logfiles) is empty";
fi

if [ -z "$svs_path" ] || [ -z "$root_dir" ] || [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ] || [ -z "$4" ] || [ -z "$5" ] || [ -z "$6" ] || [ -z "$segmentation_dims" ] || [ -z "$SAF_dims" ] || [ -z "$logbasedir" ] || [ -z "$alpha" ]
then
    exit 1
fi
##===========Input===========##

#parameters
config="artefact"

#directories
slice_basename=$(basename $svs_path) #get the slice name
slice_basename=${slice_basename%.*}

# announcements
echo "Subject: $slice_basename"
echo "Config: $config"
echo ""

##===========Cluster options=============##

#cluster parameters
logdir="$logbasedir/$slice_basename/$config/iterate_betas_and_gammas"
r=1

#make output directory
if ! [ -d $root_dir ]; then
  mkdir -p $root_dir
fi

# remove logfiles if present
if [ -d $logdir ]; then
    if [ $r -eq 1 ]; then
      rm -r $logdir
    else
      echo "Folder with log and text files already exists"
      echo "See $logdir or select remove with -r"
      exit 1
    fi
fi

mkdir -p $logdir


##===========Running pipeline=============##


local_dir="$root_dir/$slice_basename/$config/local_colour_matrix_and_MAD" #get local colour matrix matrix first, if necessary
echo python get_local_colour_matrix_and_MAD.py --svs_path $svs_path --local_dir $local_dir --seg_dim $segmentation_dims>>$logdir/local_colour_matrix_and_MAD.txt

echo ""
echo "Generating thresholds and SAFs for..."
echo ""

## Iterate over betas to generate thresholds first

for beta in $(seq $start_b $incre_b $end_b);

  do

    echo ""
    echo "--- Beta : $beta ---"
    echo ""

    threshold_dir="$root_dir/$slice_basename/$config/thresholds/$alpha/$beta"
    echo python get_dab_thresholds.py --svs_path $svs_path --cm_dir $local_dir --thr_dir $threshold_dir --seg_dim $segmentation_dims --alpha $alpha --beta $beta --config $config>>$logdir/thresholds.txt

    ## Iterate over gammas to generate SAF maps

    for gamma in $(seq $start_g $incre_g $end_g);

      do

        echo ""
        echo "Gamma : $gamma"
        echo ""

        SAFmap_dir="$root_dir/$slice_basename/$config/SAF/$alpha/$beta/$gamma"
        echo python get_SAF_map.py --svs_path $svs_path --cm_dir $local_dir --thr_dir $threshold_dir --saf_dir $SAFmap_dir --seg_dim $segmentation_dims --patch_dim $patch_dims --saf_patch_dim $SAF_dims --gamma $gamma --config $config>>$logdir/SAF.txt

    done

done

## Grid search to find optimal SAF map

echo python grid_search.py --safs_dir "$root_dir/$slice_basename/$config/SAF/$alpha" --remove>>$logdir/grid_search.txt


#print text file just to be sure
echo "~~Printing text files to be submitted~~"
echo ""
echo "Colour matrix derivation"
echo ""
cat $logdir/local_colour_matrix_and_MAD.txt
echo ""
echo "DAB thresholds"
echo ""
cat $logdir/thresholds.txt
echo ""
echo "SAF maps"
echo ""
cat $logdir/SAF.txt
echo ""
echo "Grid search"
echo ""
cat $logdir/grid_search.txt
echo ""
echo "~~~"

# submit the textfiles as jobs to queues
jid_cm=$(fsl_sub -q long.q -l $logdir -t $logdir/local_colour_matrix_and_MAD.txt)
jid_thr=$(fsl_sub -q long.q -j $jid_cm -l $logdir -t $logdir/thresholds.txt)
jid_saf=$(fsl_sub -q long.q -j $jid_thr -l $logdir -t $logdir/SAF.txt)
fsl_sub -q short.q -j $jid_saf -l $logdir -t $logdir/grid_search.txt
