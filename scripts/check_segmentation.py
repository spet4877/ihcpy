#!/usr/bin/env python

"""

This script takes the .svs slide and DAB threshold, and outputs example segmentation.

"""

import os
import sys

from skimage import img_as_int

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

import argparse
from os.path import join, exists
import numpy as np
from modules import histoSAF, dir_utils, maps, stain_utils
import matplotlib.pyplot as plt


def main():
    p = argparse.ArgumentParser(description="Generate segmentation from WSI")

    """directories"""

    p.add_argument('-sp', '--svs_path',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='path of the svs file')
    p.add_argument('-cm', '--cm_dir',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='directory containing GLOBAL colour matrix for the stain')
    p.add_argument('-td', '--thr_dir',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='directory to save the thresholds to')
    p.add_argument('-segd', '--seg_dir',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='directory to save the thresholds to')

    """parameters"""

    p.add_argument('-sd', '--seg_dim',
                   required=True, nargs='+', type=int,
                   help='dimension of segmentation window')
    p.add_argument('-pd', '--patch_dim',
                   required=True, nargs='+', type=int,
                   help='dimension of patch to store area; this is the patch map resolution (high resolution SAF map)')

    """Configuration type"""

    p.add_argument('-c', '--config',
                   required=True, nargs='+', type=str,
                   help='default or artefact-corrected')


    # parse arguments
    args = p.parse_args()
    args.svs_path = ' '.join(args.svs_path)
    args.thr_dir = ' '.join(args.thr_dir)
    args.cm_dir = ' '.join(args.cm_dir)
    args.seg_dir = ' '.join(args.seg_dir)

    dim_window, dim_patch = np.array(args.seg_dim), \
                            np.array(args.patch_dim)

    # parse directories
    svs_fname, thr_dir, cm_dir, seg_dir = args.svs_path, args.thr_dir, args.cm_dir, args.seg_dir
    config = args.config[0]

    # some assertions; check beta is available if artefact-corrected configuration

    assert config in ["default", "artefact"], "Configuration must be either default or artefact (given: {})".format(
        config)

    """Set different masks opts"""


    if config in ["default"]:

        cell_mask_opts = {"default_threshold": True,
                          "median": [2, 2]}  # median filter for cell mask; needed for x40 slides

    elif config in ["artefact"]:

        cell_mask_opts = {"external_dab_threshold_map": True,  # local threshold map that has been adjusted w/ MAD
                          "columnwise_colourmatrix": True,  # local colour matrix map (columnwise here)
                          "median": [2, 2]}

    tissue_mask_opts = {}

    dir_utils.dict_to_json(fname="masks_opts",
                           output_dir=thr_dir,
                           data=[cell_mask_opts, tissue_mask_opts])

    """Initialize histoSAF object and segmentation window dimensions"""

    # create histosaf object
    histosaf = histoSAF.histoSAF(svs_dir=svs_fname,
                                 output_dir=seg_dir)

    # create dictionaries and coordinates for just the segmentation map.
    histosaf.segmentation_info(dim_window)

    """Load colour matrix"""

    # check colour matrix

    cm_file = join(cm_dir, "{}.npz".format(histosaf.metadata["name"]))
    assert exists(cm_file), "Colour matrix .npz file does not exist (used {})".format(cm_file)
    colourmatrix = np.load(cm_file)["a"]
    histosaf.metadata["colour_matrix"] = colourmatrix

    if config in ["artefact"]:  # if artefact configuration, load local colour matrix, MAD maps

        maps_data = join(cm_dir,
                         "maps_data.p")

        assert exists(maps_data), "Local colour matrix and MAD maps for {} do not exist".format(
            histosaf.metadata["name"])

        cell_mask_opts["colour_matrix_map"] = dir_utils.pickle_to_data(maps_data)["colour_matrix_map"]

    """Get DAB thresholds for cell mask"""

    ###=========Default=========###

    if config in ["default"]:
        thr_file = join(thr_dir,
                        "default_threshold.p")

        assert exists(thr_file), "No default threshold pickle (used {})".format(thr_file)

        thr_dict = dir_utils.pickle_to_data(thr_file)
        default_dab_threshold = thr_dict["default"]
        histosaf.metadata["default_thr_for_dab"] = default_dab_threshold

    ###=========Artefact=========###

    elif config in ["artefact"]:

        local_thr_file = join(thr_dir,
                              "dab_thresholds.p")
        assert exists(local_thr_file), "No local DAB threshold pickle (used {})".format(local_thr_file)

        cell_mask_opts["dab_threshold_map_RGB"] = dir_utils.pickle_to_data(local_thr_file)["dab_threshold_map"]

    """Create output directory"""

    dir_utils.create_folder(seg_dir)

    """Set up column & patch dimensions for segmentation"""

    """Column options"""

    full_coordinates = np.copy(histosaf.seg_map_info["coords_x"])
    slice_increments = int(dim_patch[0] / dim_window[0])

    """Coordinates (index) for columns"""

    first_start, second_start, third_start = np.argsort(full_coordinates)[int((len(full_coordinates) - 1) * (0.25))], \
                                             np.argsort(full_coordinates)[int((len(full_coordinates) - 1) * (0.50))], \
                                             np.argsort(full_coordinates)[int((len(full_coordinates) - 1) * (0.75))]

    first_end, second_end, third_end = first_start + slice_increments, \
                                       second_start + slice_increments, \
                                       third_start + slice_increments

    """Coordinates (actual) for patches"""
    height = histosaf.metadata["dimensions"][-1]
    y_coords = np.arange(0, height, dim_patch[1])
    patch_first_y_start, patch_second_y_start, patch_third_y_start = np.percentile(y_coords, 25), \
                                                                     np.percentile(y_coords, 50), \
                                                                     np.percentile(y_coords, 75)

    patch_first_y_end, patch_second_y_end, patch_third_y_end = patch_first_y_start + dim_patch[0], \
                                                               patch_second_y_start + dim_patch[0], \
                                                               patch_third_y_start + dim_patch[0]
    """Set up column options"""
    column_opts_1 = {"window_size": dim_window,
                     "stain_separation": "pNNLS",
                     "coords_x": full_coordinates[first_start: first_end]}
    column_opts_2 = {"window_size": dim_window,
                     "stain_separation": "pNNLS",
                     "coords_x": full_coordinates[second_start: second_end]}
    column_opts_3 = {"window_size": dim_window,
                     "stain_separation": "pNNLS",
                     "coords_x": full_coordinates[third_start: third_end]}

    """Get segmentation and save patches (can toggle with different options"""

    all_columns = [column_opts_1, column_opts_2, column_opts_3]
    patch_coordinates = [[patch_first_y_start, patch_first_y_end],
                         [patch_second_y_start, patch_second_y_end],
                         [patch_third_y_start, patch_third_y_end]]

    """    all_columns = [column_opts_2, column_opts_3] #50, 75
    patch_coordinates = [[patch_first_y_start, patch_first_y_end], #25, 75
                         [patch_third_y_start, patch_third_y_end]]

    #all_columns = [column_opts_1, column_opts_2]  # 25, 50
    all_columns = [column_opts_1]
    patch_coordinates = [[patch_first_y_start, patch_first_y_end],  # 25, 50
                         [patch_second_y_start, patch_second_y_end]]"""

    for col_index, column_opts in enumerate(all_columns):

        if config in ["default"]:

            total_RGB, total_mask, threshold_list = stain_utils.column_segmentation_with_default_threshold(
                histosaf.metadata,
                column_opts,
                tissue_mask_opts,
                cell_mask_opts)

        elif config in ["artefact"]:

            total_RGB, total_mask, threshold_list = stain_utils.column_segmentation_with_local_thresholds(
                histosaf.metadata,
                column_opts,
                tissue_mask_opts,
                cell_mask_opts)


        for patch_index, patch_coords in enumerate(patch_coordinates):
            patch_ = total_RGB[int(patch_coords[0]):int(patch_coords[1]), :, :]
            mask_ = total_mask[int(patch_coords[0]):int(patch_coords[1]), :]
            stain_utils.save_gif(patch_,
                                 mask_,
                                 fps=1,
                                 dir_=seg_dir,
                                 fname_="{}x_{}y.gif".format((col_index + 1) * 25, (patch_index + 1) * 25),
                                 mask_toggle=True)

            dir_utils.save_arr_to_png(patch_,
                                      dir_=seg_dir,
                                      name_="patch_{}_{}".format((col_index + 1) * 25, (patch_index + 1) * 25))
            dir_utils.save_arr_to_png(img_as_int(mask_),
                                      dir_=seg_dir,
                                      name_="mask_{}_{}".format((col_index + 1) * 25, (patch_index + 1) * 25))
        # don't store in memory
        del total_RGB
        del total_mask
        del threshold_list

    """Save outputs"""

    dir_utils.save_arr_to_png(histosaf.thumbnail,
                              dir_=seg_dir,
                              name_="thumbnail")

    dir_utils.dict_to_json(fname="masks_opts",
                           output_dir=seg_dir,
                           data=[cell_mask_opts, tissue_mask_opts])


if __name__ == '__main__':
    main()
