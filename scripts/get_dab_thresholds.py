#!/usr/bin/env python

"""
This script takes the .svs slide and computes a DAB threshold map
Assumes that colour matrix ("get_colour_matrix.py" for
default; "get_local_colour_matrix_and_MAD.py" for artefact) is  already computed.

:param svs_path: path of the .svs file
:param cm_dir: directory containing the colour matrix .npz
:param thr_dir: directory to save the DAB thresholds
:param seg_dim: Dimension of column or patch to perform segmentation
:param alpha: alpha parameter to regularize WOV segmentation (rule of thumb: lower value = more segmentation)
:param beta: weighting for striping correction; only used in the artefact configuration.
:param config: configuration to use; only 'default' or 'artefact'.

:return dab_thresholds.p: Pickle containing the dictionary with the DAB threshold map
:return default_thresholds.p: Pickle containing the dictionary with the default DAB threshold
:return threshold_map.png: plot showing the DAB thresholds along the slide, along with the default threshold.
:return masks_opts.json: json file containing different options for masks used.
"""

import os
import sys

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

import argparse
from os.path import join, exists
import numpy as np
from modules import histoSAF, dir_utils, maps
import matplotlib.pyplot as plt


def main():
    p = argparse.ArgumentParser(description="DAB threshold map for WSI")

    """directories"""

    p.add_argument('-sp', '--svs_path',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='path of the svs file')
    p.add_argument('-cm', '--cm_dir',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='directory containing colour matrix for the stain; can be the global OR local colour matrix map (and MAD map)')
    p.add_argument('-td', '--thr_dir',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='directory to save the thresholds to')

    """parameters"""

    p.add_argument('-sd', '--seg_dim',
                   required=True, nargs='+', type=int,
                   help='dimension of segmentation window')
    p.add_argument('-alpha', '--alpha',
                   required=True, nargs='+', type=float,
                   help='weighting for segmentation in WOV cost function')
    p.add_argument('-beta', '--beta',
                   required=False, nargs='+', type=float,
                   help='weighting for striping correction; only used in config that is artefact-corrected')

    """Configuration type"""

    p.add_argument('-c', '--config',
                   required=True, nargs='+', type=str,
                   help='default or artefact-corrected')

    # parse arguments
    args = p.parse_args()
    args.svs_path = ' '.join(args.svs_path)
    args.thr_dir = ' '.join(args.thr_dir)
    args.cm_dir = ' '.join(args.cm_dir)
    args.config = ' '.join(args.config)

    dim_window = np.array(args.seg_dim)
    alpha = np.array(args.alpha)[0]
    config = args.config

    # some assertions; check beta is available if artefact-corrected configuration

    assert config in ["default", "artefact"], "Configuration must be either default or artefact (given: {})".format(
        config)

    if config in ["artefact"]:
        beta = np.array(args.beta)[0]
        assert isinstance(beta, float), "Beta parameter is not a float ({})".format(beta)

    # parse directories
    svs_fname, thr_dir, cm_dir = args.svs_path, args.thr_dir, args.cm_dir

    """Check for threshold pickle"""

    output_checker = join(thr_dir, "dab_thresholds.p")

    if exists(output_checker):
        print("{} exists. Stopping get_dab_thresholds.py...".format(output_checker))
        return

    """Set different masks opts"""

    tissue_mask_opts = {"L_mask": 0.75}  # tissue mask to preprocess DAB channel prior to thresholding
    cell_mask_opts = {"WOV_otsu_RGB": [alpha, 256]}  # regularization parameter and number of bins

    if config in ["artefact"]:
        cell_mask_opts["MAD_weighting"] = beta

    """Initialize histoSAF object and segmentation window dimensions"""

    # create histosaf object
    histosaf = histoSAF.histoSAF(svs_dir=svs_fname,
                                 output_dir=thr_dir)

    histosaf.segmentation_info(dim_window)

    """Load colour matrix"""
    # check and/or compute colour matrix

    cm_file = join(cm_dir, "{}.npz".format(histosaf.metadata["name"]))
    assert exists(cm_file), "Colour matrix .npz file does not exist (used {})".format(cm_file)
    colourmatrix = np.load(cm_file)["a"]
    histosaf.metadata["colour_matrix"] = colourmatrix

    if config in ["artefact"]:  # if artefact configuration, load local colour matrix, MAD maps

        # check and/or compute colour matrix
        maps_data = join(cm_dir,
                         "maps_data.p")

        assert exists(maps_data), "Local colour matrix and MAD maps for {} do not exist".format(
            histosaf.metadata["name"])

        mad_map = dir_utils.pickle_to_data(maps_data)["mad_map"]
        colour_matrix_map = dir_utils.pickle_to_data(maps_data)["colour_matrix_map"]

    """Create output directory"""

    dir_utils.create_folder(thr_dir)

    """Get DAB threshold map"""

    if config in ["default"]:

        threshold, threshold_map = maps.dab_threshold_map_with_global_colourmatrix(histosaf.metadata,
                                                                                   histosaf.seg_map_info,
                                                                                   tissue_mask_opts,
                                                                                   cell_mask_opts,
                                                                                   histosaf.metadata["colour_matrix"])

    elif config in ["artefact"]:

        threshold, threshold_map = maps.dab_threshold_map_with_local_colourmatrix_and_MAD(histosaf.metadata,
                                                                                          histosaf.seg_map_info,
                                                                                          tissue_mask_opts,
                                                                                          cell_mask_opts,
                                                                                          mad_map,
                                                                                          colour_matrix_map,
                                                                                          cell_mask_opts[
                                                                                              "MAD_weighting"])

    """Save outputs"""

    dir_utils.data_to_pickle(fname="dab_thresholds",
                             output_dir=thr_dir,
                             data=threshold_map)

    dir_utils.data_to_pickle(fname="default_threshold",
                             output_dir=thr_dir,
                             data=threshold)

    dir_utils.dict_to_json(fname="masks_opts",
                           output_dir=thr_dir,
                           data=[cell_mask_opts, tissue_mask_opts])

    """Visualization"""

    plt.figure(figsize=(20, 12))
    plt.legend(fontsize=30)
    plt.plot(np.squeeze(threshold_map["dab_threshold_map"]), label="DAB threshold map")
    plt.ylim([0, np.max(threshold_map["dab_threshold_map"]) * 1.2])
    plt.axhline(threshold["default"], ls="--", lw=4, color="green",
                label="Default : {:.2f}".format(threshold["default"]))
    plt.legend(fontsize=30)
    plt.savefig(join(thr_dir, "threshold_map.png"))
    plt.close()


if __name__ == '__main__':
    main()
