#!/usr/bin/env python

"""
This script takes the .svs slide and computes a colour matrix (CM) and median absolute deviation (MAD) map of the slide.
Each CM and MAD value corresponds to the CM and MAD computed for each column/patch's DAB channel intensity values.

This is used to deal with 2 main artefacts:

1. Staining gradient artefact: the local colour matrix adjusts according to the staining gradient.
2. Striping/stitching artefact: the MAD changes based on the presence of stitching. We use MAD to adjust our thresholds accordingly.

:param svs_path: path of the .svs file
:param local_dir: directory to save the local colour matrix and MAD maps.

:return maps_data.p: pickled dictionary with the colour matrix map ("colour_matrix_map") and MAD map ("mad_map")
"""

import os
import sys

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

import matplotlib.pyplot as plt
import argparse
from os.path import join, exists
import numpy as np
from modules import histoSAF, dir_utils, maps, colour_matrix


def main():
    p = argparse.ArgumentParser(description="Local colour matrix & MAD maps for WSI")

    """directories"""

    p.add_argument('-sp', '--svs_path',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='Path of the svs file')
    p.add_argument('-ld', '--local_dir',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='directory to save local colour matrix and MAD map')

    """parameters"""

    p.add_argument('-sd', '--seg_dim',
                   required=True, nargs='+', type=int,
                   help='dimension of segmentation window')

    # parse arguments
    args = p.parse_args()
    args.svs_path = ' '.join(args.svs_path)
    args.local_dir = ' '.join(args.local_dir)

    dim_window = np.array(args.seg_dim)

    # parse directories
    svs_fname, local_dir = args.svs_path, args.local_dir

    print(svs_fname)

    # create histosaf object
    histosaf = histoSAF.histoSAF(svs_dir=svs_fname,
                                 output_dir=local_dir)

    # create dictionaries and coordinates for just the segmentation map.
    histosaf.segmentation_info(dim_window)

    """Set different opts"""

    tissue_mask_opts = {"L_mask": 0.75}  # preprocessing DAB channels for computing MAD
    local_colour_matrix_opts = {"option": "kmeans",
                                "tissue_mask": [1, 0.75]}

    """Compute global colour matrix (used for hematoxylin masking)"""

    # check and/or compute colour matrix
    cm_file = join(local_dir, "{}.npz".format(histosaf.metadata["name"]))

    if not exists(cm_file):  # colour matrix does not exist
        dir_utils.create_folder(local_dir)

        images_array = colour_matrix.extract_patches(svs_fname,
                                                     size=128,
                                                     n=1000,
                                                     option=1,
                                                     threshold_values=[0.85, 0, 0.50, 0.50],
                                                     verbose=True)

        colourmatrix = colour_matrix.k_means(images_array,
                                             verbose=True,
                                             threshold_value=0.85)

        np.savez_compressed(join(local_dir, "{}.npz".format(histosaf.metadata["name"])),
                            a=colourmatrix)

        np.savetxt(join(local_dir, "{}.txt".format(histosaf.metadata["name"])), colourmatrix)

    """Compute local colour matrix and MAD maps"""

    local_cm_file = join(local_dir, "maps_data.p")

    if not exists(local_cm_file):  # local colour matrix does not exist
        object_maps = maps.local_colour_matrix_and_MAD_map(histosaf.metadata,
                                                           histosaf.seg_map_info,
                                                           tissue_mask_opts,
                                                           local_colour_matrix_opts)

        """Save results"""

        dir_utils.data_to_pickle(fname="maps_data",
                                 output_dir=local_dir,
                                 data=object_maps)

    object_maps = dir_utils.pickle_to_data(local_cm_file)

    """Save options to JSON for reference"""

    dir_utils.dict_to_json(fname="masks_opts",
                           output_dir=local_dir,
                           data=[tissue_mask_opts])

    """(Visualization) Thumbnail of the WSI"""

    dir_utils.save_arr_to_png(histosaf.thumbnail,
                              dir_=local_dir,
                              name_="thumbnail")

    """(Visualization) Colour matrix map """

    colour_matrix_map = np.squeeze(object_maps["colour_matrix_map"])

    plt.figure(figsize=(20, 12))
    plt.title("DAB colour vector", fontsize=30)
    plt.plot(colour_matrix_map[:, 1, 0], label="Red", color="red", lw=4)
    plt.plot(colour_matrix_map[:, 1, 1], label="Green", color="green", lw=4)
    plt.plot(colour_matrix_map[:, 1, 2], label="Blue", color="blue", lw=4)
    plt.savefig(join(local_dir, "dab_colour_vector.png"))
    plt.close()

    plt.figure(figsize=(20, 12))
    plt.title("Hema colour vector", fontsize=30)
    plt.plot(colour_matrix_map[:, 0, 0], label="Red", color="red", lw=4)
    plt.plot(colour_matrix_map[:, 0, 1], label="Green", color="green", lw=4)
    plt.plot(colour_matrix_map[:, 0, 2], label="Blue", color="blue", lw=4)
    plt.savefig(join(local_dir, "hema_colour_vector.png"))
    plt.close()

    """(Visualization) MAD map  """

    plt.figure(figsize=(20, 12))
    plt.plot(np.squeeze(object_maps["mad_map"]), label="Column-wise MAD")

    plt.legend(fontsize=30)
    plt.ylim([0, np.max(object_maps["mad_map"]) * 1.2])
    plt.savefig(join(local_dir, "mad_plot.png"))
    plt.close()


if __name__ == '__main__':
    main()
