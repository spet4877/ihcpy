"""
Module for evaluating SAF maps. This includes:
- Within-slide variation: Horizontal SAF maps (normalized to mean) along with metrics to evaluate their striping and staining gradient

"""

from __future__ import division

import matplotlib.pyplot as plt
import numpy as np
import scipy

"""Horizontal plot before generating metrics"""

def horplot_and_COV(saf_map_opts):
    from numpy.ma import masked_array

    # get SAF map and tissue mask
    safmap = saf_map_opts["SAF_map"]
    tissuemask = saf_map_opts["tissue_mask"]

    # mask SAF map
    masked_SAF = masked_array(safmap, np.logical_not(tissuemask))

    # compute valid pixels for weighting
    valid_pixels_ = np.sum(~masked_SAF.mask, axis=0)
    weights_ = valid_pixels_ / np.max(valid_pixels_)

    # weight horizontal plot (normalized to mean)
    weighted_horplot = weights_ * masked_SAF.mean(axis=0) / (masked_SAF.mean())

    # COV
    cov = np.std(weighted_horplot) / np.mean(weighted_horplot)

    # SAF

    saf = {"masked_SAF": masked_SAF,
           "horplot": masked_SAF.mean(axis=0) / (masked_SAF.mean()),
           "weighting": weights_,
           "weighted_horplot": weighted_horplot}

    return saf, cov

"""Butterworth filter for generating metrics"""

def butter_highpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = scipy.signal.butter(order, normal_cutoff, btype='high', analog=False)
    return b, a

def butter_highpass_filter(data, cutoff, fs, order=5):
    b, a = butter_highpass(cutoff, fs, order=order)
    y = scipy.signal.filtfilt(b, a, data)
    return y

"""Metrics"""

def characteristics_of_striping(saf_map_opts,
                                high_pass_filters_opts,
                                plot=False):
    """Takes the unmasked SAF map and tissue mask as input.
    It processes the SAF map to produce a weighted horizontal plot of the SAF.
    It then filters it with 2 high-pass filters to keep the striping contribution to the signal.

    The base assumption here is that the striping frequency is observed to be constant within-subject and
    between-subject (approx. 0.26 of all frequency bins, which the highest bin equated to the Nyquist frequency).

    It outputs the characteristics associated with with the striping frequency, such as STD or power coefficient"""

    horplots, cov = horplot_and_COV(saf_map_opts)

    horplot = horplots["weighted_horplot"]

    # filter options

    perc_lower = high_pass_filters_opts["cutoff_lower"]  # lower frequency of bandwidth where striping frequency lives.
    perc_upper = high_pass_filters_opts["cutoff_upper"]  # higher frequency of bandwidth where striping frequency is
    order = high_pass_filters_opts["order"]  # higher order, steeper cutoff?
    fs = high_pass_filters_opts["sample_rate"]
    cutoff_lower = perc_lower * (0.5 * fs)
    cutoff_upper = perc_upper * (0.5 * fs)

    # Filter the data, and plot both the original and filtered signals.
    yl = butter_highpass_filter(horplot, cutoff_lower, fs, order)
    yu = butter_highpass_filter(horplot, cutoff_upper, fs, order)

    low_frequency_residual = horplot - yl  # the 'lower' frequency components
    striping_frequency_interval = yl - yu  # keep the signals that fall within the frequency bandwidth where the striping frequency lives.

    # make sure that the mean of the higher frequency signals is 0 (i.e. this means that the lower frequency terms are accuracy, and this just adds to it)

    if (np.isnan(np.mean(yl))) | np.isnan(np.mean(yu)) | np.isnan(np.mean(striping_frequency_interval)):
        mean_first, mean_second, mean_sfi = 0, 0, 0
        print(yl)
        print(yu)
        print(striping_frequency_interval)
    else:
        mean_first, mean_second, mean_sfi = int(np.round(np.mean(yl))), int(np.round(np.mean(yu))), int(
            np.round(np.mean(striping_frequency_interval)))

    assert (mean_first == 0) & (mean_second == 0) & (mean_sfi == 0), "The mean of the high-pass filtered data is not " \
                                                                     "0. Check your cut-offs selected. "

    # compute the STD for higher frequency terms, and COV for the lower frequency term.
    from scipy.stats import median_absolute_deviation

    mad_first, mad_second, mad_sfi = median_absolute_deviation(yl), median_absolute_deviation(
        yu), median_absolute_deviation(
        striping_frequency_interval)
    std_first, std_second, std_sfi = np.std(yl), np.std(yu), np.std(striping_frequency_interval)

    COV_lower, std_lower, mean_lower = (np.std(low_frequency_residual) / np.mean(low_frequency_residual)), np.std(
        low_frequency_residual), np.mean(low_frequency_residual)

    # compute the power terms associated with the filtered and unfiltered data

    """    y_first = scipy.fft.rfft(yl) / np.sum(scipy.fft.rfft(yl))
    y_second = scipy.fft.rfft(yu) / np.sum(scipy.fft.rfft(yu))
    y_res = scipy.fft.rfft(low_frequency_residual) / np.sum(scipy.fft.rfft(low_frequency_residual))
    unfilt_y = scipy.fft.rfft(horplot) / np.sum(scipy.fft.rfft(horplot))
    y_isolated = scipy.fft.rfft(striping_frequency_interval) / np.sum(scipy.fft.rfft(striping_frequency_interval))"""

    from scipy.integrate import trapz
    normalization_ = np.sum(np.abs(scipy.fft.rfft(horplot)))
    normalization_ = trapz(np.abs(scipy.fft.rfft(horplot)), dx=1)

    y_first = np.abs(scipy.fft.rfft(yl)) / normalization_
    y_second = np.abs(scipy.fft.rfft(yu)) / normalization_
    y_res = np.abs(scipy.fft.rfft(low_frequency_residual)) / normalization_
    unfilt_y = np.abs(scipy.fft.rfft(horplot)) / normalization_
    y_isolated = np.abs(scipy.fft.rfft(striping_frequency_interval)) / normalization_

    # print((scipy.fft.rfft(striping_frequency_interval)))

    frequency_bins = scipy.fft.rfftfreq(horplot.shape[0], 1 / (fs))

    # pick out the exact power terms that correspond to the highest power in the striping frequency interval (i.e. presumably power term of the striping frequency)

    max_power_freq = frequency_bins[np.argmax(np.abs(y_isolated))]

    max_power = np.max(np.abs(y_isolated))

    # find nearest frequency
    def find_nearest(array, value):
        array = np.asarray(array)
        idx = (np.abs(array - value)).argmin()
        return array[idx], idx

    nearest_striping_frequency, idx = find_nearest(frequency_bins, 0.26 * (0.5 * fs))
    power_of_set_frequency = np.abs(y_isolated[idx])

    total_power_in_interval = trapz(np.abs(y_isolated), dx=1)
    # print(total_power_in_interval)
    # print(trapz(np.abs(y_isolated),dx=1))

    # the 4 metrics to try

    var_r = np.mean(np.absolute(np.gradient(yl)))
    std_r = np.std(yl)

    var_bp = np.mean(np.absolute(np.gradient(yl - yu)))
    std_bp = np.std(yl - yu)

    var_l = np.mean(np.absolute(np.gradient(horplot - yl)))
    std_l = np.std(horplot - yl)



    """    striping_char = {"mean": mean_sfi,
                     "mad": mad_sfi,
                     "STD": std_sfi,
                     "striping_frequency": max_power_freq / (0.5 * fs),
                     "striping_plot": striping_frequency_interval,
                     "total_power_integral_plot": cumtrapz(np.abs(y_isolated)),  # don't need
                     "all_frequency_components": np.abs(scipy.fft.rfft(horplot)),
                     "power_of_set_frequency": power_of_set_frequency,
                     "horplot": horplot,
                     "yl": yl}"""

    striping_char = {"var_r": var_r,
                     "std_r": std_r,
                     "var_bp": var_bp,
                     "std_bp": std_bp,
                     "horplot" : horplot,
                     "bandpass": striping_frequency_interval,
                     "residual": yl}

    first_highpass_char = {"mean": mean_first,
                           "STD": std_first,
                           "mad": mad_first}

    second_highpass_char = {"mean": mean_second,
                            "STD": std_second,
                            "mad": mad_second}

    lower_frequency_char = {"mean": mean_lower,
                            "STD": std_l,
                            "COV": COV_lower,
                            "var_l": var_l}

    non_striping_char = {"lower": lower_frequency_char,
                         "first": first_highpass_char,
                         "second": second_highpass_char,
                         "destriped_plot": horplot - striping_frequency_interval}

    # plot to frequency reponse of the 2 high-pass filters, the spatial domain of signals and the frequency domain:

    figures = {}
    if plot:
        import matplotlib as mpl
        mpl.rcParams["figure.facecolor"] = "FFFFFF"
        mpl.rcParams["axes.facecolor"] = "FFFFFF"
        mpl.rcParams['font.family'] = 'Times'
        mpl.rcParams['axes.labelcolor'] = "000000"
        mpl.rcParams['axes.edgecolor'] = "000000"

        mpl.rcParams['ytick.color'] = "000000"
        mpl.rcParams['xtick.color'] = "000000"
        mpl.rcParams['ytick.labelsize'] = 30
        mpl.rcParams['xtick.labelsize'] = 30

        # mpl.rcParams["savefig.facecolor"]
        # frequency reponse function numerator and denominators for the 2 high-pass filters (for plotting)

        bl, al = butter_highpass(cutoff_lower, fs,
                                 order)  # numerator and denominator of the frequency response function
        bu, au = butter_highpass(cutoff_upper, fs, order)

        """Plot the frequency response"""
        """        
        wl, hl = scipy.signal.freqz(bl, al,
                                    worN=8000)  # omega here is the frequency spanned, relative to the Nyquist frequency [0 to pi], in rad/s
        wu, hu = scipy.signal.freqz(bu, au, worN=8000)
        plt.subplot(3, 1, 1)
        plt.plot(wl / np.pi, np.abs(hl), 'b', label="First high-pass")
        plt.plot(wu / np.pi, np.abs(hu), 'w', label="Second high-pass")
        # plt.plot(cutoff, 0.5 * np.sqrt(2), 'ko')
        # plt.axvline(cutoff/(0.5*fs), color='r')
        # plt.xlim(0, 0.5 * fs)
        plt.title("Filter Frequency Response (sampling rate of {} Hz)".format(fs), fontsize=20)
        plt.xlabel('Frequency normalized to half of sampling rate')
        plt.axvline(cutoff_lower / (0.5 * fs), color="saddlebrown", lw=1, ls="-.",
                    label="First cut-off freq.: {:.3f}".format(cutoff_lower / (0.5 * fs)))
        plt.axvline(cutoff_upper / (0.5 * fs), color="red", lw=1, ls="-.",
                    label="Second cut-off freq.: {:.3f}".format(cutoff_upper / (0.5 * fs)))
        plt.grid()
        plt.legend(fontsize=20)"""

        """Plot the spatial domain """

        """        n = horplot.shape[0]  # total number of samples
        N = np.arange(0, n, 1)

        fig1 = plt.figure(figsize=(12, 8))
        plt.title("Spatial Domain -- SAF horizontal plots", fontsize=20)

        plt.plot(N, horplot, 'r-', label='Unfiltered SAF horizontal plot')
        plt.plot(N, yl, "b-", linewidth=2,
                 label='filtered data (high-pass); mean +/- STD = {:.2f} +/- {:.5f}; MAD = {:.5f}'.format(mean_first,
                                                                                                          std_first,
                                                                                                          mad_first))
        plt.plot(N, yu, "w-", alpha=0.7, linewidth=2,
                 label='filtered data (second high-pass); mean +/- STD = {:.2f} +/- {:.5f}'.format(mean_second,
                                                                                                   std_second))

        plt.plot(N, low_frequency_residual, "m-", linewidth=2,
                 label='residual (low frequencies); COV = {:.2f}'.format(COV_lower))

        plt.plot(N, striping_frequency_interval, "g-", linewidth=4,
                 label="isolated stripe frequency; mean +/- STD = {:.2f} +/- "
                       "{:.5f}".format(
                     mean_sfi, std_sfi))
        plt.xlabel('Index of WSI', fontsize=20)
        plt.xticks(fontsize=20)
        plt.legend(fontsize=18)
        plt.close()"""

        n = horplot.shape[0]  # total number of samples
        N = np.arange(0, n, 1)

        fig1 = plt.figure(figsize=(12, 8))
        # plt.title("Spatial Domain -- SAF horizontal plots", fontsize=20)

        plt.plot(N, horplot, 'r-', linewidth=4, label='raw SAF')

        plt.plot(N, yl, "b-", linewidth=4, ls="dashed",
                 label='High-pass filtered (HPF) SAF; std = {:.4f}'.format(np.std(yl)))
        """        plt.plot(N, yu, "w-", alpha=0.7, linewidth=2,
                 label='filtered data (second high-pass); mean +/- STD = {:.2f} +/- {:.5f}'.format(mean_second,
                                                                                                   std_second))"""

        plt.plot(N, low_frequency_residual, "m-", linewidth=4,
                 label='Residual (raw SAF - HPF SAF)')

        plt.plot(N, striping_frequency_interval, "saddlebrown", linewidth=2,
                 label='Bandwidth of striping; std = {:.4f}'.format(std_sfi))

        """        plt.plot(N, striping_frequency_interval, "g-", linewidth=4,
                 label="Stripe frequency")"""

        # y = np.sin(2 * np.pi * 0.26 * 0.5 * fs * N / (fs) + np.pi) / 10
        # plt.plot(N, y, "g-", linewidth=4,
        #         label='Striping frequency')

        plt.xlabel('Index of WSI', fontsize=30)
        plt.ylabel('Averaged SAF', fontsize=30)
        plt.xticks(fontsize=20)
        plt.legend(fontsize=18)
        plt.yticks(fontsize=20)

        ax = plt.gca()
        handles, labels = ax.get_legend_handles_labels()
        # When creating the legend, only use the first two elements
        # to effectively remove the last two.
        l = plt.legend(handles, labels)

        for text in l.get_texts():
            text.set_color("black")
            text.set_fontsize(25)

        plt.close()

        """Plot the frequency domain of filtered data"""
        # plt.subplot(2, 1, 2)

        fig2 = plt.figure(figsize=(12, 8))
        plt.title("Frequency Domain ", fontsize=20, color="black")

        # plt.plot(frequency_bins / (0.5 * fs), np.abs(y_isolated), color="b", label="bandwidth")

        plt.plot(frequency_bins / (0.5 * fs), np.abs(y_first), color="b", label="filtered data (high-pass)")
        plt.plot(frequency_bins / (0.5 * fs), np.abs(y_res), color="m", label="residual (low frequencies)")
        # plt.plot(frequency_bins / (0.5 * fs), np.abs(y_isolated), color="g",
        #         label="isolated stripe frequency")

        # plt.axvline(max_power_freq / (0.5 * fs), color="g", lw=0.8, ls="-.",
        #            label="Identified striping frequency: {:.2f}; power : {:.4f}".format(max_power_freq / (0.5 * fs),
        #                                                                                 max_power))

        # plt.axvline(nearest_striping_frequency / (0.5 * fs), color="magenta", lw=0.8, ls="-.",
        #            label="Power of set striping freq {:.2f}.: {:.4f}".format(nearest_striping_frequency / (0.5 * fs),
        #                                                                      power_of_set_frequency))
        plt.axvline(cutoff_lower / (0.5 * fs), color="saddlebrown", lw=0.8, ls="-.", label="First cut-off freq.")
        plt.axvline(cutoff_upper / (0.5 * fs), color="r", lw=0.8, ls="-.", label="Second cut-off freq. ; total power "
                                                                                 "in interval : {:.4f}".format(
            total_power_in_interval))
        plt.legend(fontsize=20)
        plt.xlabel('Frequency normalized to 0.5 of sampling rate', fontsize=20)

        # plt.subplots_adjust(left=None, bottom=0, right=None, top=1.4, wspace=None, hspace=None)
        plt.ylim([-0.001, 0.005])
        plt.xticks(fontsize=20)

        ax = plt.gca()
        handles, labels = ax.get_legend_handles_labels()
        # When creating the legend, only use the first two elements
        # to effectively remove the last two.
        l = plt.legend(handles, labels)

        for text in l.get_texts():
            text.set_color("black")
            text.set_fontsize(25)

        plt.close()

        figures["spatial"] = fig1
        figures["frequency"] = fig2

    return striping_char, non_striping_char, figures