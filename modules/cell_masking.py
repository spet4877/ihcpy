#!/usr/bin/env python
"""


Module to perform the actual masking of cells. Contains multiple methods for thresholding. The main one used is WOV Otsu in RGB intensity space ("WOV_Otsu_RGB")
This module also has the manual thresholds hardcoded for the reproducicibility and MND dataset.

"""
import numpy as np
from skimage.segmentation import random_walker

"""Settings for manual thresholds"""

"""Reproducibility"""
def CD68_DAB_thresholds():
    """These are the thresholds set by Adele Smart for
    the CD68 reproducibility data.

    There are 3 thresholds, which are the upper intensity limits for the DAB stains.
    The DAB stains here are assumed to be converted from absorbance to RGB space (i.e. OD_to_RGB),
    then converted to 8-bit grayscale values (0,255).

    Dark pixels are DAB, and white pixels are non-tissue here (i.e. so DAB pixels < threshold)

    Weak = weakly stained pixels
    Medium = medium stained pixels
    Strong = darkly stained pixels

    Instrument: Aperio ScanScope AT Turbo (Leica Biosystems)
    Software: ImageScope software (v12.3.2.8013)"""
    weak_ = 215
    medium_ = 171
    strong_ = 117

    return [weak_, medium_, strong_]

def PLP_DAB_thresholds():
    """These are the thresholds set by Adele Smart for
    the PLP reproducibility data.

    There are 3 thresholds, which are the upper intensity limits for the DAB stains.
    The DAB stains here are assumed to be converted from absorbance to RGB space (i.e. OD_to_RGB),
    then converted to 8-bit grayscale values (0,255).

    Dark pixels are DAB, and white pixels are non-tissue here (i.e. so DAB pixels < threshold)

    Weak = weakly stained pixels
    Medium = medium stained pixels
    Strong = darkly stained pixels

    Instrument: Aperio ScanScope AT Turbo (Leica Biosystems)
    Software: ImageScope software (v12.3.2.8013)"""
    weak_ = 212
    medium_ = 182
    strong_ = 109

    return [weak_, medium_, strong_]

def ferritin_DAB_thresholds():
    """These are the thresholds set by Adele Smart for
    the ferritin reproducibility data.

    There are 3 thresholds, which are the upper intensity limits for the DAB stains.
    The DAB stains here are assumed to be converted from absorbance to RGB space (i.e. OD_to_RGB),
    then converted to 8-bit grayscale values (0,255).

    Dark pixels are DAB, and white pixels are non-tissue here (i.e. so DAB pixels < threshold)

    Weak = weakly stained pixels
    Medium = medium stained pixels
    Strong = darkly stained pixels

    Instrument: Aperio ScanScope AT Turbo (Leica Biosystems)
    Software: ImageScope software (v12.3.2.8013)"""
    weak_ = 189
    medium_ = 163
    strong_ = 138

    return [weak_, medium_, strong_]

def SMI312_DAB_thresholds():
    """These are the thresholds set by Adele Smart for
    the ferritin reproducibility data.

    There are 3 thresholds, which are the upper intensity limits for the DAB stains.
    The DAB stains here are assumed to be converted from absorbance to RGB space (i.e. OD_to_RGB),
    then converted to 8-bit grayscale values (0,255).

    Dark pixels are DAB, and white pixels are non-tissue here (i.e. so DAB pixels < threshold)

    Weak = weakly stained pixels
    Medium = medium stained pixels
    Strong = darkly stained pixels

    Instrument: Aperio ScanScope AT Turbo (Leica Biosystems)
    Software: ImageScope software (v12.3.2.8013)"""
    weak_ = 193
    medium_ = 160
    strong_ = 105

    return [weak_, medium_, strong_]

def Iba1_DAB_thresholds():
    """These are the thresholds set by Adele Smart for
    the Iba1 reproducibility data.

    There are 3 thresholds, which are the upper intensity limits for the DAB stains.
    The DAB stains here are assumed to be converted from absorbance to RGB space (i.e. OD_to_RGB),
    then converted to 8-bit grayscale values (0,255).

    Dark pixels are DAB, and white pixels are non-tissue here (i.e. so DAB pixels < threshold)

    Weak = weakly stained pixels
    Medium = medium stained pixels
    Strong = darkly stained pixels

    Instrument: Aperio ScanScope AT Turbo (Leica Biosystems)
    Software: ImageScope software (v12.3.2.8013)"""
    weak_ = 215
    medium_ = 171
    strong_ = 117

    return [weak_, medium_, strong_]

"""MND"""

def MND_PLP_DAB_thresholds():
    """These are the thresholds set by Adele Smart for
    the PLP MND data.

    There are 3 thresholds, which are the upper intensity limits for the DAB stains.
    The DAB stains here are assumed to be converted from absorbance to RGB space (i.e. OD_to_RGB),
    then converted to 8-bit grayscale values (0,255).

    Dark pixels are DAB, and white pixels are non-tissue here (i.e. so DAB pixels < threshold)

    Weak = weakly stained pixels
    Medium = medium stained pixels
    Strong = darkly stained pixels

    Instrument: Aperio ScanScope AT Turbo (Leica Biosystems)
    Software: ImageScope software (v12.3.2.8013)"""
    weak_ = 219
    medium_ = 186
    strong_ = 109

    return [weak_, medium_, strong_]

def MND_CD68_DAB_thresholds():
    """These are the thresholds set by Adele Smart for
    the CD68 MND data.

    There are 3 thresholds, which are the upper intensity limits for the DAB stains.
    The DAB stains here are assumed to be converted from absorbance to RGB space (i.e. OD_to_RGB),
    then converted to 8-bit grayscale values (0,255).

    Dark pixels are DAB, and white pixels are non-tissue here (i.e. so DAB pixels < threshold)

    Weak = weakly stained pixels
    Medium = medium stained pixels
    Strong = darkly stained pixels

    Instrument: Aperio ScanScope AT Turbo (Leica Biosystems)
    Software: ImageScope software (v12.3.2.8013)"""
    weak_ = 220
    medium_ = 183
    strong_ = 116

    return [weak_, medium_, strong_]

def MND_Iba1_DAB_thresholds():
    """These are the thresholds set by Adele Smart for
    the PLP reproducibility data.

    There are 3 thresholds, which are the upper intensity limits for the DAB stains.
    The DAB stains here are assumed to be converted from absorbance to RGB space (i.e. OD_to_RGB),
    then converted to 8-bit grayscale values (0,255).

    Dark pixels are DAB, and white pixels are non-tissue here (i.e. so DAB pixels < threshold)

    Weak = weakly stained pixels
    Medium = medium stained pixels
    Strong = darkly stained pixels

    Instrument: Aperio ScanScope AT Turbo (Leica Biosystems)
    Software: ImageScope software (v12.3.2.8013)"""
    weak_ = 220
    medium_ = 183
    strong_ = 116

    return [weak_, medium_, strong_]

def MND_SMI312_DAB_thresholds():
    """These are the thresholds set by Adele Smart for
    the PLP reproducibility data.

    There are 3 thresholds, which are the upper intensity limits for the DAB stains.
    The DAB stains here are assumed to be converted from absorbance to RGB space (i.e. OD_to_RGB),
    then converted to 8-bit grayscale values (0,255).

    Dark pixels are DAB, and white pixels are non-tissue here (i.e. so DAB pixels < threshold)

    Weak = weakly stained pixels
    Medium = medium stained pixels
    Strong = darkly stained pixels

    Instrument: Aperio ScanScope AT Turbo (Leica Biosystems)
    Software: ImageScope software (v12.3.2.8013)"""
    weak_ = 193
    medium_ = 160
    strong_ = 105

    return [weak_, medium_, strong_]

"""Threshold-based methods """
"""WOV otsu"""

def WOV_otsu(img_to_cell_mask,
             tissue_mask,
             WOV_otsu_opts):
    # some checks
    from skimage.exposure import histogram
    from skimage._shared.utils import warn

    if img_to_cell_mask.ndim > 2 and img_to_cell_mask.shape[-1] in (3, 4):
        msg = "threshold_otsu is expected to work correctly only for " \
              "grayscale images; image shape {0} looks like an RGB image"
        warn(msg.format(img_to_cell_mask.shape))

    # parse parameters needed for WOV Otsu
    reg_power, nbins = WOV_otsu_opts[0], WOV_otsu_opts[1]

    # mask the densities before finding the threshold
    img = np.copy(img_to_cell_mask)
    img = img[tissue_mask > 0]
    img = img[img > 0]  # you do mask for 0s.
    #img = img[img < 255] # for the RGB cases

    hist, bin_centers = histogram(img.ravel(), nbins, source_range='image')
    hist = hist.astype(float)

    # class probabilities for all possible thresholds
    weight1 = np.cumsum(hist)
    weight2 = np.cumsum(hist[::-1])[::-1]
    # class means for all possible thresholds
    mean1 = np.cumsum(hist * bin_centers) / weight1
    mean2 = (np.cumsum((hist * bin_centers)[::-1]) / weight2[::-1])[::-1]

    # Clip ends to align class 1 and class 2 variables:
    # The last value of ``weight1``/``mean1`` should pair with zero values in
    # ``weight2``/``mean2``, which do not exist.
    # variance12 = weight1[:-1] * weight2[1:] * (mean1[:-1] - mean2[1:]) ** 2

    weight1 = weight1 / img.ravel().shape
    weight2 = weight2 / img.ravel().shape

    #assert reg_power <= 1, \
    #    "Cannot have a regularization power more than 1"

    weighted = np.power(weight2[1:], reg_power)

    # variance12 = weighted * weight1[:-1] * mean1[:-1] ** 2 + weight2[1:] * mean2[1:] ** 2
    variance12 = weight1[:-1] * mean1[:-1] ** 2 + weighted * weight2[1:] * mean2[1:] ** 2
    # unw_variance12 = weight1[:-1] * mean1[:-1] ** 2 + weight2[1:] * mean2[1:] ** 2

    idx = np.argmax(variance12)
    threshold = bin_centers[:-1][idx]

    cell_mask = img_to_cell_mask > threshold

    return cell_mask, threshold

def WOV_otsu_RGB(img_to_cell_mask,
             tissue_mask,
             WOV_otsu_opts):
    # some checks
    from skimage.exposure import histogram
    from skimage._shared.utils import warn

    if img_to_cell_mask.ndim > 2 and img_to_cell_mask.shape[-1] in (3, 4):
        msg = "threshold_otsu is expected to work correctly only for " \
              "grayscale images; image shape {0} looks like an RGB image"
        warn(msg.format(img_to_cell_mask.shape))

    # parse parameters needed for WOV Otsu
    reg_power, nbins = WOV_otsu_opts[0], WOV_otsu_opts[1]

    # mask the densities before finding the threshold
    img = np.copy(img_to_cell_mask)
    img = img[tissue_mask > 0]
    #img = img[img > 0]  # you do mask for 0s.
    img = img[img < 255] # for the RGB cases


    #print("Right before thresholding (how many entries to construct histogram):")
    #print(np.count_nonzero(img))

    if np.count_nonzero(img) == 0:
        threshold = 1
    else:

        hist, bin_centers = histogram(img.ravel(), nbins, source_range='image')
        hist = hist.astype(float)

        # class probabilities for all possible thresholds
        weight1 = np.cumsum(hist)
        weight2 = np.cumsum(hist[::-1])[::-1]
        # class means for all possible thresholds
        mean1 = np.cumsum(hist * bin_centers) / weight1
        mean2 = (np.cumsum((hist * bin_centers)[::-1]) / weight2[::-1])[::-1]

        weight1 = weight1 / img.ravel().shape
        weight2 = weight2 / img.ravel().shape

        weighted = np.power(weight2[1:], reg_power) # weight ^ (-1 to 1)

        variance12 = weight1[:-1] * mean1[:-1] ** 2 + weighted * weight2[1:] * mean2[1:] ** 2 #weight 2 will be 1 +/- (1)

        array_checker = np.asarray(variance12)
        if array_checker.size == 0: #check if the entire image has been masked out (i.e. nothing to find a threshold on)
            threshold = 1 #essentially just black space
        else:
            idx = np.argmax(variance12)
            threshold = bin_centers[:-1][idx]

    cell_mask = img_to_cell_mask < threshold # for RGB, it's lower than the threshold.

    #print(threshold)

    return cell_mask, threshold

def inverted_WOV_otsu(img_to_cell_mask,
             tissue_mask,
             WOV_otsu_opts):
    # some checks
    from skimage.exposure import histogram
    from skimage._shared.utils import warn

    if img_to_cell_mask.ndim > 2 and img_to_cell_mask.shape[-1] in (3, 4):
        msg = "threshold_otsu is expected to work correctly only for " \
              "grayscale images; image shape {0} looks like an RGB image"
        warn(msg.format(img_to_cell_mask.shape))

    # Check if the image is multi-colored or not
    first_pixel = img_to_cell_mask.ravel()[0]
    if np.all(img_to_cell_mask == first_pixel):
        return first_pixel

    # parse parameters needed for WOV Otsu
    # TODO (need to check to make sure that it equates to Otsu)
    reg_power, nbins = WOV_otsu_opts[0], WOV_otsu_opts[1]

    # mask the densities before finding the threshold
    img = np.copy(img_to_cell_mask)
    img = img[tissue_mask > 0]
    img = img[img > 0]  # you do mask for 0s.

    hist, bin_centers = histogram(img.ravel(), nbins, source_range='image')
    hist = hist.astype(float)

    # class probabilities for all possible thresholds
    weight1 = np.cumsum(hist)
    weight2 = np.cumsum(hist[::-1])[::-1]
    # class means for all possible thresholds
    mean1 = np.cumsum(hist * bin_centers) / weight1
    mean2 = (np.cumsum((hist * bin_centers)[::-1]) / weight2[::-1])[::-1]

    # Clip ends to align class 1 and class 2 variables:
    # The last value of ``weight1``/``mean1`` should pair with zero values in
    # ``weight2``/``mean2``, which do not exist.
    # variance12 = weight1[:-1] * weight2[1:] * (mean1[:-1] - mean2[1:]) ** 2

    weight1 = weight1 / img.ravel().shape
    weight2 = weight2 / img.ravel().shape

    #assert reg_power <= 1, \
    #    "Cannot have a regularization power more than 1"

    weighted = np.power(weight1[:-1], reg_power)

    # variance12 = weighted * weight1[:-1] * mean1[:-1] ** 2 + weight2[1:] * mean2[1:] ** 2
    variance12 = weighted * weight1[:-1] * mean1[:-1] ** 2 + weight2[1:] * mean2[1:] ** 2
    # unw_variance12 = weight1[:-1] * mean1[:-1] ** 2 + weight2[1:] * mean2[1:] ** 2

    idx = np.argmax(variance12)
    threshold = bin_centers[:-1][idx]

    cell_mask = img_to_cell_mask > threshold

    return cell_mask, threshold

def manual_rgb(img_to_cell_mask, threshold, below=True):
    """Set a manual threshold on the image already scaled from 0 to 255
    This is done by converting it np.int8 datatype.
    :param img_to_cell_mask image that is scaled from 0 to 255
    :param threshold threshold given; in the range of 0 to 255 only

    Based on how the software is set, high DAB corresponds to dark (i.e. 0) pixels
    so we take values less than the threshold."""

    assert threshold <= 255, "Threshold not within valid range [0,255]"

    if below:
        return img_to_cell_mask < threshold
    else:
        return img_to_cell_mask > threshold


def manual_density(img_to_cell_mask, threshold, below=True):
    """Set a manual threshold on the DAB density image.
    :param img_to_cell_mask image that represents the DAB density channel
    :param threshold threshold given"""

    if below:
        return img_to_cell_mask < threshold
    else:
        return img_to_cell_mask > threshold


"""Percentile"""


def percentile(img_to_cell_mask, percentile_):
    from numpy import percentile

    # mask out 0s first

    img = img_to_cell_mask[img_to_cell_mask > 0]

    # get threshold

    threshold = percentile(img, percentile_)

    return img_to_cell_mask > threshold, threshold


"""Random walker"""


def random_walker_segmentation(img_to_cell_mask,
                               tissue_mask,
                               random_walker_opts):
    # Random walker semgentation guided with unmasked DAB densities

    lower_, upper_ = random_walker_opts[0], random_walker_opts[1]
    markers = np.zeros(img_to_cell_mask.shape, dtype=np.uint)
    thr_below_ = np.percentile(img_to_cell_mask[tissue_mask > 0], lower_)
    thr_above_ = np.percentile(img_to_cell_mask[tissue_mask > 0], upper_)
    markers[(img_to_cell_mask < thr_below_)] = 1
    markers[(img_to_cell_mask > thr_above_)] = 2

    labels = random_walker(img_to_cell_mask, markers, beta=10, mode="bf")
    labels[labels == 1] = 0
    labels[labels == 2] = 1

    cell_mask = labels

    # Mask again with tissue mask

    cell_mask = cell_mask * tissue_mask

    return cell_mask


"""Faint staining or no staining checker"""


def otsu_failure(img_to_cell_mask,
                 threshold=0.10):
    """
    A simple method based on failure in Otsu's method. Otsu's method fails on patces with no specific staining or
    really faint staining. A failure in Otsu's method is defined as a wildly wrong threshold, results in higher than
    expected positive pixels. We compare this positive pixel fraction with a threshold; if it is over, we designate
    it as a faint/non-specific stain patch.

    :param img_to_cell_mask DAB densities to generate cell mask
    :param threshold check you positive pixel fraction (
    i.e. SAF). This is determined biologically (i.e. CD68 usually isn't more than 5% of the tissue)

    :return True if faintly stained, false if not.
    """

    from skimage.filters import threshold_otsu

    otsu_mask = img_to_cell_mask > threshold_otsu(img_to_cell_mask[img_to_cell_mask > 0])

    if (np.count_nonzero(otsu_mask) / (otsu_mask.shape[0] * otsu_mask.shape[1])) > threshold:
        return True
    else:
        return False
