#!/usr/bin/env python

from skimage.color import rgb2lab, rgb2gray
from skimage.filters import threshold_otsu

from modules import stain_utils, stain_separation

"""
Module to keep all non-cell (non-specific) masking. 
Separated based on colour spaces used.

Includes: 

- general tissue masking
- masking for non-specific DAB staining before thresholding

Params:

- All images (I) are assumed to be RGB, and in range [0,255]

"""

"""CIELAB"""
"""Some information from wikipedia page on CIELAB colour space: LAB space is three-dimensional, and covers the entire 
range of human color perception, or gamut. It is based on the opponent color model of human vision, where red/green 
forms an opponent pair, and blue/yellow forms an opponent pair. 

The lightness value, L*, also referred to as "Lstar," defines black at 0 and white at 100. 

The a* axis is relative to the green–red opponent colors, with negative values 
toward green and positive values toward red. 

The b* axis represents the blue–yellow opponents, with negative numbers 
toward blue and positive toward yellow. 

All this performed on the original RGB image.
"""


def luminance_mask(I, thresh=0.8):
    """
    Get a binary mask where true denotes 'not white'
    Effectively a background/tissue mask


    :param I:
    :param thresh: threshold value to decide if 'white'
    :return:
    """

    I_LAB = rgb2lab(I)
    L = I_LAB[:, :, 0] / 100.0
    return L < thresh


def b_mask(I, thresh=0):
    """
    Get a binary mask where true denotes 'more brown'.
    Useful for preprocessing and masking out non-specific DAB staining
    :param I:
    :param thresh: threshold value to decide if 'brown'
    :return:
    """
    I_LAB = rgb2lab(I)
    B = I_LAB[:, :, -1] / 255.0
    return B > thresh


"""Stain space"""


def hematoxylin_mask(I, colourmatrix, thresh):
    density_map_pNNLS = stain_separation.pNNLS(I, colourmatrix)
    _, hema_idx = stain_utils.stain_indices(colourmatrix)
    hema_map_pNNLS = density_map_pNNLS[:, :, hema_idx]

    return hema_map_pNNLS > thresh


def get_global_hematoxylin_mask(thumbnail, colourmatrix):
    density_map_pNNLS = stain_separation.pNNLS(thumbnail, colourmatrix)
    _, hema_idx = stain_utils.stain_indices(colourmatrix)
    hema_map_pNNLS = density_map_pNNLS[:, :, hema_idx]

    global_thr = threshold_otsu(hema_map_pNNLS)

    return global_thr


""" greyscale"""


def greyscale_mask(I, thresh=0.8):
    """
    Get a binary mask where true denotes 'not white'
    Effectively a background/tissue mask


    :param I:
    :param thresh: threshold value to decide if 'white'
    :return:
    """
    I_gs = rgb2gray(I)
    g = I_gs[:, :] / 255.0
    return g < thresh


""" OD/absorbance space """


def avg_OD_mask(I, thresh=0.1):
    """
    Get a binary mask where true denotes 'not white'
    Effectively a background/tissue mask

    High OD indicates high absorbance. This means more stain is present.


    :param I:
    :param thresh: threshold value to decide if 'white'
    :return:
    """
    I = I[:, :, :3]

    OD = stain_utils.RGB_to_OD(I)

    OD_mean = OD.mean(axis=-1)

    return OD_mean > thresh


"""Impromptu mask for SAF maps"""


def make_tissue_map(SAF_map,
                    area_thr=1e-4):
    """Makes tissue mask in the case where you only have SAF map
    It's not that good, so use at your own risk"""

    from skimage.morphology import remove_small_objects
    from scipy.ndimage.morphology import binary_fill_holes
    mask = SAF_map > area_thr
    mask = binary_fill_holes(mask)
    mask = remove_small_objects(mask)

    return mask


def morphing_tissue_mask(tissue_mask, morphing_opts):
    from scipy.ndimage.morphology import binary_erosion, binary_dilation
    from skimage.morphology import remove_small_holes, disk

    """Operations to improve the quality of tissue mask applied on SAF maps
    So options only tested on 256x256 tissue masks for 2 subjects

    Options (256x256):

    morphing_opts = {"threshold" : 0.1,
                    "remove_small_holes":True,
                    "erosion":[1.5],
                    "dilation":[1.5]}"""

    if "threshold" in morphing_opts:  # this means the tissue mask is not a tissue mask here.
        tissue_mask = tissue_mask > morphing_opts["threshold"]
    if "remove_small_holes" in morphing_opts:
        if morphing_opts["remove_small_holes"]:
            tissue_mask = remove_small_holes(tissue_mask)

    if "erosion" in morphing_opts:
        tissue_mask = binary_erosion(tissue_mask, structure=disk(morphing_opts["erosion"][0]))
    if "dilation" in morphing_opts:
        tissue_mask = binary_dilation(tissue_mask, structure=disk(morphing_opts["dilation"][0]))

    return tissue_mask


from skimage.morphology import area_opening, area_closing
from scipy.ndimage import binary_dilation, binary_erosion
import numpy as np


def tissue_mask_from_SAF_map(saf):
    saf[:, int(0.95 * (saf.shape[1])):] = 0
    saf[:, :int(0.02 * (saf.shape[1]))] = 0

    #safthresh = 1e-5  # mnd, mouse data?

    #safthresh = 5e-2 #reproducibility

    safthresh = 0

    tissue_mask_from_histo = (saf > safthresh)
    closed_mask = binary_dilation(tissue_mask_from_histo, np.ones((4,4)))
    #closed_mask = binary_erosion(closed_mask, np.ones((5, 5)))

    # clear all small holes and small structures
    closed_mask = area_closing(closed_mask.astype(np.float64), area_threshold=1e4)
    closed_mask = area_opening(closed_mask.astype(np.float64), area_threshold=1e5)

    return closed_mask
