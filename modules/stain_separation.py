#!/usr/bin/env python


"""
Methods on how to perform stain separation.

Implemented methods:
1. Standard matrix inversion inspired by
    Ruifrok, A.C. & Johnston, D.A., 'Quantification of histochemical staining by color deconvolution'

2. Standard NNLS method shown in
    Carey, D., 'A Novel Approach for the Colour Deconvolution of Multiple Histological Stains'

3. LARS-LASSO method inspired by
    A. Vahadane et al., ‘Structure-Preserving Color Normalization and Sparse Stain Separation for Histological Images’, IEEE Transactions on Medical Imaging, vol. 35, no. 8, pp. 1962–1971, Aug. 2016.

4. pseudo-NNLS method (to improve on computational efficiency; equivalent to standard NNLS in the RGB space)
"""

from __future__ import division

from modules import stain_utils as utils
import numpy as np
from scipy.optimize import nnls

def matrix_inversion(I, colourmatrix):
    inverse_ = np.linalg.pinv(colourmatrix)
    OD_ = utils.RGB_to_OD(I)
    D_matrix = np.dot(OD_, inverse_)
    return D_matrix


def NNLS(I, cm):
    OD_ = utils.RGB_to_OD(I)
    x_, y_ = OD_.shape[0], OD_.shape[1]
    D_matrix = np.zeros((x_, y_, 3))

    for px in range(x_):
        for py in range(y_):
            # print(nnls(cm,OD_[px,py])[0])

            result = nnls(cm.T, OD_[px, py])[0]

            D_matrix[px, py, :] = result
            # D_matrix[px,py,0] = result[2]
            # D_matrix[px,py,1] = result[0]
            # D_matrix[px,py,2] = result[1]

    return D_matrix


def pNNLS(I, cm):
    D_matrix = matrix_inversion(I, cm)
    D_matrix_modified = np.copy(D_matrix)

    dab_idx = np.argmax(cm[:, -1])
    hema_idx = np.abs(np.argmax(cm[:, -1]) - 1)

    # modified stain vectors

    isolated_dab_vector = np.array(([cm[dab_idx, :],
                                     [0, 0, 0],
                                     [0, 0, 0]]))
    isolated_hema_vector = np.array(([cm[hema_idx, :],
                                      [0, 0, 0],
                                      [0, 0, 0]]))

    inv_dabv = np.linalg.pinv(isolated_dab_vector)
    inv_hemav = np.linalg.pinv(isolated_hema_vector)

    OD_ = utils.RGB_to_OD(I)

    # print(np.dot(OD_,inv_hemav).shape)

    hema_conc = np.dot(OD_, inv_hemav)[:, :, 0]
    dab_conc = np.dot(OD_, inv_dabv)[:, :, 0]

    hema_conc = np.where(hema_conc <= 0, 0, hema_conc)  # set solutions to 0 if below 0; as per NNLS
    dab_conc = np.where(dab_conc <= 0, 0, dab_conc)

    # modify original least squares solution

    D_matrix_modified[:, :, dab_idx] = np.where(D_matrix[:, :, dab_idx] <= 0, 0,
                                                D_matrix[:, :, dab_idx])  # set all negative values to 0
    D_matrix_modified[:, :, hema_idx] = np.where(D_matrix[:, :, hema_idx] <= 0, 0, D_matrix[:, :, hema_idx])

    # if both positive, just leave it

    D_matrix_modified[:, :, dab_idx] = np.where(D_matrix_modified[:, :, hema_idx] == 0, dab_conc,
                                                D_matrix_modified[:, :,
                                                dab_idx])  # if hema was negative or 0 before, use pinv DAB
    D_matrix_modified[:, :, hema_idx] = np.where(D_matrix_modified[:, :, dab_idx] == 0, hema_conc,
                                                 D_matrix_modified[:, :, hema_idx])

    return D_matrix_modified
