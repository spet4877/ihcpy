#!/usr/bin/env python
"""


Module to handle segmentation map (i.e. dimensions with segmentation window), patch map (i.e. micro-patch area storage), and SAF map (i.e. final quantification.

"""
import math
from os.path import join

import imageio
import numpy as np
from tqdm import tqdm

from modules import stain_utils, stain_separation, maskMaker, dir_utils, colour_matrix, structure_tensor
from scipy.ndimage import gaussian_filter1d
from scipy import stats
import matplotlib.pyplot as plt

from modules.structure_tensor import tensor_to_HSV

"""Coordinate managmement """


def define_patch_in_window(window_coords, window_size, patch_size):
    if window_size.shape[0] == 1:  # it's a column

        patch_x1 = window_coords[0] // patch_size[0]
        patch_x2 = (window_coords[0] + window_size[0]) // patch_size[0]

        patch_y1 = 0
        patch_y2 = None

    else:

        patch_x1 = window_coords[0] // patch_size[0]
        patch_x2 = (window_coords[0] + window_size[0]) // patch_size[0]

        patch_y1 = window_coords[1] // patch_size[1]
        patch_y2 = (window_coords[1] + window_size[1]) // patch_size[1]

    return patch_x1, patch_x2, patch_y1, patch_y2


"""segmentation map (cell mask) to patch map"""


def patch_wise_SAF_from_cell_mask(cell_mask,
                                  window_size):
    """Converting to area measurements to be computed in patches.
    Done for a SINGLE cell mask here
    The SAF here assumes a full area (i.e. window size) for tissue (i.e. fully tissue)"""

    from skimage.util import view_as_blocks

    # print(window_size)
    # print(cell_mask.shape)

    sliding_dab_mask = view_as_blocks(cell_mask, block_shape=window_size)

    # get cell area and tissue area maps to check when SAF is not adequate
    cell_area_map = np.zeros((sliding_dab_mask.shape[0], sliding_dab_mask.shape[1]))
    # print(cell_area_map.shape)

    SAF_map = np.zeros((sliding_dab_mask.shape[0], sliding_dab_mask.shape[1]))
    segmentation_map = sliding_dab_mask

    for ii in range(sliding_dab_mask.shape[1]):
        for jj in range(sliding_dab_mask.shape[0]):
            cell_area_map[jj, ii] = np.count_nonzero(sliding_dab_mask[jj, ii, :, :])

            SAF_map[jj, ii] = cell_area_map[jj, ii] / (window_size[0] * window_size[1])

    return SAF_map.T, segmentation_map.T, cell_area_map.T


def patch_wise_area_from_tissue_mask(saf_tissue_mask,
                                     window_size,
                                     area_thr=0.5):
    """Converting to area measurements to be computed in patches.
    Done for a SINGLE tissue mask here
    If the amount of tissue is <50% of the patch, then it's taken to be an empty patch."""

    from skimage.util import view_as_blocks

    # print(window_size)
    # print(cell_mask.shape)

    sliding_tissue_mask = view_as_blocks(saf_tissue_mask, block_shape=window_size)

    saf_tissue_area_map = np.zeros((sliding_tissue_mask.shape[0], sliding_tissue_mask.shape[1]))

    for ii in range(sliding_tissue_mask.shape[1]):
        for jj in range(sliding_tissue_mask.shape[0]):
            area = np.count_nonzero(sliding_tissue_mask[jj, ii, :, :])
            if (area / (window_size[0] * window_size[1])) > area_thr:
                saf_tissue_area_map[jj, ii] = np.count_nonzero(sliding_tissue_mask[jj, ii, :, :])
            else:
                saf_tissue_area_map[jj, ii] = 0

    return saf_tissue_area_map.T


def patch_map_from_seg_map_RGB_via_default_threshold(metadata, patch_map_info, seg_map_info, tissue_mask_opts,
                                                     saf_tissue_mask_opts, cell_mask_opts):
    # segmentation map information
    coords_x = seg_map_info["coords_x"]
    coords_y = seg_map_info["coords_y"]
    segmentation_window = seg_map_info["size"]
    default_dab_threshold = metadata["default_thr_for_dab"]

    # patch map information
    cell_area_map = np.zeros_like(patch_map_info["map"])
    tissue_area_map = np.zeros_like(patch_map_info["map"])
    patch_size = patch_map_info["size"]

    # metadata information
    svs_fname = metadata["svs_fname"]
    colourmatrix = metadata["colour_matrix"]

    # check for segmentation window; patch or column
    if segmentation_window.shape[0] == 1:
        modified_window = np.array([segmentation_window[0],
                                    metadata["dimensions"][-1] // patch_size[1] * patch_size[
                                        1]])  # modify window to fit patch size
    else:
        modified_window = segmentation_window

    for idx, coord_x in enumerate(tqdm(coords_x)):
        for idy, coord_y in enumerate(coords_y):
            # set stain index
            dab_idx, hema_idx = stain_utils.stain_indices(colourmatrix)

            # set up coordinates and get window
            coord = [coord_x, coord_y]

            mpatch_x1, \
            mpatch_x2, \
            mpatch_y1, \
            mpatch_y2 = define_patch_in_window(coord,
                                               segmentation_window,
                                               patch_size)

            window = stain_utils.get_tile(svs_fname,
                                          x=coord_x,  # col number
                                          y=coord_y,  # row number, start from top
                                          sz_x=modified_window[0],
                                          sz_y=modified_window[1])

            # stain separation
            density_map_pNNLS = stain_separation.pNNLS(window, colourmatrix)
            dab_map_pNNLS = density_map_pNNLS[:, :, dab_idx]

            rgb_dab_map = stain_utils.OD_to_RGB(dab_map_pNNLS)

            # 1) tissue mask to mask DAB channel and 2) SAF tissue mask to normalize SAF

            mm = maskMaker.MaskMaker(rgb_dab_map, window)
            mm.get_tissue_mask(tissue_mask_opts)
            mm.get_saf_tissue_mask(saf_tissue_mask_opts)

            cell_mask = rgb_dab_map < default_dab_threshold
            cell_mask = maskMaker.morph_cell_mask(cell_mask, cell_mask_opts)

            # cell_mask = cell_mask * mm.tissue_mask
            saf_tissue_mask = mm.saf_tissue_mask

            # Micro-patch configuratuion

            _, _, area = patch_wise_SAF_from_cell_mask(cell_mask,
                                                       window_size=(patch_size[0], patch_size[1]))

            saf_tissue_area = patch_wise_area_from_tissue_mask(saf_tissue_mask,
                                                               window_size=(patch_size[0], patch_size[1]),
                                                               area_thr=0.5)

            cell_area_map[mpatch_x1: mpatch_x2, \
            mpatch_y1:mpatch_y2] = area

            tissue_area_map[mpatch_x1: mpatch_x2, \
            mpatch_y1:mpatch_y2] = saf_tissue_area

    return cell_area_map.T, tissue_area_map.T


def patch_map_from_seg_map_RGB_via_local_thresholds(metadata,
                                                    patch_map_info,
                                                    seg_map_info,
                                                    tissue_mask_opts,
                                                    cell_mask_opts,
                                                    saf_tissue_mask_opts,
                                                    gamma):
    # apply smoothing from gamma

    if gamma > 0:
        cell_mask_opts["dab_threshold_map_RGB"] = gaussian_filter1d(cell_mask_opts["dab_threshold_map_RGB"],
                                                                    sigma=gamma)

    # segmentation map information
    coords_x = seg_map_info["coords_x"]
    coords_y = seg_map_info["coords_y"]
    segmentation_window = seg_map_info["size"]

    # patch map information
    cell_area_map = np.zeros_like(patch_map_info["map"])
    tissue_area_map = np.zeros_like(patch_map_info["map"])
    patch_size = patch_map_info["size"]

    # metadata information
    svs_fname = metadata["svs_fname"]

    # check for segmentation window; patch or column
    if segmentation_window.shape[0] == 1:
        modified_window = np.array([segmentation_window[0],
                                    metadata["dimensions"][-1] // patch_size[1] * patch_size[
                                        1]])  # modify window to fit patch size
    else:
        modified_window = segmentation_window

    for idx, coord_x in enumerate(tqdm(coords_x)):
        for idy, coord_y in enumerate(coords_y):
            cell_mask_opts["window_index_to_evaluate"] = [idx, idy]  # you need to store window index

            colourmatrix = cell_mask_opts["colour_matrix_map"][cell_mask_opts["window_index_to_evaluate"][0],
                           0, :, :]

            # set stain index
            dab_idx, hema_idx = stain_utils.stain_indices(colourmatrix)

            # set up coordinates and get window
            coord = [coord_x, coord_y]

            mpatch_x1, \
            mpatch_x2, \
            mpatch_y1, \
            mpatch_y2 = define_patch_in_window(coord,
                                               segmentation_window,
                                               patch_size)

            window = stain_utils.get_tile(svs_fname,
                                          x=coord_x,  # col number
                                          y=coord_y,  # row number, start from top
                                          sz_x=modified_window[0],
                                          sz_y=modified_window[1])

            # stain separation
            density_map_pNNLS = stain_separation.pNNLS(window, colourmatrix)
            dab_map_pNNLS = density_map_pNNLS[:, :, dab_idx]

            rgb_dab_map = stain_utils.OD_to_RGB(dab_map_pNNLS)

            # perform masking
            mm = maskMaker.MaskMaker(rgb_dab_map, window)
            mm.get_tissue_mask(tissue_mask_opts)
            mm.get_saf_tissue_mask(saf_tissue_mask_opts)

            mm.get_cell_mask(tissue_mask_opts, cell_mask_opts)

            cell_mask = mm.cell_mask
            saf_tissue_mask = mm.saf_tissue_mask

            cell_mask = maskMaker.morph_cell_mask(cell_mask, cell_mask_opts)

            # Micro-patch configuratuion

            _, _, area = patch_wise_SAF_from_cell_mask(cell_mask,
                                                       window_size=(patch_size[0], patch_size[1]))

            """            saf_tissue_area = patch_wise_area_from_tissue_mask(saf_tissue_mask,
                                                               window_size=(patch_size[0], patch_size[1]),
                                                               area_thr=0.5)"""

            saf_tissue_area = patch_wise_area_from_tissue_mask(saf_tissue_mask,
                                                               window_size=(patch_size[0], patch_size[1]),
                                                               area_thr=0.5)

            cell_area_map[mpatch_x1: mpatch_x2, \
            mpatch_y1:mpatch_y2] = area

            tissue_area_map[mpatch_x1: mpatch_x2, \
            mpatch_y1:mpatch_y2] = saf_tissue_area

    return cell_area_map.T, tissue_area_map.T


"""patch map to SAF map"""


def SAF_map_from_patch_map(SAF_map_info,
                           patch_map_info):
    """Create a SAF of a different patch size from the micro-patch SAF map
    Info is a dictionary with map,
     patch sizes (SAF and micro),
      and coordinates ranges (SAF and micro)"""

    # SAF map

    SAF_map = np.zeros_like(SAF_map_info["map"])
    SAF_tissue_map = np.zeros_like(SAF_map_info["map"])
    coords_x = SAF_map_info["coords_x"]
    coords_y = SAF_map_info["coords_y"]
    SAF_patch_dims = SAF_map_info["size"]

    # patch map
    patch_map = patch_map_info["map"]
    tissue_patch_map = patch_map_info["tissue_map"]
    patch_dims = patch_map_info["size"]

    for idx, coord_x in enumerate(tqdm(coords_x)):
        for idy, coord_y in enumerate(coords_y):
            coord = [coord_x, coord_y]

            mpatch_x1, \
            mpatch_x2, \
            mpatch_y1, \
            mpatch_y2 = define_patch_in_window(coord,
                                               SAF_patch_dims,
                                               patch_dims)

            SAF_map[idx, idy] = np.sum(patch_map[mpatch_y1:mpatch_y2,
                                       mpatch_x1:mpatch_x2]) / (SAF_patch_dims[0] * SAF_patch_dims[1])

            SAF_tissue_map[idx, idy] = np.sum(tissue_patch_map[mpatch_y1:mpatch_y2,
                                              mpatch_x1:mpatch_x2]) / (SAF_patch_dims[0] * SAF_patch_dims[1])

            # print(mpatch_x1, mpatch_x2, mpatch_y1, mpatch_y2)

            # print(np.sum(patch_map[mpatch_x1:mpatch_x2,
            #             mpatch_y1:mpatch_y2]))

    # tissue_map_mask = (SAF_tissue_map.T > 0.5)

    return SAF_map.T, SAF_tissue_map.T


"""segmentation map (cell mask) to DAB threshold map"""


def dab_threshold_map_with_global_colourmatrix(metadata,
                                               seg_map_info,
                                               tissue_mask_opts,
                                               cell_mask_opts,
                                               colourmatrix):
    # segmentation map information
    coords_x = seg_map_info["coords_x"]
    coords_y = seg_map_info["coords_y"]
    segmentation_window = seg_map_info["size"]

    # DAB threshold information
    dab_thr_map = np.zeros_like(
        seg_map_info["map"])  # the dimensions of the threshold is exactly the same as the segmentation map.

    # metadata information
    svs_fname = metadata["svs_fname"]

    # check for segmentation window; patch or column
    if segmentation_window.shape[0] == 1:
        modified_window = np.array([segmentation_window[0],
                                    metadata["dimensions"][-1]])  # modify window to fit patch size
        dab_thr_map = dab_thr_map[:, np.newaxis]
    else:
        modified_window = segmentation_window

    interval = int(modified_window[1] * 0.1)

    for idx, coord_x in enumerate(tqdm(coords_x)):
        for idy, coord_y in enumerate(coords_y):
            coord = [coord_x, coord_y]

            window = stain_utils.get_tile(svs_fname,
                                          x=coord[0],  # col number
                                          y=coord[1],  # row number, start from top
                                          sz_x=modified_window[0],
                                          sz_y=int(modified_window[
                                                       1] - interval))  # crop it to remove black space that may bias the thresholding

            # set stain index
            dab_idx, hema_idx = stain_utils.stain_indices(colourmatrix)

            # stain separation
            density_map_pNNLS = stain_separation.pNNLS(window, colourmatrix)
            dab_map_pNNLS = density_map_pNNLS[:, :, dab_idx]

            rgb_dab_map_ = stain_utils.OD_to_RGB(dab_map_pNNLS)

            # using maskMaker to make 1) tissue mask to mask DAB channel, 2) cell mask, 3) tissue mask to normalize SAF
            mm = maskMaker.MaskMaker(rgb_dab_map_, window)
            mm.get_tissue_mask(tissue_mask_opts)

            # A quick check to make sure that there is still some DAB left
            empty_column_checker = mm.cell[mm.tissue_mask > 0]
            empty_column_checker = empty_column_checker[empty_column_checker < 255]  # for the RGB cases

            if empty_column_checker.size:  # some DAB still left
                mm.get_cell_mask(tissue_mask_opts, cell_mask_opts)
                dab_threshold = mm.dab_threshold
            else:  # no DAB values, so use neighbouring threshold
                dab_threshold = dab_thr_map[idx, idy - 1]

            dab_thr_map[idx, idy] = dab_threshold

    default_dab_threshold = np.median(dab_thr_map)

    thresholds = {"default": default_dab_threshold}
    maps = {"dab_threshold_map": np.squeeze(dab_thr_map.T)}

    return thresholds, maps


def dab_threshold_map_with_local_colourmatrix_and_MAD(metadata,
                                                      seg_map_info,
                                                      tissue_mask_opts,
                                                      cell_mask_opts,
                                                      mad_map,
                                                      cm_map,
                                                      MAD_weight):
    from scipy.ndimage import gaussian_filter1d

    # segmentation map information
    coords_x = seg_map_info["coords_x"]
    coords_y = seg_map_info["coords_y"]
    segmentation_window = seg_map_info["size"]

    # DAB threshold information
    dab_thr_map = np.zeros_like(
        seg_map_info["map"])  # the dimensions of the threshold is exactly the same as the segmentation map.

    # metadata information
    svs_fname = metadata["svs_fname"]

    # get WOV otsu original regularization

    reg = cell_mask_opts["WOV_otsu_RGB"][0]

    # adjust the MAD

    normal_ = gaussian_filter1d(mad_map, axis=0, sigma=1)
    smoothed_ = gaussian_filter1d(mad_map, axis=0, sigma=16)
    weights_ = 1 + (np.subtract(normal_, smoothed_)) / smoothed_

    # check for segmentation window; patch or column
    if segmentation_window.shape[0] == 1:
        modified_window = np.array([segmentation_window[0],
                                    metadata["dimensions"][-1]])  # modify window to fit patch size
        dab_thr_map = dab_thr_map[:, np.newaxis]
    else:
        modified_window = segmentation_window

    # interval = int(modified_window[1] * 0.1)

    for idx, coord_x in enumerate(tqdm(coords_x)):
        for idy, coord_y in enumerate(coords_y):
            coord = [coord_x, coord_y]

            window = stain_utils.get_tile(svs_fname,
                                          x=coord[0],  # col number
                                          y=coord[1],  # row number, start from top
                                          sz_x=modified_window[0],
                                          sz_y=modified_window[
                                              1])  # crop it to remove things that may bias the thresholding

            colourmatrix = cm_map[idx, 0, :, :]

            # set stain index
            dab_idx, hema_idx = stain_utils.stain_indices(colourmatrix)

            # stain separation
            density_map_pNNLS = stain_separation.pNNLS(window, colourmatrix)
            dab_map_pNNLS = density_map_pNNLS[:, :, dab_idx]

            rgb_dab_map_ = stain_utils.OD_to_RGB(dab_map_pNNLS)

            # get the MAD adjustment and use it for regularization

            weight_for_reg = weights_[idx]
            cell_mask_opts["WOV_otsu_RGB"][
                0] = reg * weight_for_reg ** MAD_weight

            # using maskMaker to make 1) tissue mask to mask DAB channel, 2) cell mask, 3) tissue mask to normalize SAF
            mm = maskMaker.MaskMaker(rgb_dab_map_, window)
            mm.get_tissue_mask(tissue_mask_opts)

            # A quick check to make sure that there is still some DAB left
            empty_column_checker = mm.cell[mm.tissue_mask > 0]
            empty_column_checker = empty_column_checker[empty_column_checker < 255]  # for the RGB cases

            if empty_column_checker.size:  # some DAB still left
                mm.get_cell_mask(tissue_mask_opts, cell_mask_opts)
                dab_threshold = mm.dab_threshold
            else:  # no DAB values, so use neighbouring threshold
                dab_threshold = dab_thr_map[idx, idy - 1]

            dab_thr_map[idx, idy] = dab_threshold

    default_dab_threshold = np.median(dab_thr_map)

    thresholds = {"default": default_dab_threshold}

    maps = {"dab_threshold_map": np.squeeze(dab_thr_map.T)}

    return thresholds, maps


"""Colour matrix and median absolute deviation map"""


def local_colour_matrix_and_MAD_map(metadata,
                                    seg_map_info,
                                    tissue_mask_opts,
                                    cm_opts):
    from scipy.stats import median_absolute_deviation

    # segmentation map information
    coords_x = seg_map_info["coords_x"]
    coords_y = seg_map_info["coords_y"]
    segmentation_window = seg_map_info["size"]

    # DAB threshold information

    dab_mad_map = np.zeros_like(
        seg_map_info["map"])  # the dimensions of the threshold is exactly the same as the segmentation map.

    # metadata information
    svs_fname = metadata["svs_fname"]

    # check for segmentation window; patch or column
    if segmentation_window.shape[0] == 1:
        modified_window = np.array([segmentation_window[0],
                                    metadata["dimensions"][-1]])  # modify window to fit patch size
        dab_mad_map = dab_mad_map[:, np.newaxis]
    else:
        modified_window = segmentation_window

    # colour matrix map

    colour_matrix_map = np.zeros((dab_mad_map.shape[0],
                                  dab_mad_map.shape[1],
                                  3,
                                  3))

    for idx, coord_x in enumerate(tqdm(coords_x)):

        for idy, coord_y in enumerate(coords_y):
            coord = [coord_x, coord_y]

            window = stain_utils.get_tile(svs_fname,
                                          x=coord[0],  # col number
                                          y=coord[1],  # row number, start from top
                                          sz_x=modified_window[0],
                                          sz_y=modified_window[1])

            """Get local colour matrix for column/patch"""

            colourmatrix = colour_matrix.single_kmeans(img=window,
                                                       option=cm_opts["tissue_mask"][0],
                                                       threshold=cm_opts["tissue_mask"][1])

            if colourmatrix is None:
                colourmatrix = colour_matrix_map[idx, idy - 1, :, :]  # take the neighbouring colourmatrix

            # adjust colour matrix to make DAB the top row and hema the second row

            colourmatrix = colour_matrix.adjust_colourmatrix(colourmatrix)

            """Get local MAD for column/patch"""
            # set stain index
            dab_idx, hema_idx = stain_utils.stain_indices(colourmatrix)

            # stain separation
            density_map_pNNLS = stain_separation.pNNLS(window, colourmatrix)
            dab_map_pNNLS = density_map_pNNLS[:, :, dab_idx]

            rgb_dab_map_ = stain_utils.OD_to_RGB(dab_map_pNNLS)

            # using maskMaker to make 1) tissue mask to mask DAB channel, 2) cell mask, 3) tissue mask to normalize SAF
            mm = maskMaker.MaskMaker(rgb_dab_map_, window)
            mm.get_tissue_mask(tissue_mask_opts)

            # generate MAD map (MAD is taken from preprocessed DAB pixels)
            dab_ = rgb_dab_map_[mm.tissue_mask > 0]  # preprocessed luminance masking
            dab_ = dab_[dab_ < 255]  # further removal of all white pixels

            mad_for_column = median_absolute_deviation(dab_)

            if math.isnan(mad_for_column) or math.isinf(mad_for_column):
                mad_for_column = dab_mad_map[idx, idy - 1]

            # print(dab_mad_map[idx, idy])

            # make maps
            dab_mad_map[idx, idy] = mad_for_column
            colour_matrix_map[idx, idy, :, :] = colourmatrix

    object_maps = {"colour_matrix_map": colour_matrix_map,
                   "mad_map": dab_mad_map}

    return object_maps


"""Orientation map"""


def patch_map_from_seg_and_orientation_map_RGB_via_default_threshold(metadata,
                                                                     patch_map_info,
                                                                     seg_map_info,
                                                                     tissue_mask_opts,
                                                                     saf_tissue_mask_opts,
                                                                     cell_mask_opts,
                                                                     structure_tensor_opts):
    # segmentation map information
    coords_x = seg_map_info["coords_x"]
    coords_y = seg_map_info["coords_y"]
    segmentation_window = seg_map_info["size"]
    default_dab_threshold = metadata["default_thr_for_dab"]

    # structure tensor opts

    kernel = structure_tensor_opts["sigma"]
    bin_num = 90  # 2 degrees per bin

    # patch map information
    cell_area_map = np.zeros_like(patch_map_info["map"])
    tissue_area_map = np.zeros_like(patch_map_info["map"])
    normal_sta_map = np.zeros((tissue_area_map.shape[0], tissue_area_map.shape[1], 3))
    masked_sta_map = np.zeros_like(normal_sta_map)
    fod_sta_map = np.zeros((tissue_area_map.shape[0], tissue_area_map.shape[1], bin_num))
    patch_size = patch_map_info["size"]

    # metadata information
    svs_fname = metadata["svs_fname"]
    colourmatrix = metadata["colour_matrix"]

    # check for segmentation window; patch or column
    if segmentation_window.shape[0] == 1:
        modified_window = np.array([segmentation_window[0],
                                    metadata["dimensions"][-1] // patch_size[1] * patch_size[
                                        1]])  # modify window to fit patch size
    else:
        modified_window = segmentation_window

    # create custom map for saving

    for idx, coord_x in enumerate(tqdm(coords_x)):
        for idy, coord_y in enumerate(coords_y):

            if "intermediates" in structure_tensor_opts:
                saving_sta_map = np.copy(np.transpose(normal_sta_map, axes=[1, 0, 2]))

                structure_tensor.save_intermediate(idx,
                                                   saving_sta_map,
                                                   structure_tensor_opts["intermediates"])

            # set stain index
            dab_idx, hema_idx = stain_utils.stain_indices(colourmatrix)

            # set up coordinates and get window
            coord = [coord_x, coord_y]

            mpatch_x1, \
            mpatch_x2, \
            mpatch_y1, \
            mpatch_y2 = define_patch_in_window(coord,
                                               segmentation_window,
                                               patch_size)

            window = stain_utils.get_tile(svs_fname,
                                          x=coord_x,  # col number
                                          y=coord_y,  # row number, start from top
                                          sz_x=modified_window[0],
                                          sz_y=modified_window[1])

            # stain separation
            density_map_pNNLS = stain_separation.pNNLS(window, colourmatrix)
            dab_map_pNNLS = density_map_pNNLS[:, :, dab_idx]

            rgb_dab_map = stain_utils.OD_to_RGB(dab_map_pNNLS)

            # 1) tissue mask to mask DAB channel and 2) SAF tissue mask to normalize SAF

            mm = maskMaker.MaskMaker(rgb_dab_map, window)
            mm.get_tissue_mask(tissue_mask_opts)
            mm.get_saf_tissue_mask(saf_tissue_mask_opts)

            cell_mask = rgb_dab_map < default_dab_threshold
            cell_mask = maskMaker.morph_cell_mask(cell_mask, cell_mask_opts)

            # cell_mask = cell_mask * mm.tissue_mask
            saf_tissue_mask = mm.saf_tissue_mask

            # Compute STA

            dab_for_orientation = np.copy(1 - rgb_dab_map / 255)  # inverting it for STA
            """dab_for_orientation[
                cell_mask == 0] = 0  # mask it before feeding it in for orientation computation ('upweight' proteins of interest)"""
            orientation, _, ai = structure_tensor.structure_tensor(dab_for_orientation, kernel)

            # Micro-patch configuration

            area, normal_sta, masked_sta, fod_map = patch_wise_orientation_and_area(cell_mask=cell_mask,
                                                                                    orientation=orientation + np.pi / 2,
                                                                                    # offset by pi/2 to keep it [0,pi]
                                                                                    ai=ai,
                                                                                    dab=dab_for_orientation,
                                                                                    window_size=(
                                                                                    patch_size[0], patch_size[1]))

            saf_tissue_area = patch_wise_area_from_tissue_mask(saf_tissue_mask,
                                                               window_size=(patch_size[0], patch_size[1]),
                                                               area_thr=0.5)

            cell_area_map[mpatch_x1: mpatch_x2, \
            mpatch_y1:mpatch_y2] = area

            normal_sta_map[mpatch_x1: mpatch_x2, \
            mpatch_y1:mpatch_y2, :] = normal_sta

            masked_sta_map[mpatch_x1: mpatch_x2, \
            mpatch_y1:mpatch_y2, :] = masked_sta

            tissue_area_map[mpatch_x1: mpatch_x2, \
            mpatch_y1:mpatch_y2] = saf_tissue_area

            fod_sta_map[mpatch_x1: mpatch_x2, \
            mpatch_y1:mpatch_y2, :] = fod_map

    # convert to RGB to visualize HSV
    hsv_sta_map = tensor_to_HSV(normal_sta_map[..., 0],
                                normal_sta_map[..., 1],
                                normal_sta_map[..., 2],
                                hue_theta_zero=0,
                                hue_theta_pi=1)

    normal_sta_map = np.transpose(normal_sta_map, axes=[1, 0, 2])
    masked_sta_map = np.transpose(masked_sta_map, axes=[1, 0, 2])
    hsv_sta_map = np.transpose(hsv_sta_map, axes=[1, 0, 2])
    fod_sta_map = fod_sta_map.swapaxes(0, 1)

    return cell_area_map.T, tissue_area_map.T, normal_sta_map, masked_sta_map, hsv_sta_map, fod_sta_map


def patch_wise_orientation_and_area(cell_mask, dab, orientation, ai, window_size):
    """Converting to area and mean orientation measurements to be computed in patches.
    Done for a SINGLE cell mask and orientation here
    The SAF here assumes a full area (i.e. window size) for tissue (i.e. fully tissue)"""

    from skimage.util import view_as_blocks

    sliding_dab_mask = view_as_blocks(cell_mask, block_shape=window_size)
    sliding_orientation = view_as_blocks(orientation, block_shape=window_size)
    sliding_ai = view_as_blocks(ai, block_shape=window_size)
    sliding_dab = view_as_blocks(dab, block_shape=window_size)

    bin_num = 90  # 2 degree/bin

    cell_area_map = np.zeros((sliding_dab_mask.shape[0], sliding_dab_mask.shape[1]))
    normal_sta_map = np.zeros((sliding_dab_mask.shape[0], sliding_dab_mask.shape[1], 3))
    masked_sta_map = np.zeros_like(normal_sta_map)
    fod_map = np.zeros((sliding_dab_mask.shape[0], sliding_dab_mask.shape[1], bin_num))

    for ii in range(sliding_dab_mask.shape[1]):
        for jj in range(sliding_dab_mask.shape[0]):

            S = sliding_dab_mask[jj, ii, :, :]
            O = sliding_orientation[jj, ii, :, :]
            A = sliding_ai[jj, ii, :, :]
            D = sliding_dab[jj, ii, :, :]

            # Make hsv map for visualization
            normal_sta_map[jj, ii, 0] = stats.circmean(O)
            normal_sta_map[jj, ii, 1] = np.mean(A)
            normal_sta_map[jj, ii, 2] = np.mean(D)

            # get FOD per patch
            h, _ = np.histogram(O[S > 0].flatten(), bins=bin_num)
            fod_map[jj, ii, :] = h

            cell_area_map[jj, ii] = np.count_nonzero(S > 0)

            # Make masked maps for saving
            if np.count_nonzero(
                    S > 0) == 0:  # if no tissue-of-interest, just set to an unknown quantity (i.e. no structure)
                # orientation_map[jj, ii] = 0
                masked_sta_map[jj, ii, 0] = np.nan  # save as nan
                masked_sta_map[jj, ii, 1] = np.nan
                masked_sta_map[jj, ii, 2] = np.nan

            else:

                masked_sta_map[jj, ii, 0] = stats.circmean(O[S > 0])
                masked_sta_map[jj, ii, 1] = np.mean(A[S > 0])
                masked_sta_map[jj, ii, 2] = np.mean(D[S > 0])  # only compute the structure for the tissue of
                # interest

    normal_sta_map = np.transpose(normal_sta_map, axes=[1, 0, 2])
    masked_sta_map = np.transpose(masked_sta_map, axes=[1, 0, 2])
    fod_map = fod_map.swapaxes(0, 1)

    return cell_area_map.T, normal_sta_map, masked_sta_map, fod_map


def SAF_and_orientation_map_from_patch_map(SAF_map_info,
                                           patch_map_info):
    """Create a SAF of a different patch size from the micro-patch SAF map
    Info is a dictionary with map,
     patch sizes (SAF and micro),
      and coordinates ranges (SAF and micro)"""

    # SAF map

    SAF_map = np.zeros_like(SAF_map_info["map"])
    smaller_orientation_map = np.zeros((SAF_map_info["map"].shape[0], SAF_map_info["map"].shape[1], 3))
    SAF_tissue_map = np.zeros_like(SAF_map_info["map"])
    coords_x = SAF_map_info["coords_x"]
    coords_y = SAF_map_info["coords_y"]
    SAF_patch_dims = SAF_map_info["size"]

    # patch map
    patch_map = patch_map_info["map"]
    orientation_map = patch_map_info["normal_sta_map"]
    tissue_patch_map = patch_map_info["tissue_map"]
    patch_dims = patch_map_info["size"]

    for idx, coord_x in enumerate(tqdm(coords_x)):
        for idy, coord_y in enumerate(coords_y):
            coord = [coord_x, coord_y]

            mpatch_x1, \
            mpatch_x2, \
            mpatch_y1, \
            mpatch_y2 = define_patch_in_window(coord,
                                               SAF_patch_dims,
                                               patch_dims)

            SAF_map[idx, idy] = np.sum(patch_map[mpatch_y1:mpatch_y2,
                                       mpatch_x1:mpatch_x2]) / (SAF_patch_dims[0] * SAF_patch_dims[1])

            smaller_orientation_map[idx, idy, 0] = stats.circmean(orientation_map[mpatch_y1:mpatch_y2,
                                                                  mpatch_x1:mpatch_x2, 0])
            smaller_orientation_map[idx, idy, 1] = np.mean(orientation_map[mpatch_y1:mpatch_y2,
                                                           mpatch_x1:mpatch_x2, 1])
            smaller_orientation_map[idx, idy, 2] = np.mean(orientation_map[mpatch_y1:mpatch_y2,
                                                           mpatch_x1:mpatch_x2, 2])

            SAF_tissue_map[idx, idy] = np.sum(tissue_patch_map[mpatch_y1:mpatch_y2,
                                              mpatch_x1:mpatch_x2]) / (SAF_patch_dims[0] * SAF_patch_dims[1])

            # print(mpatch_x1, mpatch_x2, mpatch_y1, mpatch_y2)

            # print(np.sum(patch_map[mpatch_x1:mpatch_x2,
            #             mpatch_y1:mpatch_y2]))

    # tissue_map_mask = (SAF_tissue_map.T > 0.5)
    smaller_orientation_map = np.transpose(smaller_orientation_map, axes=[1, 0, 2])

    return SAF_map.T, SAF_tissue_map.T, smaller_orientation_map


"""DAB channel in native resolution"""


def get_dab_channel(metadata,
                    seg_map_info):
    # segmentation map information
    coords_x = seg_map_info["coords_x"]
    coords_y = seg_map_info["coords_y"]
    segmentation_window = seg_map_info["size"]

    # metadata information
    svs_fname = metadata["svs_fname"]
    colourmatrix = metadata["colour_matrix"]

    # native resolution map
    # native_map = np.zeros((metadata["dimensions"][-1],
    #                       metadata["dimensions"][0]), dtype=np.uint8)
    native_map = np.zeros((metadata["dimensions"][-1],
                           metadata["dimensions"][0], 3), dtype=np.uint8)

    print("Native map size : {}".format(native_map.shape))

    # check for segmentation window; patch or column
    # if segmentation_window.shape[0] == 1:
    modified_window = np.array([segmentation_window[0],
                                metadata["dimensions"][-1]])  # modify window to fit patch size
    # else:
    #    modified_window = segmentation_window

    """"
    dab_idx, hema_idx = stain_utils.stain_indices(colourmatrix)

    # set up coordinates and get window

    window = stain_utils.get_tile(svs_fname,
                                  x=coords_x[971],  # col number
                                  y=coords_y[0],  # row number, start from top
                                  sz_x=modified_window[0],
                                  sz_y=modified_window[1])


    # stain separation
    density_map_pNNLS = stain_separation.pNNLS(window, colourmatrix)
    dab_map_pNNLS = density_map_pNNLS[:, :, dab_idx]
    rgb_dab_map = stain_utils.OD_to_RGB(dab_map_pNNLS)

    print(coords_x[971])
    print(coords_x[971] + modified_window[0])
    print(coords_x[972])
    print(coords_x[972] + modified_window[0])
    print(coords_x[973])
    print(native_map[:modified_window[1], coords_x[972]: coords_x[973]].shape)
    # columnar DAB channel
    native_map[:modified_window[1], coords_x[971]: coords_x[971] + modified_window[0]] = rgb_dab_map


    print("works!")

    window = stain_utils.get_tile(svs_fname,
                                  x=coords_x[972],  # col number
                                  y=coords_y[0],  # row number, start from top
                                  sz_x=modified_window[0],
                                  sz_y=modified_window[1])

    # stain separation
    density_map_pNNLS = stain_separation.pNNLS(window, colourmatrix)
    dab_map_pNNLS = density_map_pNNLS[:, :, dab_idx]
    rgb_dab_map = stain_utils.OD_to_RGB(dab_map_pNNLS)

    # columnar DAB channel
    native_map[:modified_window[1], coords_x[972]: coords_x[972] + modified_window[0]] = rgb_dab_map

    print("works!")
    """

    for idx, coord_x in enumerate(tqdm(coords_x)):
        for idy, coord_y in enumerate(coords_y):
            # set stain index
            dab_idx, hema_idx = stain_utils.stain_indices(colourmatrix)

            # set up coordinates and get window

            window = stain_utils.get_tile(svs_fname,
                                          x=coord_x,  # col number
                                          y=coord_y,  # row number, start from top
                                          sz_x=modified_window[0],
                                          sz_y=modified_window[1])

            # stain separation
            density_map_pNNLS = stain_separation.pNNLS(window, colourmatrix)
            # dab_map_pNNLS = density_map_pNNLS[:, :, dab_idx]

            # convert to intensity space (still 1-dim)
            # rgb_dab_map = stain_utils.OD_to_RGB(dab_map_pNNLS)

            # convert to actually RGB values (i.e. 3-dim)
            rgb_dab_map, _ = stain_utils.density_map_to_RGB(density_map_pNNLS, colourmatrix)

            # print("Column {}: {}".format(idx, rgb_dab_map.shape))

            # columnar DAB channel
            # native_map[:modified_window[1], coord_x: coord_x + modified_window[0]] = rgb_dab_map
            native_map[:modified_window[1], coord_x: coord_x + modified_window[0], :] = rgb_dab_map

        if idx == 100:
            dir_utils.save_arr_to_png(native_map,
                                      dir_=join(metadata["output_dir"], "checkpoints"),
                                      name_=metadata["name"] + "_{}".format(idx))

            print("Save at {}".format(idx))

        if idx == 500:
            dir_utils.save_arr_to_png(native_map,
                                      dir_=join(metadata["output_dir"], "checkpoints"),
                                      name_=metadata["name"] + "_{}".format(idx))

            print("Save at {}".format(idx))

        if idx == 1000:
            dir_utils.save_arr_to_png(native_map,
                                      dir_=join(metadata["output_dir"], "checkpoints"),
                                      name_=metadata["name"] + "_{}".format(idx))

            print("Save at {}".format(idx))

        if idx == 1500:
            dir_utils.save_arr_to_png(native_map,
                                      dir_=metadata["output_dir"],
                                      name_=metadata["name"] + "_{}".format(idx))

            print("Save at {}".format(idx))

    return native_map
