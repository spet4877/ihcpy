import numpy as np

from modules import stain_separation, stain_utils
import pytest
from numpy import load


@pytest.fixture
def patch():
    """
    :return: 1 pre-generated RGB patches (128 x 128 x 3)
    """
    return load("./data.npz")["a"][0]


@pytest.fixture
def colourmatrix():
    """
    :return: colour matrix from pre-generated patches
    """
    return load("./data.npz")["b"]


@pytest.fixture
def mi_dab():
    """
    :return: DAB channel stain-separated with matrix inversion
    """
    return load("./data.npz")["c"]


@pytest.fixture
def NNLS_dab():
    """
    :return: DAB channel stain-separated with NNLS method
    """
    return load("./data.npz")["d"]


@pytest.fixture
def pseudo_NNLS_dab():
    """
    :return: DAB channel stain-separated with pseudo-NNLS method
    """
    return load("./data.npz")["e"]


def test_matrix_inversion(patch, colourmatrix, mi_dab):
    stains = stain_separation.matrix_inversion(patch, colourmatrix)
    dab_index, _ = stain_utils.stain_indices(colourmatrix)
    dab = stains[:, :, dab_index]

    assert np.all(np.isclose(dab, mi_dab))


def test_nnls(patch, colourmatrix, NNLS_dab):
    stains = stain_separation.NNLS(patch, colourmatrix)
    dab_index, _ = stain_utils.stain_indices(colourmatrix)
    dab = stains[:, :, dab_index]

    assert np.all(dab >= 0)  # check for no negative numbers
    assert np.all(np.isclose(dab, NNLS_dab))  # check that it is equal to what I derived before


def test_p_nnls(patch, colourmatrix, pseudo_NNLS_dab, NNLS_dab):
    stains = stain_separation.pNNLS(patch, colourmatrix)
    dab_index, _ = stain_utils.stain_indices(colourmatrix)
    dab = stains[:, :, dab_index]

    assert np.all(dab >= 0)  # check for no negative numbers
    assert np.all(np.isclose(dab, pseudo_NNLS_dab))  # check that it is equal to what I derived before
    assert np.all(np.isclose(dab, NNLS_dab))  # check that is almost identical to NNLS
