import numpy as np
import pytest
from numpy import load

from modules import cell_masking, stain_utils, stain_separation
from skimage.filters import threshold_otsu

@pytest.fixture
def patches():
    """
    :return: stack of pre-generated RGB patches (128 x 128 x 3)
    """
    return load("./data.npz")["a"]

@pytest.fixture
def colourmatrix():
    """
    :return: colour matrix from pre-generated patches
    """
    return load("./data.npz")["b"]

@pytest.fixture
def thresholds():
    """
    :return: thresholds from pre-generated patches

    """

    return load("./data.npz")["f"]

@pytest.fixture
def alpha():
    """
    :return:  alpha term (i.e. WOV) used to generate thresholds

    """

    return load("./data.npz")["g"]


def test_WOV(patches, colourmatrix, thresholds, alpha):

    stack = patches[:10, ...]
    stack_thresholds = thresholds[:10]

    computed = np.zeros(shape=(10, 1))

    cell_mask_opts = {"WOV_otsu_RGB": [alpha, 256]}

    dab_index, _ = stain_utils.stain_indices(colourmatrix)

    for idx, patch in enumerate(stack):
        stains = stain_separation.pNNLS(patch, colourmatrix)
        dab_channel = stains[:, :, dab_index]
        rgb_dab = stain_utils.OD_to_RGB(dab_channel)
        tissue_mask = np.ones_like(rgb_dab)
        _, computed[idx] = cell_masking.WOV_otsu_RGB(rgb_dab, tissue_mask, cell_mask_opts["WOV_otsu_RGB"])

    assert np.all(np.equal(computed, stack_thresholds))


def test_otsu(patches, colourmatrix, thresholds, alpha):
    stack = patches[:10, ...]

    computed = np.zeros(shape=(10, 1))
    computed_otsu = np.zeros(shape=(10, 1))

    cell_mask_opts = {"WOV_otsu_RGB": [0, 256]}

    dab_index, _ = stain_utils.stain_indices(colourmatrix)

    for idx, patch in enumerate(stack):
        stains = stain_separation.pNNLS(patch, colourmatrix)
        dab_channel = stains[:, :, dab_index]
        rgb_dab = stain_utils.OD_to_RGB(dab_channel)
        tissue_mask = np.ones_like(rgb_dab)
        _, computed[idx] = cell_masking.WOV_otsu_RGB(rgb_dab, tissue_mask, cell_mask_opts["WOV_otsu_RGB"])
        computed_otsu[idx] = threshold_otsu(image=rgb_dab[rgb_dab < 255], nbins=255)  # excludes 'white' pixels

    assert np.all(np.equal(computed, computed_otsu))
