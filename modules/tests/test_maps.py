import numpy as np
import pytest
from numpy import load

from modules import maps

###======= Mask =======###

@pytest.fixture
def mask():
    """
    :return: simulated mask (1024 x 1024)
    """
    return load("./mask.npz")["a"]

@pytest.fixture
def downsampled_mask():
    """
    :return: mask downsampled via SAF patch computation
    """
    return load("./mask.npz")["b"]

@pytest.fixture
def patch_size():
    """
    :return: patch size to compute SAF

    """
    array = load("./mask.npz")["c"]
    return tuple((array[0], array[1]))




###======= Maps =======###

@pytest.fixture
def patch_map():
    """
    :return: Sample patch map prior computing SAF map

    """

    return load("./maps.npz")["a"]

@pytest.fixture
def patch_size_for_patch_map():
    """
    :return: Patch map's resolution

    """

    return load("./maps.npz")["b"]

@pytest.fixture
def SAF_map():
    """
    :return: Sample SAF map
    """

    return load("./maps.npz")["c"]

@pytest.fixture
def patch_size_for_SAF_map():
    """
    :return: SAF map's resolution

    """

    return load("./maps.npz")["d"]

def test_patch_wise_SAF_from_cell_mask(mask, downsampled_mask, patch_size):

    print(patch_size)

    _, _, computed = maps.patch_wise_SAF_from_cell_mask(mask, patch_size)
    computed = computed.T

    assert np.all(np.equal(computed, downsampled_mask))


def test_patch_map_to_SAF_map(patch_map, SAF_map, patch_size_for_patch_map, patch_size_for_SAF_map,):

    ## Patch map information

    micro_patch = patch_size_for_patch_map
    micro_patch_xx = np.arange(0, patch_map.shape[1] * micro_patch[1], micro_patch[1])
    micro_patch_yy = np.arange(0, patch_map.shape[0] * micro_patch[0], micro_patch[0])

    patch_map_info = {"map": np.zeros((micro_patch_xx.size,
                                       micro_patch_yy.size)),
                      "coords_x": micro_patch_xx,
                      "coords_y": micro_patch_yy,
                      "size": micro_patch}

    ## SAF map information

    SAF_patch = patch_size_for_SAF_map

    saf_patch_xx = np.arange(0, SAF_map.shape[1] * SAF_patch[1], SAF_patch[1])
    saf_patch_yy = np.arange(0, SAF_map.shape[0] * SAF_patch[0], SAF_patch[0])

    SAF_map_info = {"map": np.zeros((saf_patch_xx.size, saf_patch_yy.size)),
                    "coords_x": saf_patch_xx,
                    "coords_y": saf_patch_yy,
                    "size": SAF_patch}

    patch_map_info["map"] = patch_map
    patch_map_info["tissue_map"] = np.ones_like(patch_map)

    ## Compute the SAF map from patch map

    computed, _ = maps.SAF_map_from_patch_map(SAF_map_info,
                                            patch_map_info)


    assert np.all(np.equal(computed, SAF_map))
