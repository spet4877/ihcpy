import numpy as np

from modules import colour_matrix, stain_utils
import pytest
from numpy import load


@pytest.fixture
def patches():
    """
    :return: stack of 100 pre-generated RGB patches (128 x 128 x 3)
    """
    return load("./data.npz")["a"]


@pytest.fixture
def colourmatrix():
    """
    :return: colour matrix from pre-generated patches
    """
    return load("./data.npz")["b"]


def test_k_means(patches, colourmatrix):
    computed = colour_matrix.k_means(patches,
                                     verbose=False,
                                     threshold_value=0.85)

    computed_dab_index, computed_hema_index = stain_utils.stain_indices(computed)
    dab_index, hema_index = stain_utils.stain_indices(colourmatrix)

    assert np.all(np.isclose(computed[computed_dab_index,...], colourmatrix[dab_index,...], rtol=1e-4))
    assert np.all(np.isclose(computed[computed_hema_index, ...], colourmatrix[hema_index, ...], rtol=1e-4))