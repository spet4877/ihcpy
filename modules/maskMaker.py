#!/usr/bin/env python
"""


Module to perform the actual masking of cells.
Receiv

"""
import numpy as np
from scipy.ndimage import binary_fill_holes
from skimage.morphology import dilation
from skimage.filters import median

from modules import tissue_masking, cell_masking, stain_utils


class MaskMaker:

    def __init__(self,
                 img_to_cell_mask,
                 img_to_tissue_mask):
        # check image properties
        assert len(img_to_cell_mask.shape) == 2, \
            "Cell channel needs to be greyscale."

        assert len(img_to_tissue_mask.shape) == 3, \
            "Tissue needs to be RGB."

        # assign objects
        self.cell = img_to_cell_mask
        self.tissue = img_to_tissue_mask

    def get_tissue_mask(self, tissue_mask_opts):

        """Tissue mask; if no options are available, it's just a default matrix of 1s
        at the same dimension as the patch. Note that multiple options will lead to
        an intersection of masks.

        :param tissue_mask_opts is a dictionary of options for masks"""

        # create a 2D mask
        default_mask = np.ones((self.tissue.shape[0],
                                self.tissue.shape[1]))

        if "threshold" in tissue_mask_opts:
            tissue_mask = self.cell > tissue_mask_opts["threshold"]

            default_mask = default_mask * tissue_mask

        if "L_mask" in tissue_mask_opts:
            tissue_mask = tissue_masking.luminance_mask(self.tissue,
                                                        thresh=tissue_mask_opts["L_mask"])
            default_mask = default_mask * tissue_mask

        if "B_mask" in tissue_mask_opts:
            tissue_mask = tissue_masking.b_mask(self.tissue,
                                                thresh=tissue_mask_opts["B_mask"])
            default_mask = default_mask * tissue_mask

        if "hema_mask" in tissue_mask_opts:
            tissue_mask = tissue_masking.hematoxylin_mask(self.tissue)
            default_mask = default_mask * tissue_mask

        self.tissue_mask = default_mask

    def get_cell_mask(self,
                      tissue_mask_opts,
                      cell_mask_opts):

        # make tissue mask if not already done
        if not hasattr(self, "tissue_mask"):
            MaskMaker.get_tissue_mask(self, tissue_mask_opts)

        # perform segmentation based on options provided
        if "random_walker" in cell_mask_opts:

            """            if cell_masking.otsu_failure(self.cell,
                                         threshold=0.05):
                # print("Bad COLUMN")
                # TODO: confirm if tissue mask needs to be changed.

                cell_mask = cell_masking.random_walker_segmentation(self.cell,
                                                                    self.tissue_mask,
                                                                    [50, 90])  # faintly stained, so estimate a really
                # a LOW SAF."""
            """else:"""
            cell_mask = cell_masking.random_walker_segmentation(self.cell,
                                                                self.tissue_mask,
                                                                cell_mask_opts["random_walker"])
            threshold = None

        elif "WOV_otsu" in cell_mask_opts:

            regularization = cell_mask_opts["WOV_otsu"][0]

            if "kurtosis_weighting" in cell_mask_opts:
                from scipy.stats import kurtosis
                power = cell_mask_opts["kurtosis_weighting"]
                masked_density = self.cell[self.tissue_mask > 0]  # mask with L-mask (or tissue mask)
                masked_density = masked_density[masked_density > 0]  # mask for 0s

                upper_bounded = np.percentile(masked_density, 99.9)

                masked_density = masked_density[
                    masked_density < upper_bounded]  # seems to give you valid kurtossi measures
                kurt = kurtosis(masked_density)
                regularization = regularization / (kurt ** power)

            cell_mask, threshold = cell_masking.WOV_otsu(self.cell,
                                                         self.tissue_mask,
                                                         [regularization, cell_mask_opts["WOV_otsu"][1]])

        elif "WOV_otsu_RGB" in cell_mask_opts:

            regularization = cell_mask_opts["WOV_otsu_RGB"][0]
            nbins = cell_mask_opts["WOV_otsu_RGB"][1]
            WOV_otsu_array = np.array([regularization, nbins])

            print(WOV_otsu_array)

            cell_mask, threshold = cell_masking.WOV_otsu_RGB(self.cell, self.tissue_mask, WOV_otsu_array)


        elif "threshold" in cell_mask_opts:
            cell_mask = self.cell > cell_mask_opts["threshold"]
            threshold = cell_mask_opts["threshold"]


        elif "Otsu" in cell_mask_opts:

            cell_mask_opts["Otsu"][0] = 0  # a regularization power of 1

            cell_mask, threshold = cell_masking.WOV_otsu(self.cell,
                                                         self.tissue_mask,
                                                         cell_mask_opts["Otsu"])

        elif "manual" in cell_mask_opts:

            # assuming the DAB channel is in absorbance space

            cell_rgb = stain_utils.OD_to_RGB(OD=self.cell)
            weak_, medium_, strong_ = cell_mask_opts["manual"][0]

            if cell_mask_opts["manual"][1] == "weak":
                threshold = weak_

            elif cell_mask_opts["manual"][1] == "medium":
                threshold = medium_
            else:
                threshold = strong_

            cell_mask = cell_masking.manual_rgb(cell_rgb,
                                                threshold=threshold,
                                                below=True)

        elif "dab_threshold_map" in cell_mask_opts:

            # set the threshold based on the patch and column index

            indices_for_current_window = cell_mask_opts[
                "window_index_to_evaluate"]  # this should be supplied from your loop
            dab_threshold_map = cell_mask_opts["dab_threshold_map"]

            # if it's a column-based DAB threshold map, add an extra axis for consistency

            if len(dab_threshold_map.shape) == 1:
                dab_threshold_map = dab_threshold_map[:, np.newaxis]

            # assign threshold to be used based on window index + external DAB threshold map

            threshold = dab_threshold_map[indices_for_current_window[0], indices_for_current_window[1]]
            # print(threshold)

            cell_mask = cell_masking.manual_density(self.cell,
                                                    threshold=threshold,
                                                    below=False)

        elif "dab_threshold_map_RGB" in cell_mask_opts:

            # set the threshold based on the patch and column index

            indices_for_current_window = cell_mask_opts[
                "window_index_to_evaluate"]  # this should be supplied from your loop
            dab_threshold_map = cell_mask_opts["dab_threshold_map_RGB"]

            # if it's a column-based DAB threshold map, add an extra axis for consistency

            if len(dab_threshold_map.shape) == 1:
                dab_threshold_map = dab_threshold_map[:, np.newaxis]

            # assign threshold to be used based on window index + external DAB threshold map

            threshold = dab_threshold_map[indices_for_current_window[0], indices_for_current_window[1]]
            # print(threshold)

            cell_mask = cell_masking.manual_density(self.cell,
                                                    threshold=threshold,
                                                    below=True)

        elif "percentile" in cell_mask_opts:

            cell_mask, threshold = cell_masking.percentile(self.cell,
                                                           cell_mask_opts["percentile"])

        else:
            raise Exception("No cell mask options given")

        # check if any of the morphology operations are in the cell mask options

        statement_ = ("dilation" in cell_mask_opts) | ("remove_small_holes" in cell_mask_opts) | (
                    "erosion" in cell_mask_opts) | ("median" in cell_mask_opts) | ("area_threshold" in cell_mask_opts)

        if statement_:
            cell_mask = morph_cell_mask(cell_mask, cell_mask_opts)

        self.cell_mask = cell_mask * self.tissue_mask
        self.dab_threshold = threshold

    def get_saf_tissue_mask(self, tissue_mask_opts):

        # create a 2D mask
        default_mask = np.ones((self.tissue.shape[0],
                                self.tissue.shape[1]))

        if "L_mask" in tissue_mask_opts:
            tissue_mask = tissue_masking.luminance_mask(self.tissue,
                                                        thresh=tissue_mask_opts["L_mask"])
            default_mask = default_mask * tissue_mask

        if "B_mask" in tissue_mask_opts:
            tissue_mask = tissue_masking.b_mask(self.tissue,
                                                thresh=tissue_mask_opts["B_mask"])
            default_mask = default_mask * tissue_mask

        if "hema_mask" in tissue_mask_opts:
            tissue_mask = tissue_masking.hematoxylin_mask(self.tissue,
                                                          colourmatrix=tissue_mask_opts["hema_mask"][0],
                                                          thresh=tissue_mask_opts["hema_mask"][1])  # hema_mask
            # options: 0 is colour matrix, 1 is global threshold.
            default_mask = default_mask * tissue_mask

        self.saf_tissue_mask = morph_tissue_mask(default_mask, tissue_mask_opts)


def morph_tissue_mask(tissue_mask, SAF_tissue_mask_opts):
    if "dilation" in SAF_tissue_mask_opts:
        block = np.ones(SAF_tissue_mask_opts["dilation"])
        tissue_mask = dilation(tissue_mask,
                               selem=block)
    if "median" in SAF_tissue_mask_opts:
        block = np.ones(SAF_tissue_mask_opts["median"])
        tissue_mask = median(tissue_mask, selem=block)

    if "fill_holes" in SAF_tissue_mask_opts:
        tissue_mask = binary_fill_holes(tissue_mask)

    return tissue_mask


def morph_cell_mask(cell_mask, cell_mask_opts):
    from scipy.ndimage.morphology import binary_dilation, binary_erosion
    from skimage.morphology import remove_small_holes, square
    from skimage.filters import median
    from skimage.morphology import area_opening

    selem = square(1)

    if "dilation" in cell_mask_opts:
        cell_mask = binary_dilation(cell_mask,
                                    structure=selem,
                                    border_value=0)
    if "remove_small_holes" in cell_mask_opts:
        cell_mask = remove_small_holes(cell_mask)
    if "erosion" in cell_mask_opts:
        cell_mask = binary_erosion(cell_mask,
                                   structure=selem,
                                   border_value=1)
    if "area_threshold" in cell_mask_opts:
        area_threshold = int(cell_mask_opts["area_threshold"])
        cell_mask = area_opening(cell_mask, area_threshold=area_threshold)
    if "median" in cell_mask_opts:

        cell_mask = median(cell_mask, selem=np.ones((cell_mask_opts["median"][0],
                                                     cell_mask_opts["median"][1])))

    return cell_mask
