#!/usr/bin/env python

"""


Module to store useful data for WSI (in .svs format)

Includes:

- Metadata of the .svs
- Segmentation map coordinates (coordinates used for segmentation)
- Patch map information (used for storing micro-patch-area prior to SAF map quantification)
- SAF map information (final SAF used to be compared)

"""

import glob
from os.path import join, dirname, basename
from re import split

import numpy as np
import openslide


class histoSAF:

    def __init__(self, svs_dir, output_dir):

        """
        Initializes a dictionary to hold the metadata of the WSI of the .svs file given.
        Includes the index of the .svs file in the root directory of all .svs files.

        :param: svs_dir (str)
        :param: output_dir (str) - root path to where you will save the SAF maps and all intermediate outputs

        """

        frame = openslide.OpenSlide(svs_dir)

        # find index of slice in directory of svs's

        total_list_of_svs = sorted(glob.glob(join(dirname(svs_dir), "*.svs")))
        index_in_dir = total_list_of_svs.index(svs_dir)

        # save metadata of WSI in dictionary

        metadata = {"name": split(".svs", basename(svs_dir))[0],
                    "svs_fname": svs_dir,
                    "root_dir": dirname(svs_dir),
                    "output_dir": output_dir,
                    "index_in_dir": index_in_dir,
                    "objective": frame.properties[openslide.PROPERTY_NAME_OBJECTIVE_POWER],
                    "resolution_x": frame.properties[openslide.PROPERTY_NAME_MPP_X],
                    "resolution_y": frame.properties[openslide.PROPERTY_NAME_MPP_Y],
                    "dimensions": frame.dimensions,
                    "level_dimensions": frame.level_dimensions,
                    "level_downsamples": frame.level_downsamples}

        self.metadata = metadata

        # get thumbnail (downsampled to the last level - usually a scale factor of 16)

        self.thumbnail = np.array(frame.get_thumbnail(frame.level_dimensions[-1]))

    def segmentation_info(self,
                          segmentation_window):
        """
        Initialize a segmentation map coordinate range based on segmentation window size given
        Depending on the segmentation window given, it is either a column of a patch.

        """
        width, height = self.metadata["dimensions"]

        segmentation_window = np.array(segmentation_window)

        self.segdim = segmentation_window

        # check the number of segmentation window dimensions given

        if segmentation_window.shape[0] == 1:  # 1 dimension means it's just width; column windows

            col_width = segmentation_window[0]

            xx = np.arange(0, (width // col_width) * col_width, col_width)
            # yy = [0] * len(xx)
            yy = [0]

            self.mod_width, self.mod_height = xx[-1] + col_width, height
            seg_map = np.zeros((xx.size))


        else:  # 2 dimensions means it's a patch

            xx = np.arange(0,
                           (width // segmentation_window[0]) * segmentation_window[0],
                           segmentation_window[0])
            yy = np.arange(0,
                           (height // segmentation_window[1]) * segmentation_window[1],
                           segmentation_window[1])

            self.mod_width, self.mod_height = xx[-1] + segmentation_window[0], yy[-1] + segmentation_window[1]
            seg_map = np.zeros((xx.size,
                                yy.size))

        # self.seg_cx, self.seg_cy = xx, yy

        self.seg_map_info = {"map": seg_map,
                             "coords_x": xx,
                             "coords_y": yy,
                             "size": segmentation_window}

    def patch_info(self,
                   patch):

        """
        Initialize a patch map array based on the patch size given

        """

        self.patchdim = patch

        micro_patch_xx = np.arange(0,
                                   (self.mod_width // patch[0]) * patch[0],
                                   patch[0])

        micro_patch_yy = np.arange(0,
                                   (self.mod_height // patch[1]) * patch[1],
                                   patch[1])

        # self.patch_map = np.zeros((micro_patch_xx.size,
        #                             micro_patch_yy.size))

        # self.patch_cx, self.patch_cy = micro_patch_xx, micro_patch_yy

        self.patch_map_info = {"map": np.zeros((micro_patch_xx.size,
                                                micro_patch_yy.size)),
                               "coords_x": micro_patch_xx,
                               "coords_y": micro_patch_yy,
                               "size": patch}

    def SAF_patch_info(self,
                       SAF_patch):

        """
        Initialize a SAF map array based on the SAF patch size given

        """
        self.safdim = SAF_patch

        width, height = self.metadata["dimensions"]

        saf_patch_xx = np.arange(0,
                                 (width // SAF_patch[0]) * SAF_patch[0],
                                 SAF_patch[0])

        saf_patch_yy = np.arange(0,
                                 (height // SAF_patch[1]) * SAF_patch[1],
                                 SAF_patch[1])

        # self.saf_patch_map = np.zeros((saf_patch_xx.size,
        #                            saf_patch_yy.size))

        # self.saf_patch_cx, self.saf_patch_cy = saf_patch_xx, saf_patch_yy

        self.SAF_map_info = {"map": np.zeros((saf_patch_xx.size,
                                              saf_patch_yy.size)),
                             "coords_x": saf_patch_xx,
                             "coords_y": saf_patch_yy,
                             "size": SAF_patch}
