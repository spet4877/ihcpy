#!/usr/bin/env python
"""

Module to keep useful functions for:
- loading, visualizing and 'building' images (of stains)
- Minor preprocessing (i.e. replacing 0s with 1s, normalizing rows)
- Custom RGB conversions (i.e. to OD/absorbance space)

Params:

- All images (I) are assumed to be RGB, and in range [0,255]

"""

from __future__ import division

import numpy as np
import openslide
import imageio
import matplotlib.pyplot as plt
from skimage import img_as_int
from skimage.color import rgb2lab
from tqdm import tqdm

from modules import stain_separation, maskMaker, colour_matrix

""""Image utils"""


def read_image(path):
    """
    Read an image to RGB uint8
    :param path:
    :return:
    """
    im = imageio.imread(path)
    return im


def show(image, now=True, fig_size=(20, 20), range_=None):
    """
    Show an image (np.array).
    Caution! Rescales image to be in range [0,1].
    :param image:
    :param now:
    :param fig_size:
    :return:
    """
    image = image.astype(np.float32)
    m, M = image.min(), image.max()
    if fig_size != None:
        plt.rcParams['figure.figsize'] = (fig_size[0], fig_size[1])

    if range_ is None:
        plt.imshow((image - m) / (M - m), cmap='gray')
    else:
        plt.imshow(image,
                   vmax=range_[1],
                   vmin=range_[0],
                   cmap='gray')

    plt.axis('off')
    if now == True:
        plt.show()


def build_stack(tup):
    """
    Build a stack of images from a tuple of images
    :param tup:
    :return:
    """
    N = len(tup)
    if len(tup[0].shape) == 3:
        h, w, c = tup[0].shape
        stack = np.zeros((N, h, w, c))
    if len(tup[0].shape) == 2:
        h, w = tup[0].shape
        stack = np.zeros((N, h, w))
    for i in range(N):
        stack[i] = tup[i]
    return stack


def patch_grid(ims, width=5, sub_sample=None, rand=False, save_name=None, close=False, range_=None, size=None):
    """
    Display a grid of patches
    :param ims:
    :param width:
    :param sub_sample:
    :param rand:
    :return:
    """
    N0 = np.shape(ims)[0]
    if sub_sample == None:
        N = N0
        stack = ims
    elif sub_sample != None and rand == False:
        N = sub_sample
        stack = ims[:N]
    elif sub_sample != None and rand == True:
        N = sub_sample
        idx = np.random.choice(range(N), sub_sample, replace=False)
        stack = ims[idx]
    height = np.ceil(float(N) / width).astype(np.uint16)

    if size is None:

        plt.rcParams['figure.figsize'] = (18, (18 / width) * height)

    else:

        plt.rcParams['figure.figsize'] = (size[0], size[1])

    plt.figure()
    for i in range(N):
        plt.subplot(height, width, i + 1)
        im = stack[i]

        if range_ is None:
            show(im, now=False, fig_size=None)
        else:
            show(im, now=False, fig_size=None, range_=range_)
    if save_name != None:
        plt.savefig(save_name)
    plt.show()

    if close:
        plt.close()


def get_tile(file, x, y, sz_x, sz_y):
    frame = openslide.OpenSlide(file)

    img = np.array(frame.read_region((x, y), 0, (sz_x, sz_y)))
    img = img[:, :, :3]
    return img


def show_gif(fname):
    import base64
    from IPython import display
    with open(fname, 'rb') as fd:
        b64 = base64.b64encode(fd.read()).decode('ascii')
    return display.HTML(f'<img src="data:image/gif;base64,{b64}" />')


def save_gif(rgb, mask, fps=1, dir_=None, fname_=None, mask_toggle=True):
    import imageio
    from pathlib import Path
    from IPython.core.interactiveshell import InteractiveShell
    InteractiveShell.ast_node_interactivity = "all"
    from IPython import display

    if dir_ is None:
        picsPath = Path("/Users/danielkor/Applications/results/image_folder/")

    else:
        picsPath = Path(dir_)

    if fname_ is None:
        fname_ = "mask.gif"

    images = []
    images.append(rgb)

    if mask_toggle:

        images.append(img_as_int(mask))
    else:
        images.append(mask)

    imageio.mimsave(picsPath / fname_, images, 'GIF-FI', fps=fps)


def display_mask(rgb, mask, fps=1, dir_=None, mask_toggle=True):
    from pathlib import Path
    from IPython.core.interactiveshell import InteractiveShell
    InteractiveShell.ast_node_interactivity = "all"

    if dir_ is None:

        dir_ = "/Users/danielkor/Applications/results/image_folder/"

        picsPath = Path(dir_)

    else:

        picsPath = Path(dir_)

    save_gif(rgb, mask, fps, dir_, "mask.gif", mask_toggle)

    return show_gif(picsPath / 'mask.gif')


def plot_row_colors(C, fig_size=6, title=None):
    """
    Plot rows of C as colors (RGB)
    :param C: An array N x 3 where the rows are considered as RGB colors.
    :return:
    """
    assert isinstance(C, np.ndarray), "C must be a numpy array."
    assert C.ndim == 2, "C must be 2D."
    assert C.shape[1] == 3, "C must have 3 columns."

    N = C.shape[0]
    range255 = C.max() > 1.0  # quick check to see if we have an image in range [0,1] or [0,255].
    plt.rcParams['figure.figsize'] = (fig_size, fig_size)
    for i in range(N):
        if range255:
            plt.plot([0, 1], [N - 1 - i, N - 1 - i], c=C[i] / 255, linewidth=20)
        else:
            plt.plot([0, 1], [N - 1 - i, N - 1 - i], c=C[i], linewidth=20)
    if title is not None:
        plt.title(title)
    plt.axis("off")
    plt.axis([0, 1, -0.5, N - 0.5])


def line_hist(arr, weights=None, bins=300):
    if weights is None:
        line_hist = np.histogram(arr, bins=bins)
        # plt.close()
    else:
        line_hist = np.histogram(arr, bins=weights)
        # plt.close()

    # return bins, count
    return line_hist[1], line_hist[0]


def standardize(arr):
    return (arr - arr.mean()) / arr.std()


"""Visualization"""


def full_column_segmentation(metadata,
                             column_opts,
                             tissue_mask_opts,
                             cell_mask_opts):
    # metadata information
    svs_fname = metadata["svs_fname"]
    colourmatrix = metadata["colour_matrix"]

    print("Slide-specific colour matrix: {}".format(colourmatrix))

    # column information
    segmentation_window = column_opts["window_size"]
    modified_window = np.array([segmentation_window[0],
                                metadata["dimensions"][-1]])  # change to column window
    stain_separation_opt = column_opts["stain_separation"]
    coords_x = column_opts["coords_x"]

    width_of_total_column = (coords_x[-1] + segmentation_window[0]) - coords_x[0]

    """One-class detection (works with all options except if EXTERNAL DAB threshold map is used"""

    # if one-class detection is activated, assert for column-wise segmentation only.
    if cell_mask_opts["one_class_detection"]:
        # assert segmentation_window.shape[0] == 1, "One-class detection is only possible for column-wise segmentation."
        index_of_valid_windows = cell_mask_opts["one_class_detection_list"]
        default_dab_threshold = metadata["default_thr_for_dab"]

    # threshold lists
    mixture_threshold_list = []
    threshold_list = []

    # full mask and RGB
    total_mask = np.zeros((modified_window[1], width_of_total_column))
    total_RGB = np.zeros((modified_window[1], width_of_total_column, 3))

    """Column-wise segmentation"""
    for idx, coord_x in enumerate(tqdm(coords_x)):

        # print(coord_x)

        """If external DAB thresholds are used -- you need to store window index"""
        if cell_mask_opts["external_dab_threshold_map"]:
            cell_mask_opts["window_index_to_evaluate"] = [coord_x // segmentation_window[0], 0]

            if cell_mask_opts[
                "columnwise_colourmatrix"]:
                colourmatrix = cell_mask_opts["colour_matrix_map"][cell_mask_opts["window_index_to_evaluate"][0],
                               0, :, :]

                # print(colourmatrix)

        # set stain index
        dab_idx, hema_idx = stain_indices(colourmatrix)

        if "manual" in cell_mask_opts:
            dab_idx = 1
            hema_idx = 0

        # print(coord_x//segmentation_window[0])

        window = get_tile(svs_fname,
                          x=coord_x,  # col number
                          y=0,  # row number, start from top
                          sz_x=modified_window[0],
                          sz_y=modified_window[1])

        # stain separation

        if stain_separation_opt == "pNNLS":

            density_map_ = stain_separation.pNNLS(window, colourmatrix)
        else:
            density_map_ = stain_separation.matrix_inversion(window, colourmatrix)

        dab_map_ = density_map_[:, :, dab_idx]

        # using maskMaker to make 1) tissue mask to mask DAB channel, 2) cell mask, 3) tissue mask to normalize SAF
        mm = maskMaker.MaskMaker(dab_map_, window)
        mm.get_tissue_mask(tissue_mask_opts)

        """If external DAB thresholds are used -- no one-class detection (since it's built into it)"""
        """If no external DAB thresholds are used AND/OR one-class detection is used"""

        if (cell_mask_opts["one_class_detection"]) & (~cell_mask_opts["external_dab_threshold_map"]):

            # one-class detection is valid; check for valid windows
            if [idx, 0] in index_of_valid_windows:
                mm.get_cell_mask(tissue_mask_opts, cell_mask_opts)
                cell_mask = mm.cell_mask
            else:
                cell_mask = dab_map_ > default_dab_threshold
        else:

            """If external DAB thresholds are used AND/OR one-class detection is NOT used"""
            # one-class detection is not used; either any method OR external DAB threshold map is used.
            mm.get_cell_mask(tissue_mask_opts, cell_mask_opts)
            cell_mask = mm.cell_mask

        if "mixture" in tissue_mask_opts:
            mixture_threshold = mm.mixture_thr
            mixture_threshold_list.append(mixture_threshold)

        threshold = mm.dab_threshold

        threshold_list.append(threshold)

        # allocate the mask accordingly
        total_RGB[:, idx * modified_window[0]:(idx + 1) * modified_window[0], :] = window
        total_mask[:, idx * modified_window[0]:(idx + 1) * modified_window[0]] = cell_mask

        del window
        del cell_mask
        del density_map_

    return total_RGB, total_mask, threshold_list


def column_segmentation_with_local_thresholds(metadata,
                                              column_opts,
                                              tissue_mask_opts,
                                              cell_mask_opts):
    # metadata information
    svs_fname = metadata["svs_fname"]

    # column information
    segmentation_window = column_opts["window_size"]
    modified_window = np.array([segmentation_window[0],
                                metadata["dimensions"][-1]])  # change to column window
    stain_separation_opt = column_opts["stain_separation"]
    coords_x = column_opts["coords_x"]

    width_of_total_column = (coords_x[-1] + segmentation_window[0]) - coords_x[0]

    # threshold lists
    threshold_list = []

    # full mask and RGB
    total_mask = np.zeros((modified_window[1], width_of_total_column))
    total_RGB = np.zeros((modified_window[1], width_of_total_column, 3))

    """Column-wise segmentation"""
    for idx, coord_x in enumerate(tqdm(coords_x)):

        cell_mask_opts["window_index_to_evaluate"] = [coord_x // segmentation_window[0], 0]

        colourmatrix = cell_mask_opts["colour_matrix_map"][cell_mask_opts["window_index_to_evaluate"][0],
                       0, :, :]

        # set stain index
        dab_idx, hema_idx = stain_indices(colourmatrix)

        # print(coord_x//segmentation_window[0])

        window = get_tile(svs_fname,
                          x=coord_x,  # col number
                          y=0,  # row number, start from top
                          sz_x=modified_window[0],
                          sz_y=modified_window[1])

        # stain separation

        if stain_separation_opt == "pNNLS":
            density_map_ = stain_separation.pNNLS(window, colourmatrix)
        else:
            density_map_ = stain_separation.matrix_inversion(window, colourmatrix)

        dab_map_ = density_map_[:, :, dab_idx]

        rgb_dab_map_ = OD_to_RGB(dab_map_)

        # using maskMaker to make 1) tissue mask to mask DAB channel, 2) cell mask, 3) tissue mask to normalize SAF
        mm = maskMaker.MaskMaker(rgb_dab_map_, window)
        mm.get_tissue_mask(tissue_mask_opts)
        mm.get_cell_mask(tissue_mask_opts, cell_mask_opts)
        cell_mask = mm.cell_mask
        cell_mask = maskMaker.morph_cell_mask(cell_mask, cell_mask_opts)
        threshold = mm.dab_threshold
        threshold_list.append(threshold)

        # allocate the mask accordingly
        total_RGB[:, idx * modified_window[0]:(idx + 1) * modified_window[0], :] = window
        total_mask[:, idx * modified_window[0]:(idx + 1) * modified_window[0]] = cell_mask

        del window
        del cell_mask
        del density_map_

    return total_RGB, total_mask, threshold_list


def column_segmentation_with_default_threshold(metadata,
                                               column_opts,
                                               tissue_mask_opts,
                                               cell_mask_opts):
    # metadata information
    svs_fname = metadata["svs_fname"]
    colourmatrix = metadata["colour_matrix"]  # default: use global colour matrix
    default_dab_threshold = metadata["default_thr_for_dab"]

    print("Slide-specific colour matrix: {}".format(colourmatrix))

    # column information
    segmentation_window = column_opts["window_size"]
    modified_window = np.array([segmentation_window[0],
                                metadata["dimensions"][-1]])  # change to column window
    stain_separation_opt = column_opts["stain_separation"]
    coords_x = column_opts["coords_x"]

    width_of_total_column = (coords_x[-1] + segmentation_window[0]) - coords_x[0]

    # threshold lists
    threshold_list = []

    # full mask and RGB
    total_mask = np.zeros((modified_window[1], width_of_total_column))
    total_RGB = np.zeros((modified_window[1], width_of_total_column, 3))

    """Column-wise segmentation w/ default threshold"""
    for idx, coord_x in enumerate(tqdm(coords_x)):

        # print(coord_x)

        # set stain index
        dab_idx, hema_idx = stain_indices(colourmatrix)

        # print(coord_x//segmentation_window[0])

        window = get_tile(svs_fname,
                          x=coord_x,  # col number
                          y=0,  # row number, start from top
                          sz_x=modified_window[0],
                          sz_y=modified_window[1])

        # stain separation

        if stain_separation_opt == "pNNLS":

            density_map_ = stain_separation.pNNLS(window, colourmatrix)
        else:
            density_map_ = stain_separation.matrix_inversion(window, colourmatrix)

        dab_map_ = density_map_[:, :, dab_idx]

        rgb_dab_map_ = OD_to_RGB(dab_map_)

        # using maskMaker to make 1) tissue mask to mask DAB channel, 2) cell mask, 3) tissue mask to normalize SAF
        mm = maskMaker.MaskMaker(rgb_dab_map_, window)
        mm.get_tissue_mask(tissue_mask_opts)

        cell_mask = rgb_dab_map_ < default_dab_threshold

        cell_mask = maskMaker.morph_cell_mask(cell_mask, cell_mask_opts)

        threshold = default_dab_threshold

        threshold_list.append(threshold)

        # allocate the mask accordingly
        total_RGB[:, idx * modified_window[0]:(idx + 1) * modified_window[0], :] = window
        total_mask[:, idx * modified_window[0]:(idx + 1) * modified_window[0]] = cell_mask

        del window
        del cell_mask
        del density_map_

    return total_RGB, total_mask, threshold_list


def column_segmentation_patch_visualization(metadata,
                                            column_opts,
                                            patch_opts,
                                            tissue_mask_opts,
                                            cell_mask_opts):
    # metadata information
    svs_fname = metadata["svs_fname"]
    colourmatrix = metadata["colour_matrix"]
    coords_x = metadata["coords_x"]
    coords_y = metadata["coords_y"]

    # column information
    segmentation_window = column_opts["window_size"]
    modified_window = np.array([segmentation_window[0],
                                metadata["dimensions"][-1]])  # change to column window
    stain_separation_opt = column_opts["stain_separation"]

    # patch information
    visualization_window = patch_opts["window_size"]
    percentile_x, percentile_y = patch_opts["coords_percentiles"]

    # set stain index
    dab_idx, hema_idx = stain_indices(colourmatrix)

    # threshold lists
    mixture_threshold_list = []
    threshold_list = []

    # coordinate range for patch
    patch_start_coord_x = np.percentile(coords_x, percentile_x)
    patch_start_coord_y = np.percentile(coords_y, percentile_y)

    patch_end_coord_x = (patch_start_coord_x + visualization_window[0]) // segmentation_window[0] * segmentation_window[
        0]
    patch_end_coord_y = patch_start_coord_y + visualization_window[1]

    coords_x = np.array(coords_x)

    coord_range_x = coords_x[(coords_x >= patch_start_coord_x) & (coords_x < patch_end_coord_x)]

    # misc. list
    dab_densities = []
    tissue_masks = []
    masked_dab_densities = []

    """Column-wise segmentation"""
    for idx, coord_x in enumerate(tqdm(coords_x)):

        window = get_tile(svs_fname,
                          x=coord_x,  # col number
                          y=0,  # row number, start from top
                          sz_x=modified_window[0],
                          sz_y=modified_window[1])

        # stain separation

        if stain_separation_opt == "pNNLS":

            density_map_ = stain_separation.pNNLS(window, colourmatrix)
        else:
            density_map_ = stain_separation.matrix_inversion(window, colourmatrix)

        dab_map_ = density_map_[:, :, dab_idx]

        # using maskMaker to make 1) tissue mask to mask DAB channel, 2) cell mask, 3) tissue mask to normalize SAF
        mm = maskMaker.MaskMaker(dab_map_, window)
        mm.get_tissue_mask(tissue_mask_opts)
        mm.get_cell_mask(tissue_mask_opts, cell_mask_opts)

        if coord_x in coord_range_x:
            # extract tissue mask and DAB channel histogram for patch only
            tissue_mask = mm.tissue_mask
            dab_densities.append(dab_map_)
            tissue_masks.append(tissue_mask)

            masked_ = dab_map_[tissue_mask > 0]  # mask it
            masked_ = masked_[masked_ > 0]  # get rid of 0s; this is what you feed Otsu

            masked_dab_densities.append(masked_)

        if "mixture" in tissue_mask_opts:
            mixture_threshold = mm.mixture_thr
            mixture_threshold_list.append(mixture_threshold)

        threshold = mm.dab_threshold

        threshold_list.append(threshold)

    # make into array
    threshold_list = np.array(threshold_list)

    # concatenate tissue masks to form one giant mask of all columns covered by patch

    tissue_mask = np.hstack(tissue_masks)

    # find index of coordinates in coord_range x (for patch), in overall coordinate x (for columns).
    coords_x_sorted = np.argsort(coords_x)
    coord_range_x_pos = np.searchsorted(coords_x[coords_x_sorted], coord_range_x)
    indices_x = coords_x_sorted[coord_range_x_pos]

    # find the thresholds for the patch coordinates

    thresholds_to_use = threshold_list[indices_x]
    tissue_mask_to_use = tissue_mask[int(patch_start_coord_y):int(patch_end_coord_y), :]

    """Patch extraction plus apply thresholds"""

    # perform segmentation on the patch

    window = get_tile(svs_fname,
                      x=int(patch_start_coord_x),  # col number
                      y=int(patch_start_coord_y),  # row number, start from top
                      sz_x=visualization_window[0],
                      sz_y=visualization_window[1])

    # stain separation

    if stain_separation_opt == "pNNLS":

        density_map_ = stain_separation.pNNLS(window, colourmatrix)
    else:
        density_map_ = stain_separation.matrix_inversion(window, colourmatrix)

    dab_map_patch = density_map_[:, :, dab_idx]
    hema_map_patch = density_map_[:, :, hema_idx]

    # repeat the thresholds to get the same size as the columns

    thresholds_to_use = np.repeat(thresholds_to_use, modified_window[0])  # repeat by the width of to columns

    thresholds_to_use_in_patch = np.tile(thresholds_to_use, (
        dab_map_patch.shape[0], 1))  # expand it row-wise to get the exact same dimensions as the patch

    """Final masking"""
    segmentation_of_patch = dab_map_patch > thresholds_to_use_in_patch

    """Saving different results as dictionaries"""
    patch = {"segmentation": segmentation_of_patch,
             "dab_channel": dab_map_patch,
             "hema_channe": hema_map_patch,
             "RGB": window,
             "tissue_mask": tissue_mask_to_use}

    misc = {"dab_histogram": np.array(dab_densities),
            "tissue_masks": np.array(tissue_masks),
            "masked_dab_histograms": masked_dab_densities}
    # save thresholds for reference

    if "mixture" in tissue_mask_opts:
        mixture_threshold_list = np.array(mixture_threshold_list)

        mixture_threshlds_to_use = mixture_threshold_list[indices_x]

        mixture_thresholds_to_use_in_patch = np.tile(mixture_threshlds_to_use,
                                                     (dab_map_patch.shape[0],
                                                      1))

        thresholds = {"DAB_thresholds": threshold_list,
                      "thresholds_used": thresholds_to_use_in_patch,
                      "mixture_thresholds": mixture_threshold_list,
                      "mixture_thresholds used": mixture_thresholds_to_use_in_patch,
                      }

    else:

        thresholds = {"DAB_thresholds": threshold_list,  # one for each column
                      "thresholds_used": thresholds_to_use_in_patch,
                      }

    return patch, thresholds, misc


def plot_map(img, vmax=0.03, vmin=0, figsize=None, cmap="hot", cb=True):
    if figsize is None:
        figsize = [20, 20]
    fig = plt.figure(figsize=(figsize[0], figsize[1]))
    plt.imshow(img, vmax=vmax, vmin=vmin, cmap=cmap)
    plt.yticks([])
    plt.xticks([])

    if cb:
        cax = fig.add_axes([0.9, 0.167, 0.02, 0.673])
        cb = plt.colorbar(cax=cax)
        cb.ax.tick_params(labelsize=20)

    plt.close()
    return fig


"""Minor preprocessing"""


def stain_indices(cm):
    dab_idx = np.argmax(cm[:, -1])
    hema_idx = np.abs(np.argmax(cm[:, -1]) - 1)

    return dab_idx, hema_idx


def standardize_brightness(I, perc=90):
    """
    Scale all the intensities in an image to a threshold set by a percentile.
    Clips intensities above the threshold.

    :param I:
    :return: image clipped to the percentile range
    """
    p = np.percentile(I, perc)
    return np.clip(I * 255.0 / p, 0, 255).astype(np.uint8)


def remove_zeros(I):
    """
    Remove zeros, replace with 1 (on a scale of 255)
    :param I: uint8 array
    :return:
    """
    mask = (I == 0)
    I[mask] = 1
    return I


def normalize_rows(A):
    """
    Normalize rows of an array
    :param A:
    :return:
    """
    return A / np.linalg.norm(A, axis=1)[:, None]


def notwhite_mask(I, thresh=0.8):
    """
    Get a binary mask where true denotes 'not white'
    Effectively a background/tissue mask

    :param I:
    :param thresh:
    :return:
    """

    I_LAB = rgb2lab(I)
    L = I_LAB[:, :, 0] / 255.0
    return (L < thresh)


"""Custom conversion"""


def RGB_to_OD(I):
    """
    Convert from RGB to optical density
    :param I:
    :return:
    """
    I = remove_zeros(I)
    return -1 * np.log10(I / 255)


def OD_to_RGB(OD, invert=False):
    """
    Convert from optical density to RGB
    :param invert: invert the values so high density is low RGB (i.e. bright DAB)
    :param OD:
    :return:
    """

    if invert:
        return np.invert((255 * 10 ** (-1 * OD)).astype(np.uint8))

    else:
        return (255 * 10 ** (-1 * OD)).astype(np.uint8)


def RGB_to_HSD(I):
    I = I[:, :, :3]

    OD = RGB_to_OD(I)
    ODm = np.mean(OD, axis=2)
    ODm[ODm == 0] = 10 ** -10
    OD[OD == 0] = 10 ** -10  # added to prevent error messages
    # Bring to cx and cy coordinates
    # Hue is angle cx,cy makes, and saturation is norm of cx,cy
    cx = OD[:, :, 0] / ODm - 1
    cy = (OD[:, :, 1] - OD[:, :, 2]) / (ODm * np.sqrt(3))
    return OD, cx, cy


def OD_to_HSD(OD):
    OD = np.squeeze(OD)

    if len(OD.shape) == 1:
        OD = OD[np.newaxis, np.newaxis, :]

    ODm = np.mean(OD, axis=2)
    ODm[ODm == 0] = 10 ** -10
    OD[OD == 0] = 10 ** -10  # added to prevent error messages
    # Bring to cx and cy coordinates
    # Hue is angle cx,cy makes, and saturation is norm of cx,cy
    cx = OD[:, :, 0] / ODm - 1
    cy = (OD[:, :, 1] - OD[:, :, 2]) / (ODm * np.sqrt(3))
    return cx, cy


def HSD_to_RGB(cx, cy):
    R = cx + 1
    G = 0.5 * (2 - cx + np.sqrt(3) * cy)
    B = 0.5 * (2 - cx - np.sqrt(3) * cy)
    return np.dstack(R, G, B), np.exp(-np.dstack(R, G, B))


def HSD_to_RGBvec(cx, cy):
    R = cx + 1
    G = 0.5 * (2 - cx + np.sqrt(3) * cy)
    B = 0.5 * (2 - cx - np.sqrt(3) * cy)
    return np.array([R, G, B]).reshape((1, 3))


def RGBvec_to_HSD(vec):
    mean_ = np.mean(vec)
    cx = vec[0] / mean_ - 1
    cy = (vec[1] - vec[2]) / (np.sqrt(3) * mean_)

    return cx, cy


def density_map_to_RGB(density, colourmatrix):
    # normalize colour matrix to be sure
    colourmatrix = colour_matrix.normalize_colourmatrix(colourmatrix)

    dab_index, hema_idx = stain_indices(colourmatrix)
    dab_cv, hema_cv = colourmatrix[dab_index, :], colourmatrix[hema_idx, :]

    # select channels

    DAB = density[:, :, dab_index]
    hema = density[:, :, hema_idx]

    # convert to RGB

    DAB_rgb = np.stack((DAB,) * 3, axis=-1)
    DAB_rgb = DAB_rgb * dab_cv
    DAB_rgb = OD_to_RGB(DAB_rgb)

    hema_rgb = np.stack((hema,) * 3, axis=-1)
    hema_rgb = hema_rgb * hema_cv
    hema_rgb = OD_to_RGB(hema_rgb)

    return DAB_rgb, hema_rgb


"""Angle between colour vectors
Pillaged from https://stackoverflow.com/questions/2827393/angles-between-two-n-dimensional-vectors-in-python
"""


def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)


def angle_between(v1, v2):
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))
