#!/usr/bin/env python
"""


Module to perform structure tensor analysis
"""
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors
from scipy import ndimage
from os.path import join

from modules import dir_utils

"""Structure tensor functions"""

def structure_tensor(im, spatial):
    # Assumes IM is a 2D numpy array (double precision)

    if im.max() > 1:
        im = im / im.max()

    # Calculate directional derivatives dIM/dx and dIM/dy
    # imx,imy = np.gradient(im)
    imx = ndimage.sobel(im, axis=1, mode='mirror')
    imy = ndimage.sobel(im, axis=0, mode='mirror')

    # Calculate orientation for each pixel
    # Compononents of the symmetric structure tensor:
    # [d^2/dx       d/dx*d/dy
    # [d/dx*d/dy    d^2/dy]
    xy = ndimage.gaussian_filter(imy * imx, sigma=spatial, mode='mirror')  # 3rd entry
    xx = ndimage.gaussian_filter(imx ** 2, sigma=spatial, mode='mirror')  # 1st entry
    yy = ndimage.gaussian_filter(imy ** 2, sigma=spatial, mode='mirror')  # 2nd entry

    # Orientation map
    orientation = 0.5 * np.arctan2(2 * xy, (yy - xx))
    # Energy map
    energy = xx + yy

    # Anisotropy index map
    structensor = np.concatenate((xx[..., np.newaxis],
                                  yy[..., np.newaxis],
                                  xy[..., np.newaxis]), axis=2)

    structensor = np.transpose(structensor, (2, 0, 1))

    eigvals, eigvecs = eig_special_2d(structensor)

    ai = (eigvals[1, ...] - eigvals[0, ...]) / (eigvals[0, ...] + eigvals[1, ...])

    ai[np.isnan(ai)] = 0
    ai[np.isinf(ai)] = 0

    return orientation, energy, ai

def eig_special_2d(S):
    """Eigensolution for symmetric real 2-by-2 matrices.
    Arguments:
        S: ndarray
            A floating point array with shape (3, ...) containing structure tensor.
    Returns:
        val: ndarray
            An array with shape (2, ...) containing sorted eigenvalues.
        vec: ndarray
            An array with shape (2, ...) containing eigenvector corresponding
            to the smallest eigenvalue (the other is orthogonal to the first).
    Authors:
        vand@dtu.dk, 2019; niejep@dtu.dk, 2020
    """

    # Save original shape and flatten.
    input_shape = S.shape
    S = S.reshape(3, -1)

    # Calculate val.
    val = np.empty((2, S.shape[1]), dtype=S.dtype)
    np.subtract(S[0], S[1], out=val[1])
    val[1] *= val[1]
    np.multiply(S[2], S[2], out=val[0])
    val[0] *= 4
    val[1] += val[0]
    np.sqrt(val[1], out=val[1])
    np.negative(val[1], out=val[0])
    val += S[0]
    val += S[1]
    val *= 0.5

    # Calcualte vec, y will be positive.
    vec = np.empty((2, S.shape[1]), dtype=S.dtype)
    np.negative(S[2], out=vec[0])
    np.subtract(S[0], val[0], out=vec[1])

    # Deal with diagonal matrices.
    aligned = S[2] == 0

    # Sort.
    vec[:, aligned] = 1 - np.argsort(S[:2, aligned], axis=0)

    # Normalize.
    vec_norm = np.einsum('ij,ij->j', vec, vec)
    np.sqrt(vec_norm, out=vec_norm)
    vec /= vec_norm

    # Reshape and return.
    return val.reshape(val.shape[:1] + input_shape[1:]), vec.reshape(vec.shape[:1] + input_shape[1:])


"""Orientation conversion"""


def convert_rad_to_deg(O):
    return (O / (np.pi) * 180)


def wrap(O):
    return np.arctan2(np.sin(O), np.cos(O))


"""Visualization"""
from matplotlib.colors import hsv_to_rgb

def tensor_to_HSV(orientation, anistropy, rgb_dab, hue_theta_zero, hue_theta_pi):
    H = hue_theta_zero + (1 / np.pi) * (hue_theta_pi - hue_theta_zero) * np.copy(orientation)
    S = np.copy(anistropy)
    V = np.copy(rgb_dab)

    # c convert HSV to RGB

    hsv = np.concatenate((H[..., np.newaxis],
                          S[..., np.newaxis],
                          V[..., np.newaxis]), axis=2)

    # S[S==0] = 1e-6

    # imgHSV = hsv_to_rgb(hsv)

    hsv = hsv_to_rgb(hsv)
    hsv = np.uint8(255 * hsv)

    return hsv

"""
def save_intermediate(idx, map, savepath):

    if idx in [50, 100, 150, 200, 500, 750]:

        cmap_settings = ('jet', None)  # build 1st colour map
        symmmetric_jet = symmetrical_colormap(cmap_settings=cmap_settings, new_name=None)
        symmmetric_jet.set_bad(color='black')  # set nans to black background

        brb = build_custom_continuous_cmap([0, 0, 255], [255, 0, 0],
                                           [0, 0, 255])  # build 2nd colour map red = left/right, blue = up/down
        brb.set_bad(color='black')  # set nans to black background

        plt.figure(figsize=(20, 20))
        plt.imshow(map, cmap=symmmetric_jet, vmax=90, vmin=-90)
        plt.savefig(join(savepath, "orientation_{}_jet.png".format(idx)))
        plt.close()

        plt.figure(figsize=(20, 20))
        plt.imshow(map, cmap=brb, vmax=90, vmin=-90)
        plt.savefig(join(savepath, "orientation_{}_brb.png".format(idx)))
        plt.colorbar()
        plt.close()
        
"""
def save_intermediate(idx, map, dirpath):

    if idx in [50, 100, 150, 200, 300, 400, 500, 750]:

        hsv_map = tensor_to_HSV(map[..., 0],
                                map[..., 1],
                                map[..., 2],
                                hue_theta_zero=0,
                                hue_theta_pi=1)


        dir_utils.save_arr_to_png(hsv_map,
                                  dir_=dirpath,
                                  name_="hsv_sta_map_i{}".format(idx))




"""Colour maps"""


def symmetrical_colormap(cmap_settings, new_name=None):
    ''' This function take a colormap and create a new one, as the concatenation of itself by a symmetrical fold.
    '''
    # get the colormap
    cmap = plt.cm.get_cmap(*cmap_settings)
    if not new_name:
        new_name = "sym_" + cmap_settings[0]  # ex: 'sym_Blues'

    # this defined the roughness of the colormap, 128 fine
    n = 128

    # get the list of color from colormap
    colors_r = cmap(np.linspace(0, 1, n))  # take the standard colormap # 'right-part'
    colors_l = colors_r[::-1]  # take the first list of color and flip the order # "left-part"

    # combine them and build a new colormap
    colors = np.vstack((colors_l, colors_r))
    mymap = mcolors.LinearSegmentedColormap.from_list(new_name, colors)

    return mymap


def build_custom_continuous_cmap(*rgb_list):
    '''
    Generating any custom continuous colormap, user should supply a list of (R,G,B) color taking the value from [0,255], because this is
    the format the adobe color will output for you.

    Examples::

        test_cmap = build_custom_continuous_cmap([64,57,144],[112,198,162],[230,241,146],[253,219,127],[244,109,69],[169,23,69])
        fig,ax = plt.subplots()
        fig.colorbar(cm.ScalarMappable(norm=colors.Normalize(),cmap=diverge_cmap),ax=ax)

    .. image:: ./_static/custom_continuous_cmap.png
        :height: 400px
        :width: 550px
        :align: center
        :target: target

    '''
    all_red = []
    all_green = []
    all_blue = []
    for rgb in rgb_list:
        all_red.append(rgb[0])
        all_green.append(rgb[1])
        all_blue.append(rgb[2])
    # build each section
    n_section = len(all_red) - 1
    red = tuple([(1 / n_section * i, inter_from_256(v), inter_from_256(v)) for i, v in enumerate(all_red)])
    green = tuple([(1 / n_section * i, inter_from_256(v), inter_from_256(v)) for i, v in enumerate(all_green)])
    blue = tuple([(1 / n_section * i, inter_from_256(v), inter_from_256(v)) for i, v in enumerate(all_blue)])
    cdict = {'red': red, 'green': green, 'blue': blue}
    new_cmap = colors.LinearSegmentedColormap('new_cmap', segmentdata=cdict)
    return new_cmap


def inter_from_256(x):
    return np.interp(x=x, xp=[0, 255], fp=[0, 1])
