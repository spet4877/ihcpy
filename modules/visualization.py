import imageio
import matplotlib.pyplot as plt
from mpl_toolkits import axes_grid1

import os
import cv2
from io import BytesIO
from PIL import Image

from datetime import datetime
from pptx import Presentation
from pptx.util import Inches
from pptx.util import Inches, Pt

"""Plots"""
def add_colorbar(im, aspect=20, pad_fraction=0.5, location="right", **kwargs):
    """Add a vertical color bar to an image plot."""
    divider = axes_grid1.make_axes_locatable(im.axes)
    width = axes_grid1.axes_size.AxesY(im.axes, aspect=1./aspect)
    pad = axes_grid1.axes_size.Fraction(pad_fraction, width)
    current_ax = plt.gca()
    cax = divider.append_axes(location, size=width, pad=pad)
    plt.sca(current_ax)
    return im.axes.figure.colorbar(im, cax=cax, **kwargs)

"""Powerpoint"""

def initialize_pptx(outpath, title=None, comments=None, size=None, fname=None):
    if title is None:
        title = "Summary plots for each WSI"
    date = datetime.today()
    author = "Daniel Kor"

    if fname is None:
        pptxfname = os.path.join(outpath, "summary.pptx")
    else:
        pptxfname = os.path.join(outpath, "{}.pptx".format(fname))

    prs = Presentation()

    if size is None:
        size = [10, 10]
    prs.slide_width = Inches(size[0])
    prs.slide_height = Inches(size[1])

    blank_slide_layout = prs.slide_layouts[1]
    slide = prs.slides.add_slide(blank_slide_layout)

    # make first slide with our metadata
    slide.placeholders[0].text = title

    tf = slide.placeholders[1].text_frame
    tf.text = f'Date: {date}\n'
    tf.text += f"Author: {author}\n"
    tf.text += f"Comments: {comments}\n"

    return pptxfname, prs

def image_to_slide(img_dir, slide_opts):
    # get parameters
    layout = slide_opts["layout"]
    image_size = slide_opts["image_size"]
    prs = slide_opts["prs"]

    # start creating slide
    blank_slide_layout = prs.slide_layouts[layout]
    slide = prs.slides.add_slide(blank_slide_layout)

    # load png image
    img = imageio.imread(img_dir)

    # position image
    if "positon" in slide_opts:
        x, y = slide_opts["position"]
    else:
        x, y = 0, 0

    # attach image
    addimagetoslide(slide, img, Inches(x), Inches(y), Inches(image_size[0]), Inches(image_size[1]),
                    back=True)

    # add label if necessary
    if "label" in slide_opts:
        label_opts = slide_opts["label"]

        txBox = slide.shapes.add_textbox(Inches(0), Inches(0), Inches(image_size[0] / 2), Inches(image_size[1] / 2))
        tf = txBox.text_frame
        p = tf.add_paragraph()
        p.text = label_opts[0]
        p.font.size = Pt(label_opts[2])
        p.font.color.rgb = label_opts[1]
        p.bold = True

    return prs

# wrapper function to add an image as a byte stream to a slide
# note that this is in place of having to save output directly to disk, and can be used in dynamic settings as well
def addimagetoslide(slide, img, left, top, height, width, resize=None, back=False):
    if resize is None:
        resize = [1, 1]

    res = cv2.resize(img, None, fx=resize[0], fy=resize[1],
                     interpolation=cv2.INTER_CUBIC)  # since the images are going to be small, we can resize them to
    # prevent the final pptx file from being large for no reason
    image_stream = BytesIO()
    Image.fromarray(res).save(image_stream, format="PNG")

    pic = slide.shapes.add_picture(image_stream, left, top, height, width)

    if back:
        slide.shapes._spTree.insert(2, pic._element)

    image_stream.close()