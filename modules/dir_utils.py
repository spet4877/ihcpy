"""

Module to keep useful functions for saving arrays to:
- Nifti
- Npz
- Pandas dataframe (for WSI metadata)


Params:

- All images (I) are assumed to be RGB, and in range [0,255]

"""


import os
from os.path import join

"""Directory management"""


def create_folder(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)

    return folder


"""TIFF and PNG"""

import tifffile as tiff
import imageio

def save_arr_to_tiff(arr, dir_, name_):
    """This is to save the actual floating-point values.
    This is the format used in TIRL."""

    tiff_name = join(dir_,name_ + ".tiff")
    tiff.imsave(tiff_name, arr)


def save_arr_to_png(arr, dir_, name_):
    """This is for visualization; DON'T USED THESE VALUES FOR COMPUTING"""
    png_name = join(dir_, name_ + ".png")
    imageio.imsave(png_name, arr)


"""Pickle"""

import pickle


def data_to_pickle(fname,
                   output_dir,
                   data):
    with open(join(output_dir, fname + '.p'), 'wb') as fp:
        pickle.dump(data, fp, protocol=pickle.HIGHEST_PROTOCOL)


def pickle_to_data(full_fname):
    with open(full_fname, 'rb') as fp:
        data = pickle.load(fp)

    return data


"""CSV"""
import csv


def dict_to_csv(fname,
                output_dir,
                data):
    csv_columns = data.keys()

    join(output_dir, fname)

    csv_file = join(output_dir, fname + '.csv')
    dict_data = data

    file_exists = os.path.isfile(csv_file)
    try:
        with open(csv_file, 'a') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)

            if not file_exists:
                writer.writeheader()

            writer.writerow(dict_data)
    except IOError:
        print("I/O error")


def pickle_to_csv(full_fname,
                  csv_fname,
                  output_dir):
    # load pickle to dictionary
    with open(full_fname, 'rb') as fp:
        data = pickle.load(fp)

    # save dictionary to csv

    dict_to_csv(csv_fname,
                output_dir,
                data)


"""JSON"""

import json


def dict_to_json(fname,
                 output_dir,
                 data):
    outfile = join(output_dir, fname + '.json')

    try:
        with open(outfile, "a") as outfile:
            json.dump(data, outfile, indent=2)
            outfile.write('\n')
    except IOError:
        print("I/O error")


def json_to_dict(json_full_fname):
    return json.loads(open(json_full_fname).read())


def json_to_csv(json_full_fname,
                csv_fname,
                csv_output_dir):
    data = json_to_dict(json_full_fname)
    dict_to_csv(csv_fname,
                csv_output_dir,
                data)


def multiple_json_to_single_csv(rootpath,
                                json_fname,
                                csv_outpath,
                                csv_fname):

    """Reads for JSON files found in folders within the rootpath.
    The json_fname is the name of the file to look for.
    Save the information to a single csv file name csv_fname and save it to the csv_outpath"""
# TODO:
# reading many


"""Loading old hdf5 data (saved for the original CD68 pipeline"""

#(not worth it for now)
