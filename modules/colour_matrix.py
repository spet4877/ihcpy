#!/usr/bin/env python
"""


Module to keep derive SLICE-SPECIFIC colour matrix.

Includes:

- Patch extraction (sampling patches from the WSI for the data-driven methods)
- K-means clustering method inspired by
    Geijs, D.J et. al. in "Automatic color unmixing of IHC stained whole slide images"

- SVD-geodesic method inspired by
    M. Macenko et al., ‘A method for normalizing histology slides for quantitative analysis’, in 2009 IEEE International Symposium on Biomedical Imaging: From Nano to Macro, 2009, pp. 1107–1110.

- Dictionary method (as part of S-NMF) inspired by (NOT IMPLEMENTED AS TOO SLOW)
    A. Vahadane et al., ‘Structure-Preserving Color Normalization and Sparse Stain Separation for Histological Images’, IEEE Transactions on Medical Imaging, vol. 35, no. 8, pp. 1962–1971, Aug. 2016.
    (NOTE: this takes a LONG time)

Params:

- All images (I) are assumed to be RGB, and in range [0,255]

"""

from __future__ import division
import openslide
import numpy as np

from modules import tissue_masking, stain_utils
from skimage.color import rgb2lab

"""Patch extraction"""

def extract_patches(file, n=1024, size=512, option=1, threshold_values=None, verbose=True):
    """
    Sample n patches (dimensions size x size) from the file containing the WSI.
    Provides 4 options of collecting valid (i.e. non-background) pixels:
    - "1" : Masking pixels with L mask, and using the  of non-white pixels.
    - "2" : Masking pixels with average OD mask, and using the patches > threshold value of  non-white pixels.
    - "3" : Checking average L values, and keep the patch < a threshold value (above it is white pixels).
    - "4" : Checking average OD/absorbance values, and keep the patch > threshold value (below it is white pixels)

    :param file is the patch of the .svs file
    :param threshold_values: 1
    :return: patch_array (array of patches)
    """

    if threshold_values is None:
        threshold_values = [0.8, 0.2]

        # for options 1,2, both values are needed. First value is needed for the mask; second value for % pixels in the patch.
        # for options 3,4, this is the threshold to compare the average luminance/OD value with.

    frame = openslide.OpenSlide(file)

    rows, cols = frame.dimensions
    images = []
    i = 0

    center = [rows // 2, cols // 2]
    offset = int(1e4)

    # masks = []
    np.random.seed(23)  # give the same patches

    while i < n:


        sx = np.random.randint((center[1] - offset) + (size // 2), (center[1] + offset) - size // 2, 1)
        sy = np.random.randint((center[0] - offset) + (size // 2), (center[0] + offset) - size // 2, 1)

        img = np.array(frame.read_region((sx, sy), 0, (size, size)))

        img = img[:, :, 0:3]
        # print("Selected")

        if option == 1:  # mask pixel with an L mask produced from threshold_values[0]

            I_LAB = rgb2lab(img)
            L = I_LAB[:, :, 0] / 100.0
            l_mask_nonwhite = L < threshold_values[0]
            l_mask_nonblack = L > threshold_values[1]

            # print(np.count_nonzero(l_mask))
            perc_nonwhite = np.count_nonzero(l_mask_nonwhite) / (size ** 2)
            perc_nonblack = np.count_nonzero(l_mask_nonblack) / (size ** 2)
            if (perc_nonwhite > threshold_values[2]) and (perc_nonblack > threshold_values[3]):
                # print(perc_nonwhite)
                images.append(img)
                i += 1
                verbosemsg(i, n, verbose)
                # masks.append(l_mask)

        elif option == 2:
            OD_mask = tissue_masking.avg_OD_mask(img, thresh=threshold_values[0])
            perc_nonwhite = np.count_nonzero(OD_mask) / (size ** 2)
            if perc_nonwhite > threshold_values[1]:
                images.append(img)
                i += 1
                verbosemsg(i, n, verbose)
        elif option == 3:
            I_LAB = rgb2lab(img)
            L = I_LAB[:, :, 0] / 255.0
            avg_ = np.mean(L)

            if avg_ < threshold_values[0]:
                images.append(img)
                i += 1
                verbosemsg(i, n, verbose)
        elif option == 4:
            OD_ = stain_utils.RGB_to_OD(img)
            avg_ = np.mean(OD_)
            if avg_ > threshold_values[0]:
                images.append(img)
                i += 1
                verbosemsg(i, n, verbose)

    return np.array(images)


""" Literature and manual method """


def literature():
    """The colour matrix acquired by Ruifrok and Johnston (2001)"""
    colourmatrix = np.array([[0.65, 0.70, 0.29],  # hema
                             [0.27, 0.57, 0.78],  # DAB
                             [0, 0, 0],  # eosin
                             ])
    return colourmatrix

def CD68_manual():
    """The colour matrix acquired by Adele Smart for
    the CD68 reproducibility data.

    Instrument: Aperio ScanScope AT Turbo (Leica Biosystems)
    Software: ImageScope software (v12.3.2.8013)"""

    colourmatrix = np.array([[0.516, 0.752, 0.707],  # hema
                             [0.36, 0.593, 0.72],  # DAB
                             [0, 0, 0],  # residual
                             ])
    return colourmatrix

def Iba1_manual():
    """The colour matrix acquired by Adele Smart for
    the Iba1 reproducibility data.

    Instrument: Aperio ScanScope AT Turbo (Leica Biosystems)
    Software: ImageScope software (v12.3.2.8013)"""

    colourmatrix = np.array([[0.516, 0.752, 0.707],  # hema
                             [0.36, 0.593, 0.72],  # DAB
                             [0, 0, 0],  # residual
                             ])
    return colourmatrix

def PLP_manual():
    """The colour matrix acquired by Adele Smart for
    the PLP reproducibility data.

    Instrument: Aperio ScanScope AT Turbo (Leica Biosystems)
    Software: ImageScope software (v12.3.2.8013)"""

    colourmatrix = np.array([[0.6, 1, 1],  # hema
                             [0.382, 0.56, 0.698],  # DAB
                             [0, 0, 0],  # residual
                             ])
    return colourmatrix

def SMI312_manual():
    """The colour matrix acquired by Adele Smart for
    the CD68 reproducibility data.

    Instrument: Aperio ScanScope AT Turbo (Leica Biosystems)
    Software: ImageScope software (v12.3.2.8013)"""

    colourmatrix = np.array([[0.526, 0.69, 0.498],  # hema
                             [0.391, 0.57, 0.776],  # DAB
                             [0, 0, 0],  # residual
                             ])
    return colourmatrix

def ferritin_manual():
    """The colour matrix acquired by Adele Smart for
    the CD68 reproducibility data.

    Instrument: Aperio ScanScope AT Turbo (Leica Biosystems)
    Software: ImageScope software (v12.3.2.8013)"""

    colourmatrix = np.array([[0.573, 0.667, 0.477],  # hema
                             [0.37, 0.597, 0.782],  # DAB
                             [0, 0, 0],  # residual
                             ])
    return colourmatrix

"""Data-driven methods to derive slide-specific colour matrix"""


def k_means(images,
            option=1,
            threshold_value=0.8,
            verbose=True):
    """

    Using K means on an array of extracted patches to derive colour information for the slide.
    Provides 2 options of to extract valid pixels from the patches:
    - "1" : Use the luminance channel; only extract pixels < threshold value (i.e. higher L = whiter pixel)
    - "2" : Use the absorbance/OD channel; only extract pixels > threshold value (i.e. lower OD = whiter pixel)

    :param images: array of patches
    :param option: to decide which method to extract pixels with stains.
    :param threshold_value: the threshold value to exclude white pixels (i.e. background)
    :param verbose:
    :return: colourmatrix: 3 x 3 array with rows being the stain, columns being the RGB absorbance coefficient
    """
    from sklearn.cluster import KMeans

    kc1 = []
    kc2 = []
    eucl_dist = []

    n_img = images.shape[0]

    for i in range(n_img):
        img = np.squeeze(images[i])

        OD, cx, cy = stain_utils.RGB_to_HSD(img)

        if option == 1:  # Using luminance value

            img_to_mask = rgb2lab(img)[:, :, 0] / 100.00
            condition = img_to_mask < threshold_value

        else:  # Using mean OD/absorbance value
            img_to_mask = np.mean(OD, axis=2)
            condition = img_to_mask > threshold_value

        # Make vector of cx and cy coordinates with pixel intensities above a threshold
        cxcy = np.array([cx[condition].flatten(),
                         cy[condition].flatten()]).T

        # Remove NaN entries
        if np.sum(np.isnan(cxcy.flatten())) > 0:
            cxcy = cxcy[~np.isnan(np.sum(cxcy, axis=1)), :]

        # At least x point to perform Kmeans
        if cxcy.shape[0] > 100:
            # Do kmeans clustering to detect two colours
            kmeans = KMeans(n_clusters=2, random_state=0).fit(cxcy)
            # Store cluster centroids and the Euclidean distance between them
            kc1.append(kmeans.cluster_centers_[0, :])
            kc2.append(kmeans.cluster_centers_[1, :])
            eucl_dist.append(np.linalg.norm(kc1[-1] - kc2[-1]))
        if verbose & (np.remainder(i, n_img // 10) == 0):
            print('Kmeans on {:d} of {:d} images'.format(i, n_img))
    # Check clusters with a large Euclidean distance between them
    kc1, kc2, eucl_dist = np.array(kc1), np.array(kc2), np.array(eucl_dist)
    # idx = (eucl_dist < np.percentile(eucl_dist, 98)) & (eucl_dist > np.percentile(eucl_dist, 95))
    idx = (eucl_dist > np.percentile(eucl_dist, 95))

    clusters = np.concatenate((kc1[idx], kc2[idx]), axis=0)
    clusters = KMeans(n_clusters=2, random_state=0).fit(clusters)
    # Final colour centroids
    stain1 = clusters.cluster_centers_[0, :]
    stain2 = clusters.cluster_centers_[1, :]

    # Get colour matrix from the cluster centroids
    colourmatrix = get_vectors(stain1, stain2)

    # Set the residual stain vector with the cross-product

    colourmatrix = set_residual_colour_vector(colourmatrix)

    return colourmatrix


def SVD_geodesic(images,
                 option=1,
                 threshold_value=0.8,
                 alpha=98,
                 verbose=True):
    """
    Performs the SVD method on all patches extracted from the WSI to get colour matrices.
    The mean of the all colour matrices is the WSI colour matrix.

    :param I:
    :param beta:
    :param alpha:
    :return:
    """

    svd_array = []

    for idx in range(images.shape[0]):
        # print(idx)
        image = images[idx, :, :, :]
        OD = stain_utils.RGB_to_OD(image).reshape((-1, 3))

        if option == 1:  # Luminance
            mask = tissue_masking.luminance_mask(image, thresh=threshold_value).reshape(
                (-1,))  # mask non-tissue using LAB mask
            OD = stain_utils.RGB_to_OD(image).reshape((-1, 3))  # convert to OD
            OD = OD[mask]  # apply mask


        else:  # Absorbance/OD

            mask = tissue_masking.avg_OD_mask(image, thresh=threshold_value).reshape(
                (-1,))  # mask non-tissue using LAB mask
            OD = stain_utils.RGB_to_OD(image).reshape((-1, 3))  # convert to OD
            OD = OD[mask]  # apply mask

        # OD = (OD[(OD > threshold_value).any(axis=1), :]) #remove those with OD<0.15
        _, V = np.linalg.eigh(np.cov(OD, rowvar=False))  # SVD

        # print(V)

        V = V[:, [2, 1]]
        if V[0, 0] < 0: V[:, 0] *= -1
        if V[0, 1] < 0: V[:, 1] *= -1
        That = np.dot(OD, V)
        phi = np.arctan2(That[:, 1], That[:, 0])
        minPhi = np.percentile(phi, alpha)
        maxPhi = np.percentile(phi, 100 - alpha)
        v1 = np.dot(V, np.array([np.cos(minPhi), np.sin(minPhi)]))
        v2 = np.dot(V, np.array([np.cos(maxPhi), np.sin(maxPhi)]))
        single_patch_cm = np.array([v1, v2])

        svd_array.append(single_patch_cm)

        if verbose & (np.remainder(idx, images.shape[0] // 10) == 0):
            print('SVD {:d} of {:d} images'.format(idx, images.shape[0]))

    svd_array = np.array(svd_array)
    colourmatrix = svd_array.mean(axis=0)
    colourmatrix = np.vstack((colourmatrix, [0, 0, 0]))
    colourmatrix = set_residual_colour_vector(colourmatrix)

    # colourmatrix = stain_utils.normalize_rows(colourmatrix)

    return colourmatrix


def SVD_geodesic_with_distance(images,
                               option=1,
                               threshold_value=0.8,
                               alpha=98,
                               verbose=True):
    """
    Performs the SVD method on all patches extracted from the WSI to get colour matrices.
    Transforms each colour matrix into the HSD space, and removes the patches with Euclidean distances < percentile value.
    Take the mean (or median) or all the colour matrices used.

    :param I:
    :param beta:
    :param alpha:
    :return:
    """

    svd_array = []

    for idx in range(images.shape[0]):
        # print(idx)
        image = images[idx, :, :, :]
        OD = stain_utils.RGB_to_OD(image).reshape((-1, 3))

        if option == 1:  # Luminance
            mask = tissue_masking.luminance_mask(image, thresh=threshold_value).reshape(
                (-1,))  # mask non-tissue using LAB mask
            OD = stain_utils.RGB_to_OD(image).reshape((-1, 3))  # convert to OD
            OD = OD[mask]  # apply mask


        else:  # Absorbance/OD

            mask = tissue_masking.avg_OD_mask(image, thresh=threshold_value).reshape(
                (-1,))  # mask non-tissue using LAB mask
            OD = stain_utils.RGB_to_OD(image).reshape((-1, 3))  # convert to OD
            OD = OD[mask]  # apply mask

        # OD = (OD[(OD > threshold_value).any(axis=1), :]) #remove those with OD<0.15
        _, V = np.linalg.eigh(np.cov(OD, rowvar=False))  # SVD

        # print(V)

        V = V[:, [2, 1]]
        if V[0, 0] < 0: V[:, 0] *= -1
        if V[0, 1] < 0: V[:, 1] *= -1
        That = np.dot(OD, V)
        phi = np.arctan2(That[:, 1], That[:, 0])
        minPhi = np.percentile(phi, alpha)
        maxPhi = np.percentile(phi, 100 - alpha)
        v1 = np.dot(V, np.array([np.cos(minPhi), np.sin(minPhi)]))
        v2 = np.dot(V, np.array([np.cos(maxPhi), np.sin(maxPhi)]))
        single_patch_cm = np.array([v1, v2])

        svd_array.append(single_patch_cm)

        if verbose & (np.remainder(idx, images.shape[0] // 10) == 0):
            print('SVD {:d} of {:d} images'.format(idx, images.shape[0]))

    svd_array = np.array(svd_array)
    colourmatrix = svd_array.mean(axis=0)
    colourmatrix = np.vstack((colourmatrix, [0, 0, 0]))
    colourmatrix = set_residual_colour_vector(colourmatrix)

    # colourmatrix = stain_utils.normalize_rows(colourmatrix)

    return colourmatrix



"""Data-driven methods to derive column-wise colour matrix"""


def single_kmeans(img,
                  option=1,
                  threshold=0.75):
    from sklearn.cluster import KMeans

    OD, cx, cy = stain_utils.RGB_to_HSD(img)

    if option == 1:  # Using luminance value

        img_to_mask = rgb2lab(img)[:, :, 0] / 100.00
        condition = img_to_mask < threshold

    else:  # Using mean OD/absorbance value
        img_to_mask = np.mean(OD, axis=2)
        condition = img_to_mask > threshold

    # Make vector of cx and cy coordinates with pixel intensities above a threshold
    cxcy = np.array([cx[condition].flatten(),
                     cy[condition].flatten()]).T

    # Remove NaN entries
    if np.sum(np.isnan(cxcy.flatten())) > 0:
        cxcy = cxcy[~np.isnan(np.sum(cxcy, axis=1)), :]

    if cxcy.shape[0] < 2:
        return None #if less points than clusters, just return 0 and use the neighboring colourmatrix

    kmeans = KMeans(n_clusters=2, random_state=0).fit(cxcy)

    # Get colour matrix from the cluster centroids

    colourmatrix = get_vectors(kmeans.cluster_centers_[0, :], kmeans.cluster_centers_[1, :])

    # Set the residual stain vector with the cross-product

    colourmatrix = set_residual_colour_vector(colourmatrix)

    return colourmatrix


"""Colour matrix misc methods"""


def get_vectors(c1, c2, norm=True):
    M = np.concatenate((stain_utils.HSD_to_RGBvec(c1[0], c1[1]),
                        stain_utils.HSD_to_RGBvec(c2[0], c2[1])), axis=0)

    if norm:
        M = M / np.linalg.norm(M, axis=1).reshape(2, 1)
    M = np.concatenate((M,
                        np.array([0, 0, 0]).reshape((1, 3))),
                       axis=0)
    M[np.isnan(M)] = 0
    return M.reshape((3, 3))


def set_residual_colour_vector(colourmatrix):
    cross_ = np.cross(colourmatrix[0, :], colourmatrix[1, :])

    colourmatrix[-1, :] = np.divide(cross_, np.linalg.norm(cross_))

    pinverse_ = np.linalg.pinv(colourmatrix)

    assert np.linalg.det(colourmatrix.T) != 0, \
        "Not linearly independent."

    assert np.all(np.isclose(np.dot(colourmatrix, pinverse_), np.identity((3)))), \
        "Not invertible."

    return colourmatrix


def verbosemsg(i, n, verbose=True):
    if verbose & (np.remainder(i, n // 10) == 0):
        print('Loaded {:d} of {:d} images'.format(i, n))

def adjust_colourmatrix(colourmatrix):

    colourmatrix_adjusted = np.zeros_like(colourmatrix)

    dab_idx, hema_idx = stain_utils.stain_indices(colourmatrix)

    colourmatrix_adjusted[0] = colourmatrix[hema_idx,:]
    colourmatrix_adjusted[1] = colourmatrix[dab_idx, :]
    colourmatrix_adjusted[2] = colourmatrix[-1, :]

    return colourmatrix_adjusted

def normalize_colourmatrix(colourmatrix):
    colourmatrix = set_residual_colour_vector(colourmatrix)
    colourmatrix = stain_utils.normalize_rows(colourmatrix)

    return colourmatrix