# ihcpy

IHC analysis tools for MRI-microscopy comparisons.

For more details, refer to: https://www.sciencedirect.com/science/article/pii/S1053811922008473?via%3Dihub 

## Installation

This assumes you have conda (https://docs.conda.io/en/latest/) already installed.

1. Git clone this repository

```
git clone https://git.fmrib.ox.ac.uk/spet4877/ihcpy.git
```

2. Create a new conda environment

```
conda create -n ihcpy_env
conda activate ihcpy_env
```

3. Navigate to directory and install necessary packages

```
cd ihcpy
conda install --file requirements.txt --channel bioconda
```

**NOTE: python-pptx may not be properly downloaded. Please install it individually if needed (https://anaconda.org/conda-forge/python-pptx)** 

## Usage

A basic pipeline comprises 3 mains steps: 

1. Colour matrix derivation for separating of DAB channel
```
get_colour_matrix.py
get_local_colour_matrix_and_MAD.py
```
2. Thresholding for segmentation of protein
```
get_dab_thresholds.py
```
3. Quantification of segmentation in a stained area fraction (SAF) map
```
get_SAF_map.py 
```
Segmentation can also be generated and checked, to ensure the SAF maps reflect accurate protein content.
```
check_segmentation.py
```
Which and how these functions are steps are used depends on the pipeline: default or artefact.

### Default pipeline

This pipeline is used when no visible striping or staining gradients are present. We strongly recommend using this pipeline first.

The main scripts for this pipeline are:

```
get_colour_matrix.py
get_dab_thresholds.py
get_SAF_map.py 
```
For a sample pipeline that submits scripts directly to jalapeno (FMRIB cluster), refer to the bash script
```
bash default_pipeline
```

**IMPORTANT:** 

**- This only uses a SINGLE value for the alpha parameter.**

**- This script must be run in the 'scripts' folder with the other .py scripts (i.e. get_SAF_map.py)**

Typing this script will give you instructions on how to use it:

```
Usage: default_pipeline.sh -s <svs filepath> -r <root directory> -a <alpha parameter> -a <alpha> -d <column width of window> -p <patch dimensions> -f <patch dimensions> -l <log directory>

        -s Path of IHC svs file
        -r Directory to save your output to
        -a Weighting for segmentation in WOV cost function
        -d Segmentation dimension; column width of window (i.e. 32)
        -p Patch size for high resolution SAF (i.e. 32 32)
        -f Patch size for lower resolution SAF (i.e. 1024 1024)
        -l Directory to save logfiles (if using cluster)
```
A sample input:
```
bash default_pipeline.sh -s ".../VCAN_12_2h_PLP_ROI3_x40_1.svs" -r ".../PLP" -a "-0.20" -d "32" -p "32 32" -f "1024 1024" -l .../logfiles
```

### Artefact pipeline

If strong striping or gradient artefacts are seen in the SAF maps produced from the default pipeline, you can use the artefact pipeline.

The main python scripts for this pipeline are:
```
get_local_colour_matrix_and_MAD.py
get_dab_thresholds.py
get_SAF_map.py 
grid_search.py
```
For the artefact pipeline, refer to the bash script
```
artefact_pipeline

Usage: artefact_pipeline.sh -s <svs filepath> -r <root directory> -a <alpha parameter> -a <alpha> -b <beta> -g <gamma> -d <column width of window> -p <patch dimensions> -f <patch dimensions> -l <log directory>

        -s Path of IHC svs file
        -r Directory to save your output to
        -a Weighting for segmentation in WOV cost function
        -b Weighting parameter for MAD (higher striping requires higher weighting)
        -g Smoothing parameter for DAB thresholds
        -d Segmentation dimension; column width of window (i.e. 32)
        -p Patch size for high resolution SAF (i.e. 32 32)
        -f Patch size for lower resolution SAF (i.e. 1024 1024)
        -l Directory to save logfiles (if using cluster)

```
**IMPORTANT:** 

**- This only uses a SINGLE set of parameters (alpha, beta, gamma).**

**- This script must be run in the 'scripts' folder with the other .py scripts (i.e. get_SAF_map.py)**

A sample input
```
bash artefact_pipeline.sh -s ".../VCAN_12_2h_PLP_ROI3_x40_1.svs" -r ".../PLP" -a "-0.20" -b "1" -g "10" -d "32" -p "32 32" -f "1024 1024" -l .../logfiles
```

### Optimizing parameters 

#### Alpha

This is used to decide what amount of segmentation you want (rule of thumb: lower value = more segmentation).
This is done **empirically** for both default and artefact pipelines. 

The bash script:
```
iterate_alphas.sh

Usage: iterate_alphas.sh -s <svs filepath> -r <root directory> -c <config> -1 <starting alpha> -2 <increments> -3 -3 <ending alpha> -d <column width> -f <patch dimensions> -l <log directory> 

	-s Path of IHC svs file
	-r Directory to save your output to
	-c Configuration of pipeline (default or artefact)
	-1 Starting alpha for the range of alphas to iterate
	-2 Increments for the range of alphas to iterate
	-3 End alpha for the range of alphas to iterate
	-d Segmentation dimension; column width of window (i.e. 32)
	-f Patch size to display segmentation (i.e. 1024 1024)
	-l Directory to save logfiles (if using cluster)

```
...helps generate thresholds with different alpha values, and produces their corresponding segmentation. 
The best alpha can be selected based on the decided segmentation.

You need to decide the range of the alphas (i.e. starting alpha, increments, ending alpha).

For a sample where we iterate for...

alpha=[-0.30, 0.30], increments of 0.05:

```
bash iterate_alphas.sh -s ".../VCAN_12_2h_PLP_ROI3_x40_1.svs" -r ".../PLP" -c "artefact" -1 "-0.30" -2 "0.05" -3 "0.30" -d "32" -f "1024 1024" -l ".../logfiles"
```

#### Beta & Gamma

This is used to correct striping in SAF maps.
This is done **automatically** in the artefact pipelines only via a grid search.

The bash script:
```
iterate_betas_and_gammas.sh

Usage: iterate_betas_and_gammas.sh -s <svs filepath> -r <root directory> -a <alpha> -1 <starting beta> -2 <increments in beta> -3 <ending beta> -4 <starting gamma> -5 <increments in gamma> -6 <ending gamma> -d <column width of window> -p <patch dimensions> -f <patch dimensions> -l <log directory> 

	-s Path of IHC svs file
	-r Directory to save your output to
	-a Alpha term for segmentation
	-1 Starting beta for the range of betas to iterate
	-2 Increments for the range of betas to iterate
	-3 End beta for the range of betas to iterate
	-4 Starting gamma for the range of gammas to iterate
	-5 Increments for the range of gammas to iterate
	-6 End gamma for the range of gammas to iterate
	-d Segmentation dimension; column width of window (i.e. 32)
	-p Patch size for high resolution SAF (i.e. 32 32)
	-f Patch size for lower resolution SAF (i.e. 1024 1024)
	-l Directory to save logfiles (if using cluster)

```
...runs the artefact configuration repeatedly and performs a grid search to find the 'best' SAF map.
Running this for a single .svs slide with artefacts will give a proposed optimal SAF map.

You need to decide the range of the betas and gammas (i.e. starting, increments, ending).
You also need to set an alpha value. 

For a sample where we iterate for...

alpha=-0.2

beta=[-4,4] increments of 0.5

gamma=[1,30] increments of 5: 

```
bash iterate_betas_and_gammas.sh -s ".../VCAN_12_2h_PLP_ROI3_x40_1.svs" -r ".../PLP" -a "-0.20" -1 "-4.0" -2 "0.5" -3 "4.0" -4 "1" -5 "5" -6 "30" -d "32" -p "32 32" -f "1024 1024" -l ".../logfiles"
```

**NOTE: This requires high hard disk memory and time.**

## Project status
This is still a work-in-progress. 

Structure tensor has been added. 

## How to cite this material

Kor DZL, Jbabdi S, Huszar IN, et al. An automated pipeline for extracting histological stain area fraction for voxelwise quantitative MRI-histology comparisons. NeuroImage. 2022;264:119726. doi:10.1016/j.neuroimage.2022.119726

## License

Distributed under a CC-BY-4.0 license for research but not commerical use.

## Copyright

Copyright (c), 2022, University of Oxford. All rights reserved

