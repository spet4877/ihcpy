#!/usr/bin/env python
"""The setup script."""

from setuptools import setup

with open('requirements.txt', 'r') as f:
    requirements = [line.strip() for line in f.readlines() if len(line.strip()) > 0]

test_requirements = ['pytest', ]

setup(
    name='ihcpy',
    version='1.0',
    packages=['modules', 'scripts'],
    test_suite='modules.tests',
    tests_require=test_requirements,
    url='',
    license='',
    author='Daniel Z.L. Kor',
    author_email='daniel.kor@ndcn.ox.ac.uk',
    description='IHC analysis tools for MRI-microscopy comparisons',
    install_requires=requirements,
)
